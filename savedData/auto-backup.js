"use strict";

const { client, sendToMaster } = require("../discordCore");
client.once("ready", scheduleBackup);

const { zipAll } = require("./DataSaver.class");

function scheduleBackup()
{
    const nextMonth = new Date();
    nextMonth.setDate(1);
    nextMonth.setHours(0);
    nextMonth.setMinutes(1);
    nextMonth.setMonth(nextMonth.getMonth() + 1);
    const tillNextMonth = nextMonth - Date.now();
    if(tillNextMonth <= 2**31)
        setTimeout(backup, tillNextMonth);
    else
        setTimeout(scheduleBackup, 2**31 - 1000);
}

async function backup()
{
    scheduleBackup();
    const zip = await zipAll();
    sendToMaster({ files: [{ attachment: zip, name: todaysFilename() }] });
}

function todaysFilename()
{
    const today = new Date();
    return `data_${today.getFullYear()}-${n(today.getMonth()+1)}-${n(today.getDate())}.zip`;
}

function n(n)
{
    return n.toString().padStart(2, 0);
}