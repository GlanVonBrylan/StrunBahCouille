"use strict";

const {
	existsSync, statSync,
	readdirSync, mkdirSync,
	readFileSync, promises: {writeFile},
} = require("fs");
const {clone} = require("../utils");

const JSZip = require("jszip");

const dataSavers = {};


module.exports = (moduleName, defaultValue, create = true) => {
	const dataSaver = dataSavers[moduleName];
	if(dataSaver)
	{
		if(defaultValue)
		{
			if(dataSaver.defaultValue)
				console.warn(`The default value of the saver "${moduleName}" hash been overwritten.`);
			dataSaver.defaultValue = defaultValue;
		}
		return dataSaver;
	}
	else
		return create ? dataSavers[moduleName] = new DataSaver(moduleName, defaultValue) : null;
};

module.exports.savedDataFolder = __dirname;
module.exports.removeFromAll = key => {
	for(const saver of Object.values(dataSavers))
		saver.delete(key);
}

/** @see DataSaver#zip */
module.exports.zipAll = (compression = 6) => {
	const zip = new JSZip();
	for(const saver of Object.values(dataSavers))
	{
		const folder = zip.folder(saver.moduleName);
		folder.file("data.json", JSON.stringify(saver));
		for(const file of readdirSync(saver.folder))
			if(file !== "data.json")
				folder.file(file, readFileSync(`${saver.folder}/${file}`));
	}

	return zip.generateAsync({
		type: "nodebuffer", streamFiles:true,
		compression: "DEFLATE", compressionOptions: { level: compression },
	});
}


/**
 * My awesome JSON-based "database". Saves you the trouble of setting up a database if you only really need to save key-value pairs.
 * You are supposed to have one per "module". This class will create a folder for its module,
 * and each of its entries is saved in a data.json file.
 * As such, your keys should all be strings.
 * @version 2
 */
class DataSaver extends Map
{
	/**
	 * @param {string} moduleName The name of the module using this DataSaver.
	 * @param {object} defaultValue Optional. If provided, this value will be deep-cloned and assigned to unset keys when calling get().
	 * @param {Array} specialIds Obsolete. Non-number ids to load.
	 */
	constructor(moduleName, defaultValue) {
		if(typeof moduleName !== "string")
			throw new TypeError("The folder name should be a string.");

		super();
		this.moduleName = moduleName;
		const folder = this.folder = `${__dirname}/${moduleName}`;
		this.defaultValue = defaultValue;
		this.writing = false;
		this.save = this.save.bind(this);

		if(!existsSync(folder))
			mkdirSync(folder);

		if(existsSync(folder+"/data.json"))
			for(const [key, value] of Object.entries(JSON.parse(readFileSync(folder+"/data.json"))))
				this.quickSet(key, value);
	}


	/**
	 * Gets the total size of all files of this saver.
	 * @returns {number} The total size, in bytes.
	 */
	get totalSize() {
		return statSync(`${this.folder}/data.json`).size;
	}

	/**
	 * Zips all of this saver's contents.
	 * @param {integer} compression The compression level. Must be between 1 and 9.
	 * @returns {Promise<Buffer>} The zipped contents.
	 */
	zip(compression = 6) {
		const zip = new JSZip();
		zip.file("data.json", JSON.stringify(this));
		for(const file of readdirSync(this.folder))
			if(file !== "data.json")
				zip.file(file, readFileSync(`${this.folder}/${file}`));

		return zip.generateAsync({
			type: "nodebuffer", streamFiles:true,
			compression: "DEFLATE", compressionOptions: { level: compression },
		});
	}


	/**
	 * Returns the entry corresponding to the given id.
	 * If this DataSaver has a default value and there is no entry for this id, one is created by cloning the default value.
	 *
	 * @param {string} id The if of the entry to retrieve
	 * @returns {object} The entry, or undefined if there is none and there is no default value.
	 */
	get(id) {
		if(!super.has(id))
			super.set(id, clone(this.defaultValue));
		return super.get(id);
	}

	/**
	 * Creates or updates an entry.
	 * @param {string} id The id of the entry.
	 * @param {object} data The data to set.
	 * @returns {DataSaver} this
	 */
	set(id, data) {
		super.set(id, data);
		this.save();
		return this;
	}

	/**
	 * Creates or updates an entry without saving. Useful when adding several entries in a row (although remember assign() exists).
	 * @param {string} id The id of the entry.
	 * @param {object} data The data to set.
	 * @returns {DataSaver} this
	 */
	 quickSet = super.set;

	/**
	 * Adds the given entries to the DataSaver.
	 * @param {Map || object} objectOrMap The entries to add.
	 * @returns this
	 */
	assign(objectOrMap) {
		const entries = objectOrMap instanceof Map ? objectOrMap : Object.entries(objectOrMap);
		for(const [key, value] of entries)
			super.set(key, value);

		this.save();
		return this;
	}

	/**
	 * Saves data. Useful if you edited data without calling .set().
	 * Example:
	 * const johnsStuff = stuffSaver.get("John");
	 * johnsStuff.thing++; // John has one more thing now
	 * stuffSaver.save();
	 *
	 * @returns {DataSaver} this
	 */
	save() {
		if(this.writing)
		{
			if(this.writing === true)
				this.writing = setTimeout(this.save, 1000);
		}
		else
		{
			this.writing = true;
			writeFile(`${this.folder}/data.json`, JSON.stringify(this)).catch(console.error)
			.finally(() => this.writing = false);
		}

		return this;
	}

	/**
	 * Removes an entry.
	 * @param {string} id The id of the entry to remove.
	 * @returns {object} The deleted entry.
	 */
	delete(id) {
		const deleted = super.delete(id);
		if(deleted)
			this.save();

		return deleted;
	}

	/**
	 * Removes an entry without saving. Useful if you need to delete several entries in a row.
	 * Do remember to call .save() afterwards.
	 * @param {string} id The id of the entry to remove.
	 * @returns {object} The deleted entry.
	 */
	quickDelete = super.delete;

	/**
	 * Deletes all entries.
	 * @returns {DataSaver} this
	 */
	clear() {
		super.clear();
		this.save();
		return this;
	}


	toJSON() {
		return Object.fromEntries(this);
	}
}
