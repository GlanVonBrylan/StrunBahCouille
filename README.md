![Strun Bah Couille](https://i56.servimg.com/u/f56/17/10/54/90/strun_10.jpg)

This bot is french so if you don't speak french... well, too bad!

[![Discord Bots](https://top.gg/api/widget/status/412220365700595712.svg)](https://top.gg/bot/412220365700595712)
[![Discord Bots](https://top.gg/api/widget/servers/412220365700595712.svg)](https://top.gg/bot/412220365700595712)
[![Discord Bots](https://top.gg/api/widget/upvotes/412220365700595712.svg)](https://top.gg/bot/412220365700595712)

# Présentation
Strun Bah Couille est un bot brut de décoffrage et vulgaire qui a pour but d'animer votre serveur. À l'origine, il servait uniquement d'entraînement, mais s'est tellement développé qu'il est réellement devenu un bot à part entière, avec sa propre utilité.
Il est certes très doué pour déblatérer, mais dispose également de commandes utiles comme `>avatar`, quelques commandes de modération (réservées aux administrateurices, bien sûr), et a même un petit jeu dont vous êtes le héros.

Il a également un peu de conversation, pouvant répondre à des phrases comme "j'ai faim" ou "putain, le bot parle ⁈".

Il est également utile aux administrateurices grâce à sa commande `>mute`, et grâce au fait qu'il archive les messages supprimés dans un serveur à part. "Mais pourquoi ne pas plutôt le faire dans un salon spécial du même serveur ?" demandez-vous. Eh bien voyez-vous c'est très simple : à la base, je n'avais pas prévu de laisser les autres admin accéder à cette fonctionnalité qui servait juste à satisfaire ma frustration de ne pas pouvoir lire certains messages car ils avaient été supprimés. Par la suite, j'ai eu la flemme de changer. Et puis, c'est aussi parce que j'avais déjà ce serveur à part pour les messages privés envoyés à mon bot, donc tant qu'à faire...

# Utilisation
Ce bot est à utiliser avec Node.JS 18 ou supérieur.

Créez un fichier `auth.json` dans le même dossier que `bot.js` de la forme suivante :

```JSON
{
	"token": "le token d'authentification de votre bot",
 	"master": "votre id d'utilisateurice",
	"support": "invitation pour le serveur de support (facultatif)",
	"dbl": {
		"token": "votre token pour l'API Discord Bot List",
		"webook": {
			"port": 5757,
			"password": "mot de passe du webhook"
		}
	},
	"autoBackup": true
}
```

`"dbl"` est facultatif. S'il est mis; `"dbl"."webhook"` est également facultatif.

`autoBackup`, si mis à `true`, vous enverra le 1er de chaque moi un zip avec toutes les données.

Et c'est prêt ! Vous n'avez plus qu'à taper `node bot.js` dans une console de commande.


# Licence
**Strun Bah Couille** est fourni sous Licence Publique Rien À Branler (WTFPL). Pour plus de détails, lisez COPYING.txt, ou ce lien : [http://sam.zoy.org/lprab/COPYING](http://www.wtfpl.net/txt/copying)

![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/logo-220x1601.png)
