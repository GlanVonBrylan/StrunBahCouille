"use strict";

const { sendToMaster } = require("./discordCore");

global.error = function error(err)
{
	let msg = "Une erreur est survenue ; lisez la console pour plus de détails.";

	if(err && err.message)
	{
		const {message} = err;
		const status = err.httpStatus || err.response?.status || err.response?.statusCode;
		if(message === "read ECONNRESET" || message.includes("EAI_AGAIN")
			|| status >= 500 || [403, 404, 408].includes(status)
			|| message.includes("WebSocket")
			|| (err.code || err.cause?.code) === "UND_ERR_ABORTED")
			return;

		if(err.name === "DiscordAPIError")
			msg += `\nMessage : ${message}\nChemin : ${err.path}`;
		else if(err.name === "StrunError")
			msg += `\nModule : ${err.module}\nMessage : ${message}`;
		else
			msg += `\nMessage : ${message}`;
	}

	sendToMaster(msg, console.error);
	console.error(err);
}

process.on("unhandledRejection", error);
