"use strict";

const Discord = require("discord.js");
const INTENTS = Discord.GatewayIntentBits;
const auth = require("./auth.json");
const tagRegexp = /[^# ]+#[0-9]{4}/g;

const client = new Discord.Client({
	intents: [
		INTENTS.Guilds,
		INTENTS.GuildMembers,
		INTENTS.GuildPresences,
		INTENTS.GuildMessages, INTENTS.MessageContent,
		INTENTS.GuildMessageReactions,
		INTENTS.DirectMessages,
		INTENTS.DirectMessageReactions,
	],
	partials: [Discord.Partials.Channel], // for DMs
	presence: {
		activities: [{
			name: "Commande d'aide",
			state: ">aide",
			type: Discord.ActivityType.Custom,
		}],
	},
});


const PERMISSIONS = { ...Discord.PermissionFlagsBits }; // to unfreeze it
Discord.PermissionsBitField.Flags = PERMISSIONS;

for(const [key, bitfield] of Object.entries(PERMISSIONS))
	PERMISSIONS[key.toSnakeCase()] = bitfield;

const PERMS_FR = {
	ADMINISTRATOR: "Admin",
	MANAGE_GUILD: "Gérer le serveur",
	KICK_MEMBERS: "Expulser des membres",
	BAN_MEMBERS: "Bannir des membres",
	MODERATE_MEMBERS: "Exclure temporairement les membres",
	MANAGE_CHANNELS: "Gérer les salons",
	MANAGE_MESSAGES: "Gérer les messages",
	MANAGE_NICKNAMES: "Changer les pseudos des autres",
	MANAGE_ROLES: "Gérer les rôles",
	MANAGE_WEBHOOKS: "Gérer les webhooks",
	MANAGE_EMOJIS_AND_STICKERS: "Gérer les émojis et stickers",
	CREATE_GUILD_EXPRESSIONS:"Créer des émojis, stickers et soundboard",
	MANAGE_GUILD_EXPRESSIONS: "Gérer les émojis, stickers et soundboard",
	VIEW_AUDIT_LOG: "Voir les logs",
	VIEW_GUILD_INSIGHTS: "Voir les analyses de serveur",
	VIEW_CREATOR_MONETIZATION_ANALYTICS: "Voir les données de monétisation",

	CREATE_INSTANT_INVITE: "Créer des invitations",
	USE_APPLICATION_COMMANDS: "Utiliser les commandes slash",
	USE_EXTERNAL_APPS: "Utiliser les applis externes",

	VIEW_CHANNEL: "Lire les salons",
	READ_MESSAGE_HISTORY: "Lire les anciens messages",
	SEND_MESSAGES: "Envoyer des messages",
	SEND_VOICE_MESSAGES: "Envoyer des message audio",
	SEND_POLLS: "Envoyer des sondages",
	ADD_REACTIONS: "Ajouter des réactions",
	USE_EXTERNAL_EMOJIS: "Utiliser des émojis externes",
	USE_EXTERNAL_STICKERS: "Utiliser des stickers externes",
	SEND_TTS_MESSAGES: "Envoyer des messages TTS",
	EMBED_LINKS: "Intégrer des liens",
	ATTACH_FILES: "Envoyer des fichiers",
	MENTION_EVERYONE: "Mentionner `@everyone`",
	CHANGE_NICKNAME: "Changer de pseudo",

	CREATE_EVENTS: "Créer des événements",
	MANAGE_EVENTS: "Gérer les événements",
	USE_EMBEDDED_ACTIVITIES: "Utiliser les Activités",
	REQUEST_TO_SPEAK: "Demander à parler en conférence",

	MANAGE_THREADS: "Gérer les fils",
	CREATE_PUBLIC_THREADS: "Créer des fils publics",
	CREATE_PRIVATE_THREADS: "Créer des fils privés",
	SEND_MESSAGES_IN_THREADS: "Envoyer des messages dans les fils",

	CONNECT: "Se connecter en vocal",
	SPEAK: "Parler en vocal",
	USE_VAD: "Utiliser la détection de voix",
	USE_SOUNDBOARD: "Utiliser le soundboard",
	USE_EXTERNAL_SOUNDS: "Utilise les soundboards externes",
	STREAM: "Streamer",
	MUTE_MEMBERS: "Couper le micro des membres",
	DEAFEN_MEMBERS: "Rendre les membres sourds",
	MOVE_MEMBERS: "Déplacer les membres d’un vocal à l’autre",
	PRIORITY_SPEAKER: "Voix prioritaire",
};
const KEY_PERMS = [
	"ADMINISTRATOR", "MANAGE_GUILD", "KICK_MEMBERS", "BAN_MEMBERS", "MODERATE_MEMBERS", "MANAGE_CHANNELS", "MANAGE_MESSAGES", "MANAGE_NICKNAMES", "MANAGE_ROLES", "MANAGE_WEBHOOKS", "MANAGE_THREADS", "MANAGE_EMOJIS_AND_STICKERS", "VIEW_AUDIT_LOG", "VIEW_GUILD_INSIGHTS", "MANAGE_EVENTS", "USE_EMBEDDED_ACTIVITIES",
	"ATTACH_FILES", "MENTION_EVERYONE",
	"USE_PUBLIC_THREADS", "USE_PRIVATE_THREADS",
	"MUTE_MEMBERS", "DEAFEN_MEMBERS", "MOVE_MEMBERS", "PRIORITY_SPEAKER",
];

{
	const missingTranslations = Object.keys(PERMISSIONS).filter(perm => !(/[a-z]/.test(perm) || perm in PERMS_FR));
	if(missingTranslations.length)
		console.warn("Il manque les traductions pour les permissions suivantes :", missingTranslations);
	const extraTranslations = Object.keys(PERMS_FR).filter(perm => !(perm in PERMISSIONS))
	if(extraTranslations.length)
		console.warn("Il y a des traductions de permissions obsolètes :", extraTranslations);
}

var master;
const waitingForMaster = [];


module.exports = exports = {
	tagRegexp,
	auth,
	PermissionFlags: PERMISSIONS,
	getMaster: () => master,
	fetchMaster: () => master ? Promise.resolve(master) : new Promise(resolve => waitingForMaster.push(resolve)),
	sendToMaster,
	checkMaster: ({id}) => id === auth.master,
	isAdmin: member => member.permissions.has(PERMISSIONS.ADMINISTRATOR),
	jpeuxÉcrire: channel => channel.permissionsFor(client.user).has(PERMISSIONS.SEND_MESSAGES, true),
	getUser,
	getUsers,
	getMember,
	getMembers,
	getChannel,
	getChannels,
	getServer,
	getServers,
	client,
	exit: () => {
		client.destroy();
		process.exit();
	},

	confirm: msg => msg.react("✅"),

	PERMS_FR,
	KEY_PERMS,
};


client.login(auth.token);

client.once("ready", async () => {
	exports.master = master = await client.users.fetch(auth.master);
	for(const func of waitingForMaster)
		func(master);
});


function sendToMaster(msg, catcher = error)
{
	if(master)
		master.send(msg).catch(catcher);
	else
		setTimeout(() => sendToMaster(msg, catcher), 300);
}



const ID_REGEX = /[0-9]+/;

async function getGeneric(from, altToId, resolvable, source = client)
{
	if(!resolvable) return null;
	resolvable = resolvable.toString();

	const haystack = source[from];
	const matches = resolvable.match(ID_REGEX);
	let g;

	if(matches)
		try { g = haystack.fetch ? await haystack.fetch(matches[0]) : haystack.cache.get(matches[0]); } catch{} // can fail because names can contain numbers

	if(!g)
		g = haystack.cache.find(elm => elm[altToId] === resolvable);

	return g || null;
}

// Get a user from an id or a tag
function getUser(userResolvable)
{
	return getGeneric("users", "tag", userResolvable);
}

async function getUsers(str)
{
	if(typeof str !== "string" && !(str instanceof Array))
		throw new TypeError("Argument should be an array or a string");

	const users = [], matches = str instanceof Array ? str : str.match(tagRegexp) || str.split(" ");

	if(matches.length)
	{
		for(const resolvable of matches)
		{
			const user = await getUser(resolvable.trim());
			if(user)
				users.push(user);
		}
	}

	return users;
}


// Get member of a server from an id, tag, or User object
async function getMember({members}, userResolvable)
{
	if(!userResolvable) return null;
	const matches = userResolvable.id ? [userResolvable.id] : userResolvable.match(ID_REGEX);
	let m;

	if(matches)
		try { m = await members.fetch(matches[0]); } catch{}
	else
		m = members.cache.find(m => m.user.tag === userResolvable);

	return m || null;
}

async function getMembers(srv, str)
{
	if(typeof str !== "string" && !(str instanceof Array))
		throw new TypeError("Argument should be an array or a string");

	const members = [], matches = str instanceof Array ? str : str.match(tagRegexp) || str.split(" ");

	if(matches.length)
	{
		for(const resolvable of matches)
		{
			const member = await getMember(srv, resolvable.trim());
			if(member)
				members.push(member);
		}
	}

	return members;
}


// Get a channel from and id or name
function getChannel(channelResolvable, fromGuild)
{
	return getGeneric("channels", "name", channelResolvable, fromGuild);
}

async function getChannels(str, fromGuild)
{
	if(typeof str !== "string" && !(str instanceof Array))
		throw new TypeError("Argument should be an array or a string");

	const channels = [], elements = str instanceof Array ? str : str.split(" ");

	if(elements.length)
	{
		for(const resolvable of elements)
		{
			const chan = await getChannel(resolvable, fromGuild);
			if(chan)
				channels.push(chan);
		}
	}

	return channels;
}

// Get a server from and id or name
function getServer(serverResolvable)
{
	return getGeneric("guilds", "name", serverResolvable);
}

// Get servers from ids
async function getServers(str)
{
	if(typeof str !== "string" && !(str instanceof Array))
		throw new TypeError("Argument should be an array or a string");

	const servers = [], ids = str instanceof Array ? str : str.split(" ");

	if(ids.length)
	{
		for(const id of ids)
		{
			const srv = client.guilds.cache.get(id);
			if(srv)
				servers.push(srv);
		}
	}

	return servers;
}
