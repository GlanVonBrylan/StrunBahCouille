"use strict";

function p(n) {
	return (""+n).padStart(2, 0);
}

Date.prototype.old_toLocaleString = Date.prototype.toLocaleString;
Date.prototype.toLocaleString = function(locale, seconds = false) {
	if(locale && locale.toLowerCase().includes("fr"))
		return `${p(this.getDate())}/${p(this.getMonth()+1)}/${this.getFullYear()} à ${p(this.getHours())}:${p(this.getMinutes())}${seconds ? `:${p(this.getSeconds())}` : ""}`;
	else
		return this.old_toLocaleString();
}
