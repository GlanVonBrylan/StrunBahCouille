"use strict";

/********** util.js - version Node.js 2.12.1 ***********/

/* Contents :

 Array.count(element)
 Array.rand()
 Array.shuffle()
 Array.prune()
 String.reverse()
 String.capitalize()
 String.replaceLast()
 String.toSnakeCase()


exports:

 clone(obj)

 rand(max)
 rand(min, max)
 prob(percent)	// has 'percent' chances of returning true

 mapToJSON(map)
 JSONToMap(json)
*/


var DEBUG_MODE = false;



Array.prototype.count = function(e) {
	var count = 0;
	for(let i = 0 ; i < this.length ; i++)
		if(this[i] === e)
			count++;
	return count;
}

Array.prototype.random = function() {
	return this[rand(this.length-1)];
}
Array.prototype.rand = Array.prototype.random;

Array.prototype.shuffle = function() {
    for(let i = this.length - 1 ; i > 0 ; i--)
	{
        const j = ~~(Math.random() * (i + 1));
        [this[i], this[j]] = [this[j], this[i]];
    }
    return this;
}

Array.prototype.prune = function() { // removes duplicates
	return this.filter((e, i) => this.indexOf(e) === i);
}


// /!\ Ne fonctionne pas avec les symboles Unicode multi-caractères commes les émojis
String.prototype.reverse = function() {
    var newString = "";
    for (var i = this.length - 1 ; i >= 0 ; i--)
        newString += this[i];
	return newString;
};

String.prototype.capitalize = function() {
    return this.replace(/(?:^|\s|-)\S/g, a => a.toUpperCase());
};

String.prototype.replaceLast = function(search, replace) {
	const index = this.lastIndexOf(search);
	return index === -1 ? this : this.slice(0, index) + replace + this.slice(index + search.length);
}

String.prototype.toSnakeCase = function() {
	// Weird regexes to not accidentally turn strings like HTTPQuery into H_T_T_P_Query
	return this.replace(/(?!^)[A-Z](?=[^A-Z])/g, letter => `_${letter}`)
			.replace(/(?<=[^A-Z])[A-Z](?=[A-Z])/g, letter => `_${letter}`)
			.toUpperCase();
}
String.prototype.toCamelCase = function(upper) {
	const str = this.toLowerCase().replace(/_./g, ([,letter]) => letter.toUpperCase());
	return upper ? str.replace(/^\S/, a => a.toUpperCase()) : str;
}


/* Fonction récupérée sur 'Hey, JavaScript' et écrite par Elliot Bonneville, corrigée par mes soins */
function clone(obj)
{
	if(obj === null || typeof obj !== 'object')
		return obj;

	var temp = new obj.constructor();

	for(const key in obj)
		temp[key] = clone(obj[key]);

	return temp;
}

// Bornes incluses
function rand(min, max)
{
	if(max === undefined) { max = min; min = 0; }
	if(min > max) { let temp = min; min = max; max = temp; }
	return ~~(Math.random() * (max - min + 1)) + min;
}


module.exports = {

	clone,

	// rand(min, max)
	// Le minimum est optionel. Il vaut 0 par défaut.
	// Les bornes sont incluses.
	rand,

	prob: percent => Math.random() * 100 < percent,


	mapToJSON: map => JSON.stringify([...map]),
	JSONToMap: json => new Map(JSON.parse(json)),

}; // module.exports
