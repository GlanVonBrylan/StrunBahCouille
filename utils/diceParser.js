"use strict";

const { rand } = require(".");

const exprDé = /^([0-9]{0,3}d[0-9]{1,3})( *[\+-][0-9]{1,5})?( (d[éei]s)?ad?v(antage)?)?$/;

exports.parseDice = function(expr, maxRolls = 30)
{
	const matches = expr.match(exprDé);
	if(!matches)
		return "Paramètre incorrect.";
	
	let [ , dice, bonus = 0, adv] = matches;
	bonus = +bonus;

	const max = Math.min(maxRolls, 100);
	let [n, d] = dice.split('d');
	n = Math.min(max, ~~(n || 1));
	d = ~~d;
	if(n < 1 || d < 2)
		return "L’argument de dé ou de nombre est incorrect.";

	const outcome = {
		params: { n, d, bonus },
		result: throwDice(n, d),
	};

	if(adv)
	{
		const secondResult = throwDice(n, d);
		adv = adv.startsWith(" a");
		outcome.params.advantage = adv ? "avantage" : "désavantage";
		const better = secondResult.total > outcome.result.total;
		if(better === adv)
		{
			outcome.lostResult = outcome.result;
			outcome.result = secondResult;
		}
		else
			outcome.lostResult = secondResult;
	}

	return outcome;
}

function throwDice(n, d)
{
	const rolls = Array(n);
	let total = 0;
	for(let i = 0 ; i < n ; i++)
		total += rolls[i] = rand(1, d);
	return { rolls, total };
}


/*
class ParseError extends Error
{
	constructor(type, detail, got) {
		super(!detail ? type : got ? `${type} (got ${detail})` : `${type}: ${detail}`);
		this.type = type;
		this.detail = detail;
	}
}



function getStream(string) {
	if(typeof string !== "string")
		throw new TypeError("The parameter should be a string.");

	const {length} = string;
	let cursor = 0;

	return Object.freeze({
		string,
		peek: () => string[cursor],
		get: () => string[cursor++],
		eof: () => cursor >= length,
	});
}



function getLexer(str)
{
	const stream = getStream(str);
	const tokens = [];

	while(!stream.eof())
	{
		let char = stream.get();
		if(/[0-9]/.test(char))
		{
			while(/[0-9,.]/.test(stream.peek()))
				char += stream.get();
			const number = +char.replaceAll(",", ".");
			if(isNaN(number))
				throw new ParseError("invalid number", char);
			tokens.push(number);
		}
		else switch(char)
		{
			case " ": case " ": case "\t": case "\n": break;
			case "+": tokens.push("ar_+"); break;
			case "-": tokens.push("ar_-"); break;
			case "*": tokens.push(stream.peek() === "*" ? (stream.get(), "ar_^") : "ar_*"); break;
			case "^": tokens.push("ar_^"); break;
			case "/": tokens.push("ar_/"); break;
			case "(": case ")": tokens.push(char); break;
			case "d": case "D": tokens.push("d"); break;
			default: throw new ParseError("unexpected character", char);
		}
	}

	const {length} = tokens;
	let cursor = 0;

	return Object.freeze({
		peek: () => tokens[cursor],
		get: () => tokens[cursor++],
		eoft: () => cursor >= length,
	});
}


function getTree(lexer)
{
	const tree = [];
	const isSubTree = lexer.peek() === "(";
	if(isSubTree)
		lexer.get();

	const push = node => tree.push(last = node);
	let foundClosingPar = false;
	let last;

	while(!lexer.eoft())
	{
		if(lexer.peek() === "(")
		{
			push(getTree(lexer));
			continue;
		}

		const token = lexer.get();
		if(typeof token === "number")
		{
			if(token.get() !== "d")
				throw new ParseError("unexpected token after number", next);

			if(!Number.isInteger(token) || token <= 0)
				throw new ParseError("number of throws must be a positive integer", token, true);

			const sides = token.get();
			if(!Number.isInteger(sides) || sides <= 0)
				throw new ParseError("number of sides must be a positive integer", token, true);

			push({ type: "roll", throws: token, sides });
		}
		else if(token === ")")
		{
			if(!isSubTree)
				throw new ParseError("unexpected closing parenthesis");
			foundClosingPar = true;
			break;
		}
		else
		{
			const operation = token[3];
			if(!last)
			{
				if(token === "ar_+")
					continue;
				if(token !== "ar_-")
					tree.push({type: "number", val: 0});
				else
					throw new ParseError("unexpected token", `'${operation}'`);
			}

			if(last.type === "operator")
				throw new ParseError(`unexpected token after '${last.type[3]}' operator`, `'${operation}'`);

			push({type: "operator", operation, prio: operation === "^" ? 1 : ["*", "/"].includes(operation) ? 2 : 3})
		}
	}

	if(isSubTree)
	{
		if(!foundClosingPar)
			throw new ParseError("missing closing parenthesis");
		if(typeof lexer.peek() === "number")
			throw new ParseError("unexpected number after closing parenthesis");
	}

	return tree;
}
*/
/*
exports.module = exports = function parse(str) {
	const lexer = getLexer(str);
	const tree = getTree(lexer);
}
*/
