"use strict";

const crisseBarbes = {
	"Commander Shepherd": "486436345959415809",
	"Strun Bah Couille": "412220365700595712",
	"Lok Vah Couille": "432622398982979594",
	"Hisse La Nouille": "437707171204169728",
	"Hun Kaal Zonn": "442006320024125440",
	"Su Grah Double": "474912514165964822",
	"Gaan Lah Baas": "1032795028424245258",
};
const idCrisseBarbes = Object.values(crisseBarbes);

const voyelles = ['a', 'à', 'â', 'ã', 'ä', 'æ', 'e', 'é', 'è', 'ê', 'ë', 'œ', 'i', 'ì', 'î', 'ï', 'o', 'ô', 'õ', 'ö', 'u', 'ù', 'û', 'ü', 'y', 'ÿ', 'h'];

const cris_safe = ["STRUN BAH COUILLE", "SHO KAH PIK", "MOR OH RAA", "IIL FEH BO", "GUR GAN DINN", "VAA TFER FUTHR", "JEH LA DALL", "TUU FEH CHIEH", "TETT DE KUU", "JAN BON BEURH", "SAA LO PRIIH", "TEH TAA CLAK", "PAA TA PEH", "RII ZO TOH", "REUH PAA CHO", "AH BONN TWAA", "BROSS AH CHIOTT", "TAR TOH FREZ", "CLAH FU TII", "MAAR GU LIN", "BAAR BAH TRUK", "BUU LII AH", "FEH PAA LCON", "MEEM PAA VREH", "BRAN KII NYOL", "VAA NUH PIEH", "YU GI YO", "JAN PEU PLUU", "TEH KIN NUL", "REN LAHR JEN", "TIHR EEH VOU", "KREPP O ZEUU", "VIV LEUF RONSS", "SAH PRI STII", "TAY CREH YON", "HO PI TAAL", "SEH LAA DESH", "MOHR O VASH", "JEH PEH TEH", "ILH SUU FIH", "SAL SII FIH", "BOU TOH NEU", "TAH PER DUH", "TOOT AH FEY", "BO NI DEH"];
const cris = cris_safe.concat(["SUSS MAH BITT", "MANJ DEH KEU", "NIIK TA MERH", "GRO PEH DEH", "SAAL AH RAB", "NAA REH NYEH\n(rii bii dii)"]);

// REGLES DES INSULTES
// Pas d'insultes basiques type "idiot", "connard", etc.
// Pas d'insultes sexuelles type "suce-bites" ou "garage à bites". Exception faites pour celles ayant quelque peu perdu leur connotation sexuelle, comme "empaffé".
// Pas d'insultes sur le physique sauf le visage.
const insultes_simpluriel = ["pignouf", "bachi-bouzouk", "barbare", "margoulin", "hexacosioihexekontahexaphobe", "n’wah", "rascal", "béjaune", "sycophante", "bobo gauchiste", "crétin des Alpes", "iconoclaste", "droit-de-l’hommiste", "orchidoclaste", "fesse-mathieu", "sodomite servile", "cloporte anémique", "félon honni", "vermifuge", "fonctionnaire", "galapiat", "envaselineur", "nodocéphale", "Thénardier", "péquenaud brouette", "plagioclase", "préphanérogame", "préspermaphyte", "impertinent incontinent", "giton tartare", "faquin", "pédoncule", "turbodébile", "capodastre", "sociopathe analphabète", "chat gitan juif mongol", "yéti des villes", "gros malus écologique", "indécrotable con", "sombre andouille", "sombre personnage", "parallèlépipède rectangle", "belître", "gougnafier", "ostrogoth", "wisigoth", "paltoquet", "sagouin", "enfiguralimificulé", "puminoritanitain", "kassos laïque"];
const insultes = insultes_simpluriel.concat(["empaffé·e", "bois-sans-soif", "enfant du grand capital", "dinosaure partouzeur de droite", "tête de gland", "intermittent du spectacle", "mange-cagettes", "gamer sur mobile", "maman facebook", "triple buse en calbar", "fanboy d’Apple", "erreur de la Nature", "four à merde", "bathyscaphe de la vertu", "progéniture d’andouille", "conjonction de coordination", "sac à paillettes survitaminé", "charlot de vogue", "mongol·e à batteries", "social-traître", "pute à frange", "manche à couille", "weeaboo décérébré par des waifus lollipop", "marin d’opérette", "couard en collants", "tas de merde en bas de soie", "colossal couillon", "vulgaire marchand·e de tapis", "marmiton de Babylone", "petit papillon qui boit du thé", "verbe du 3e groupe",
// Insultes exclusivement singulier
"buveur·se de lait", "voleur·se de figues", "traficant·e de guano", "marchand·e de soupe", "mangeur·se de fruits secs", "maudit·e chien·ne sale", "pisse-froid", "infâme fils de chenille", "fils d’unijambiste", "Rantanplan", "pauvre cloche", "greluchon famélique", "Jean-Foutre", "marquis de couilles vertes", "Mussolini de carnaval", "vexatoire petit sot"]);

const insultesPlurielles = insultes_simpluriel.map(i => i.replaceAll(/(?<!s)(?=( |$))/g, "s")) // met un s à chaque mot qui n'en a pas déjà un
.concat(["empaffé·e·s", "bois-sans-soif", "enfants du grand capital", "dinosaures partouzeurs de droite", "têtes de gland", "intermittents du spectacle", "mange-cagettes", "gamers sur mobile", "mamans facebook", "triples buses en calbar", "fanboys d’Apple", "erreurs de la Nature", "fours à merde", "bathyscaphes de la vertu", "progénitures d’andouilles", "conjonctions de coordination", "sacs à paillettes survitaminés", "charlots de vogue", "mongols à batteries", "sociaux-traîtres", "putes à frange", "manches à couilles", "weeaboos décérébrés par des waifus lollipop", "marins d’opérette", "couards en collants", "tas de merde en bas de soie", "colossaux couillons", "vulgaires marchand·e·s de tapis", "marmitons de Babylone", "petits papillons qui boivent du thé", "verbes du 3e groupe",
// Insultes exclusivement plurielles
"dugenous", "duglands", "équipe de foot", "gouvernement de droite", "marmaille"]);

const pasInsultes = ["très chèr·e", "minestre", "mia pauvre", "mia grand·e", "bibiche", "Paulo", "mamène"];

const taMère_safe = [
	"Ta mère est tellement grosse, quand elle et des talons aiguille, elle trouve du pétrole !",
	"Ta mère est tellement grosse, quand elle se lève il y a une éclipse !",
	"Ta mère est tellement grosse qu’il faut deux pokéflutes pour la réveiller !",
	"Ta mère est tellement grosse qu’elle couvre plusieurs fuseaux horaires !",
	"Ta mère est tellement grosse que pour la photographier il faut un satellite !",
	"Ta mère est tellement grosse que lorsqu’elle se pèse, c’est son numéro de téléphone qui s’affiche !",
	"Ta mère est tellement grosse qu’au resto il lit le menu et dit « OK » au serveur !",
	"Ta mère est tellement grosse que quand elle était à l’école tout le monde était assis à côté d’elle !",
	"Ta mère est tellement grosse que si elle s’est acheté un téléphone portable, c’est parce qu’elle ne rentre plus dans les cabines téléphoniques !",
	"Ta mère est tellement grosse que la baignoire lui sert de bidet !",
	"Ta mère est tellement grosse que quand elle marche dans la rue on dirait deux hippopotames en train de se battre !",
	"Ta mère est tellement grosse que le fauteuil reste accroché à son cul quand elle se lève !",
	"Ta mère est tellement grosse qu’on calcule ses mensurations avec un altimètre !",
	"Ta mère est tellement grosse que quand Dieu a créé le Monde, il a dit : “pousse-toi” et la lumière fut.",
	"Ta mère est tellement grosse qu’il faut 5 dimensions pour la dessiner !",
	"Ta mère est tellement grosse que quand elle traverse la rue, c’est les bagnoles qui font gaffe de ne pas se faire écraser !",
	"Ta mère est tellement grosse que quand elle fait du trampoline, les martiens se demandent ce que c’est que cette planète bleue qui tourne autour de ta mère.",
	"Ta mère est tellement grosse que quand on lui tape sur le ventre, on peut surfer sur les vagues !",
	"Ta mère est tellement grosse qu’elle s’est fait installer des feux de recul !",
	"Ta mère est tellement grosse qu’elle a son propre code postal !",
	"Ta mère est tellement grosse que quand elle met des bigoudis, on dirait un car-wash !",
	"Ta mère est tellement grosse que quand les gens la voient sur la plage, ils la remettent à l’eau !",
	"Ta mère est tellement grosse qu’elle doit enlever son pantalon pour mettre les mains dans les poches !",
	"Ta mère est tellement grosse qu’il faut des chiens d’avalanche pour trouver son nombril !",
	"Ta mère est tellement grosse que lorsqu’elle se baigne, elle a une escorte de GreenPeace autour d’elle !",
	"Ta mère est tellement grosse, le Géant Vert pourrait être son nain de jardin.",
	"Ta mere est tellement grosse qu’elle pourrait boucher les chutes du Niagara !",
	"Ta mère est tellement grosse que quand tu la prends en photo, elle prend toute la mémoire de ton téléphone !",
	"Ta mère est tellement grosse qu’elle ne porte plus des culottes Petit Bateau mais gros paquebot !",
	"Ta mère est tellement grosse, sa taille de ceinture c’est l’Équateur !",
	"Ta mère est tellement grosse, que quand elle va au cinéma, elle prend un tarif de groupe !",
	"Ta mère est tellement grosse que quand prend une photo de sa maison, on la voit à toutes les fenêtres !",
	"Ta mère est tellement grosse, son tour de taille se mesure en parsecs !",
	"Ta mère est tellement grosse, quand elle s’assoit elle bouche le trou de la sécu !",
	"Ta mère est tellement moche que des fois je me dis qu’être aveugle c’est pas si mal.",
	"Ta mère est tellement laide qu’à sa naissance, les médecins ont hésité entre un berceau et une cage !",
	"Ta mère est tellement moche que quand elle va à la banque, ils éteignent les caméras.",
	"Ta mère est tellement moche que quand elle sort du zoo, les gardiens vérifient les cages !",
	"Ta mère est tellement crade que pour se brosser les dents elle utilise une ponceuse !",
	"Ta mère est tellement radine que quand tu lui mets un coup de pied au cul, elle serre les fesses pour te prendre ta chaussure !",
	"Ta mère est tellement vieille qu’elle a assisté à la naissance de Jésus, c’était la vache dans l’étable.",
	"Ta mère est tellement vieille, elle est née en noir et blanc.",
	"Ta mère est tellement vieille, elle a assisté à l’intronisation d’Elizabeth II.",
	"Ta mère est tellement vieille qu’elle est patrimoine mondial de l’UNESCO.",
	"Ta mère est tellement veille qu’elle fait du lait en poudre.",
	"Si les avions volent c’est que ta mère n’est pas dedans !",
	"La différence entre ta mère et un éléphant c’est que lui au moins, il a un travail. Ta mère même le cirque il en veut pas !",
	"Au-delà de l’ordre de grandeur des planètes, il y a les étoiles. Au-delà de l’ordre de grandeur des étoiles, il y a les trous noirs supermassifs. Au-delà, il y a les galaxies. Au-delà, il y a les amas de galaxie. Plus massif que ça, il n’y a que l’ego de Morgân, le menton des Bogdanov, et ta mère.",
];
const taMère = taMère_safe.concat([
	"La chatte à ta mère est tellement profonde, la NASA s’en sert comme hangar !",
]);

const {rand} = require(".");

var respect, genre;

class StrunError extends Error {
	constructor(module, message) {
		super(message);
		this.name = "StrunError";
		this.module = module;
	}
}

module.exports = exports = {
	crisseBarbes,
	checkCrisseBarbe: ({id}) => idCrisseBarbes.includes(id),
	niceJSONDisplay,
	parseParam,
	cris,
	cris_safe,
	insulte,
	insultePluriel,
	insultes,
	insultesPlurielles,
	taMère: safe => (safe ? taMère_safe : taMère).rand(),
	voyelles,
	delayedMsgDelete: msg => setTimeout(() => msg.delete(), 5000),
	StrunError: StrunError,

	licence: "```" + require("fs").readFileSync("COPYING.txt", "utf-8") + "```https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/WTFPL_logo.svg/256px-WTFPL_logo.svg.png",

	init: function() {
		delete this.init;
		({respect, genre} = require("../modules/zouz"));
		return this;
	}
};



function niceJSONDisplay(obj, highlight = "**", separator = "\n")
{
	const secondaryHighlight = highlight === "**" ? "*" : "";
	return Object.entries(obj).map(([key, val]) => `${highlight}${key} :${highlight} ${
		  typeof val === "object"  ? (val ? niceJSONDisplay(val, secondaryHighlight, ", ") : "non")
		: typeof val === "boolean" ? (val ? "oui" : "non")
		: val}`
	).join(separator);
}


function insulte(qui, capitalize)
{
	let insulte;
	const respecte = respect(qui);

	if(respecte)
		insulte = pasInsultes.rand();
	else
	{
		insulte = insultes.rand();
		switch(rand(4))
		{
			case 0: insulte = `sale ${insulte}`; break;
			case 1: insulte = `misérable ${insulte}`; break;
			case 2: insulte = `vil·e ${insulte}`; break;
			case 3: insulte = voyelles.includes(insulte[0]) ? `espèce d’${insulte}` : `espèce de ${insulte}`; break;
			case 4: insulte = voyelles.includes(insulte[0]) ? `bougre d’${insulte}` : `bougre de ${insulte}`; break;
		}
	}

	const mf = genre(qui);
	if(mf)
	{
		insulte = insulte.replace("r·se", mf === "m" ? "r" : "se").replace(/·([a-z]*)/g, mf === "m" ? "" : "$1");
		if(mf === "f")
			insulte = insulte.replace("fils", "fille");
		if(respecte)
			insulte = insulte.replace("mia", mf === "m" ? "mon" : "ma");
	}

	return capitalize ? insulte.capitalize() : insulte;
}

function insultePluriel(capitalize)
{
	let insulte = insultesPlurielles.rand();

	switch(rand(5))
	{
		case 0: insulte = `sales ${insulte}`; break;
		case 1: insulte = `misérables ${insulte}`; break;
		case 2: insulte = `vil·e·s ${insulte}`; break;
		case 3: insulte = voyelles.includes(insulte[0]) ? `bande d’${insulte}` : `bande de ${insulte}`; break;
		case 4: insulte = voyelles.includes(insulte[0]) ? `tas d’${insulte}` : `tas de ${insulte}`; break;
		case 5: insulte = voyelles.includes(insulte[0]) ? `bougres d’${insulte}` : `bougres de ${insulte}`; break;
	}

	return capitalize ? insulte.capitalize() : insulte;
}


function parseParam(prm)
{
	prm = prm.toLowerCase();
	if(prm === "true" || prm === "oui")
		return true;
	else if(prm === "false" || prm === "non" || prm === "")
		return false;
	else if(isFinite(prm))
		return ~~prm;

	return prm;
}
