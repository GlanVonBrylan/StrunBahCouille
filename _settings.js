"use strict";

const { writeFile } = require("fs/promises");
const { getChannel } = require("./discordCore");

var dmChan = null;
const settings = {
	dmChan: "master",
	version: require("./modules/moderation/réglages - défaut").version,
};

module.exports = exports = {
	settings,
	saveSettings,
	setDMChan,
	dmChan: () => dmChan,
};


try {
	const tempSettings = require("./savedData/settings.json");

	for(const prm in tempSettings)
		if(!(prm in settings))
			delete tempSettings[prm];

	Object.assign(settings, tempSettings);
}
catch(e) {
	saveSettings();
}


function saveSettings()
{
	writeFile("savedData/settings.json", JSON.stringify(settings));
}

async function setDMChan(value)
{
	let understood = true;
	if(value === "master")
	{
		dmChan = require("./discordCore").master;
		settings.dmChan = "master";
	}
	else if(value && isFinite(value))
	{
		dmChan = await getChannel(dmChan);
		understood = !!(settings.dmChan = (dmChan?.id || null));
	}
	else
		settings.dmChan = dmChan = null;

	saveSettings();
	return understood;
}


require("./discordCore").client.prependListener("ready", () => setDMChan(settings.dmChan));
