"use strict";

const { auth, client } = require("./discordCore");

if(auth.dbl)
{
	const { vip, pauvre, don } = require("./modules/zouz");
	const {token, webhook} = auth.dbl;
	const fetchUser = client.users.fetch.bind(client.users);
	process.on("uncaughtException", error);

	function launchDBL() {
		exports.autoPoster = new (require("topgg-autoposter").DJSPoster)(token, client);
		exports.autoPoster.on("error", error);

		if(webhook)
		{
			exports.webhook = new (require("@top-gg/sdk").Webhook)(webhook.password);
			const handleRequest = exports.webhook.middleware();

			const server = exports.webhookServer = require("http").createServer(async (req, res) => {
				if(req.method !== "POST")
				{
					res.setHeader("Allow", "POST");
					res.statusCode = req.method === "OPTIONS" ? 200 : 405;
					return res.end();
				}

				res.sendStatus = code => { // to mimic express.js
					res.statusCode = code;
					res.end();
				}
				res.status = code => {
					res.statusCode = code;
					return { json: json => res.end(JSON.stringify(json)) };
				}
				await handleRequest(req, res, Function());

				const {vote} = req;
				if(!vote)
					return;

				const id = vote.user, récompense = vip(id) || pauvre(id) ? 500 : 300;
				don(récompense, id);
				fetchUser(id).then(user => user.send(`Merci d’avoir voté ! J’ai ajouté ${récompense} ƶ à ton compte en récompense.`), error);
			});

			server.listen(webhook.port);
		}
	}

	if(webhook)
	{
		const {exec} = require("child_process");
		// In case a previous listener was left dangling...
		exec(`lsof -i TCP:${webhook.port} | grep LISTEN`, (err, stdout, stderr) => {
			if(stdout)
				exec("kill -9 " + stdout.match(/[0-9]+/), launchDBL);
			else
				launchDBL();
		});
	}
	else
		launchDBL();
}
