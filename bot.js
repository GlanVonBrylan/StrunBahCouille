"use strict";

require("./utils");
require("./utils/polyfills");
require("./error");

require("./commands").registerCategory("🖕🏽", {
	nom: "Strun",
	color: 0x1166DD,
	title: "Strun",
	thumbnail: { url: "https://i62.servimg.com/u/f62/17/10/54/90/strun_10.png" },
	description: "Je suis avant tout un emmerdeur bougon et vulgaire.",
	shortDescription: "Insultes et jurons à la capitaine Haddock",
}, 0);

const splitMessage = require("./utils/splitMessage");
const {
	auth, client,
	checkMaster, isAdmin, jpeuxÉcrire,
	PermissionFlags: { MANAGE_CHANNELS },
} = require("./discordCore");
const { dmChan } = require("./_settings");
require("./utils/Strun").init(); // important d'appeler init avant d'inclure zouz, venture etc

const { isIgnored } = require("./modules/moderation/spoy");
const { advance: venture } = require("./modules/venture");
const { analyse: yo } = require("./modules/yo");

//jvousProute = ["je vous proute", "je vous emmerde", "vous pouvez aller vous faire empapaouter chez les grecs", "c’est probablement de votre faute"],
const proutExceptions = ["><", ">.<", ">>", ">.>", ">->", ">_>", ">x<", ">*<", ">v<", ">w<", ">~<", ">-<", ">^<", ">_<", ">3<", ">:3", ">:)", ">:|", ">:(", ">:c"];

const { settings } = require("./_settings");
const { defaultSettings } = require("./modules/moderation/réglages");
const servers = require("./savedData/DataSaver.class")("servers");

const commands = require("./commands");
require("./modules");
require("./dbl");

var master, myself;

if(auth.autoBackup)
	require("./savedData/auto-backup");

	
client.on("error", error);


client.prependListener("ready", () => {
	myself = client.user;
	client.users.fetch(auth.master).then(m => master = m);
	console.log(`Connecté en tant que ${myself.tag} !`);
});


client.on("guildCreate", srv => master.send(`J’ai débarqué dans **${srv.name}** (${srv.id}) (${srv.memberCount} membres).`));
client.on("guildDelete", srv => srv.name && master.send(`J’ai quitté **${srv.name}** (${srv.id}).`));


client.on("messageCreate", async msg => {
	if(msg.author.bot || msg.system || !msg.content)
		return;

	const content = msg.content.replace(/ /g, ' '); // remplacement des espaces insécables par des espaces normaux
	const { guild, author, channel } = msg;
	let [cmd, ...prms] = content.split(" ");
	cmd = cmd.toLowerCase();
	let stg = { settings: defaultSettings };
	let p = ">";

	if(guild)
	{
		myself = await guild.members.fetch(myself.id);
		stg = servers.get(guild.id);

		p = stg.settings.prefix.toLowerCase();

		if(p !== ">")
		{
			if(cmd.startsWith(p))
				cmd = cmd.replace(p, ">");
			else if(cmd[0] === ">" && cmd !== ">préfixe") // Pour éviter que Strun de continue à répondre au préfixe par défaut...
				cmd = cmd.replace(">", "<");
		}


		if(!jpeuxÉcrire(channel))
		{
			if(cmd[0] === ">" && !proutExceptions.includes(cmd))
				author.send(`Je n’ai pas la permission d’écrire dans ${channel}, donc je ${["boude", "te proute", "vais me tourner les pouces"].rand()}.`).catch(Function());
			return;
		}


		if(settings.state === "under repair" && !checkMaster(author))
		{
			channel.send("Je suis en pleine réparation là, foutez-moi la paix.");
			return;
		}

		if(stg.settings.slowMode && msg.member && isAdmin(msg.member))
			m_testeZaWarudo(msg, stg.settings.slowMode);
	}
	else
	{
		if(!checkMaster(author))
		{
			if(settings.state === "under repair")
			{
				channel.send("Je suis en pleine réparation là, foutez-moi la paix.");
				return;
			}

			if(!isFinite(content) && cmd !== ">venture" && cmd !== ">aide")
			{
				let message = `Message de la part de ${author} :\n${msg.content}`;

				if(msg.attachments.first())
					for(const [ , attachement] of msg.attachments)
						message += "\n+ " + attachement.url;

				const chan = dmChan();
				if(chan)
					splitMessage(message).forEach(chan.send.bind(chan));
			}
		}

		if(Number.isInteger(+content))
			return venture(msg);
	}

	msg.prefix = p;

	if(cmd[0] !== ">")
	{
		const Aya = "393580418949185537";
		if(author.id !== Aya
			&& !channel.isVoiceBased()
			&& (!guild || stg.settings.yo)
			&& !isIgnored(channel))
		{
			try {
				yo(msg);
			}
			catch(e) {
				error(e);
			}
		}
		return;
	}

	if(proutExceptions.includes(cmd) || content.startsWith("> ") || content.startsWith(">>>"))
		return;

	prms = prms.filter(p => p);
	cmd = cmd.substring(1);

	if(/^[0-9]{0,3}[Dd][0-9]{1,3}/.test(cmd))
	{
		prms.unshift(cmd);
		cmd = "lance";
	}

	msg.reply = msg.reply.bind(msg);

	if(commands(cmd, msg, prms.length > 1 || prms[0] ? prms : []) === null)
	{
		msg.react("❔").then(reaction => {
			if(guild) setTimeout(() => reaction.users.remove(myself).catch(Function()), 2000);
		}, Function());
	}
});


function m_testeZaWarudo(msg, time)
{
	const message = msg.content.substring(0, 15).toUpperCase();
	const {author, channel, guild: {members:{ me }}} = msg;
	let t;

	if(/^(\*\*)?(FEHR MEH LAA|CAL MEH VUU|ZA WARUDO) ?!?(\*\*)?$/.test(message))
		t = time;
	else if(/^(\*\*)?(LII BER TEH|DIIS QU TEH|PAA PO TEH) ?!?(\*\*)?$/.test(message))
		t = 0;

	if(t !== undefined)
	{
		if(channel.permissionsFor(me).has(MANAGE_CHANNELS))
			channel.setRateLimitPerUser(t);
		else
			author.send(`Malheureusement je n’ai pas la permission de gérer ${msg.channel}...`);
	}
}
