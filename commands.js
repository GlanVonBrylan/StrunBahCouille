"use strict";

const {Collection} = require("discord.js");


const mainHandler = new CommandHandler();
const masterInfos = new Collection();

module.exports = exports = Object.assign(mainHandler, {
	infos: mainHandler.cmdsInfos,
	masterInfos,
	CommandHandler,
});


const {checkMaster, client} = require("./discordCore");


/**
 * A command handler.
 * Call a command with cmdHandler(command, msg, params)
 */
function CommandHandler()
{
	const cmds = {};
	const cmdsInfos = new Collection();
	const catAliases = new Set();
/*
	client.on("interactionCreate", interaction => {
		if(!interaction.isCommand() || !cmdsInfos.has(interaction.commandsName))
			return;

		cmdsInfos.get(interaction.commandsName)(interaction);
	});*/

	return Object.assign((commandName, msg, params) => {
		const command = cmds[commandName];
		if(typeof command !== "function")
			return null;
		if(command.masterOnly && !checkMaster(msg.author))
			return false;
		return command(msg, params, commandName);
	}, {
		cmdsInfos,
		constructor: CommandHandler,

		/**
		 * Registers a command category.
		 * @param {string|string[]} emoji The category's emoji. If an array, the first element will be taken as the emoji, and the rest as aliases. Aliases will be trimmed and set to lower case.
		 * @param {object} embedInfos Embed properties to be displayed as info, like title, color, description...
		 * @param {number} priority Priority in the category list. Lower number = higher in the list.
		 *
		 * @throws {TypeError} if the name if not a non-empty string.
		 */
		registerCategory(emoji, embedInfos, priority = 10) {
			let aliases = [];
			if(emoji instanceof Array)
				[emoji, ...aliases] = emoji;
			if(!emoji || typeof emoji !== "string")
				throw new TypeError("'emoji' must be a non-empty string.");

			aliases = aliases.map(alias => {
				if(!alias || typeof alias !== "string")
					throw new TypeError(`Aliases must be non-empty strings (category ${emoji}).`);

				alias = alias.trim().toLowerCase();
				if(catAliases.has(alias))
					throw new Error(`Alias '${alias}' is already used (category ${emoji})`);

				catAliases.add(alias);
				return alias;
			});

			const { shortDescription, nom } = embedInfos;
			if(!shortDescription)
				console.warn(`Category "${nom}" doesn't have a short description!`);
			else if(typeof shortDescription !== "string")
				throw new TypeError(`Category "${nom}"'s short descriptions is of type '${typeof shortDescription}'`);
			else if(shortDescription.length > 100)
				throw new Error(`Category "${nom}"'s short description is too lengthy (${shortDescription.length}/100)`);

			cmdsInfos.set(emoji, {
				priority,
				emoji, aliases,
				embedInfos,
				commands: new Collection(),
			});
		},

		/**
		 * Register the given command.
		 * @param {string} category This command's category. Put null to not register it in the help.
		 * @param {string|Array<string>|Object} names The names/aliases of the command. If there is a need for hidden aliases (not shown in the help), it should be an object like this: {names: string|Array<string>, hidden: Array<string>}
		 * @param {string} params A description of the parameters.
		 * @param {string} description The description of the command.
		 * @param {function} command The command that will be executed. It should take a Message as its first argument, and an array of strings as its second (the parameters).
		 * @param {object} options Options.
			* @param {boolean} options.inline Whether to make that inline in the help or not.
			* @param {boolean} options.masterOnly Whether this command is reserved to the master.
		 *
		 * @throws {TypeError} if 'names' is empty, or not a string or array of non-empty strings, or if 'command' is not a function.
		 * @throws {RangeError} if the category is not null but does not exist.
		 */
		register(category, names, params, description, command, {inline, masterOnly} = {}) {
			if(!names || names instanceof Array && !names.length)
				throw new TypeError("'names' cannot be empty.");

			let hiddenAliases = names.hidden || [];
			if(hiddenAliases && typeof hiddenAliases === "string")
				hiddenAliases = [hiddenAliases];
			if(hiddenAliases.length)
				names = names.names;

			if(typeof names === "string")
				names = [names];
			if(!(names instanceof Array) || names.some(n => !n || typeof n !== "string"))
				throw new TypeError("'names' must be a string or array of non-empty strings.");

			if(typeof command !== "function") // was params ommitted?
			{
				({inline, masterOnly} = command || {});
				command = description;
				description = params;
				params = null;
			}

			if(typeof command !== "function")
				throw new TypeError("'command' should be a function.");

			command.masterOnly = masterOnly;

			for(const name of [...names, ...hiddenAliases])
				cmds[name.trim().toLowerCase()] = command;

			const [name, ...aliases] = names;
			const cmdData = { name, aliases, params, description, inline, masterOnly };

			if(category)
			{
				const categoryInfos = cmdsInfos.get(category);
				if(!categoryInfos)
					throw new RangeError(`The category '${category}' does not exist.`);

				categoryInfos.commands.set(name, cmdData);
			}
			else if(masterOnly)
				masterInfos.set(name, cmdData);
		},
	});
}
