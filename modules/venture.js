"use strict";

const { existsSync, readFileSync, promises: {writeFile} } = require("fs");
const splitMessage = require("../utils/splitMessage");
const { getMember, getUser, sendToMaster } = require("../discordCore");
const { don, genre: {_e, er}, tn } = require("./zouz");
const { rand } = require("../utils"); // est utilisé par le code importé
const sauvegardes = require("../savedData/DataSaver.class")("Venture", {
	victoires: [],
	lastText: "",
	next: null,
	vars: {}
});
const idsVictoires = [];
let victoires = {};
let listeVictoires;

const notServerCommands = ["nouvelle", "commence", "recommence", "rappel", "reset", "reset, connard !", "reset, connard !"];

let lv, lr, lrd, lrc, lrm, le, lem, lec, ln; // locale
let fonctions, vars;
let initialisé = false;
let currentMsg;



const commands = require("../commands");

commands.registerCategory(["⚔", "venture"], {
	nom: "La Venture !",
	color: 0xFFF8DC,
	thumbnail: { url: "https://cdn.discordapp.com/emojis/506930667322343444.png?size=96&quality=lossless" },
	title: "La Venture !",
	description: `La meilleure aventure textuelle de l’Histoire débarque sur Discord ! Envoyez-moi \`>venture comment jouer ?\` en message privé pour commencer à jouer !\n(merci de prévenir <@${require("../discordCore").auth.master}> en cas de bug, faute d’orthographe, ou quoi que ce soit qui vous semblerait étrange)\nCommandes :`,
	shortDescription: "Une aventure textuelle pour passer le temps",
});

function venturify(name) { return name instanceof Array ? name.map(venturify) : ("venture "+name); }
function register(names, ...args)
{
	if(names.hidden)
	{
		names.hidden = venturify(names.hidden);
		names.names  = venturify(names.names );
	}
	else
		names = venturify(names);

	for(let i = args.length - 1 ; i > 0 ; i--)
	{
		if(typeof args[i] === "function")
		{
			const cmdHandler = args[i];
			args[i] = (msg, ...args) => {
				if(initialisé)
					cmdHandler(msg, ...args);
				else
					msg.reply(initialisé === null
					? "J’ai foiré le chargement de la Venture. Désolé ;-;"
					: "La Venture n’a pas encore été initialisée. Merci de réessayer un peu plus tard");
			}
			break;
		}
	}

	commands.register("⚔", names, ...args);
}

const masterOnly = true, inline = true;
const {donne: aide} = require("./aide");

register({names: "", hidden: ["?", "aide", "presentation", "présentation"]},
"Présente la Venture.",
(msg, cmd) => {
	cmd = cmd.join(" ").toLowerCase();
	if(cmd.startsWith("donnevictoires"))
		cmd = "donnevictoires";

	if(!cmd || ["?", "aide", "presentation", "présentation"].includes(cmd))
		aide(msg.channel, "⚔");
	else if(msg.guild && notServerCommands.includes(cmd))
		msg.channel.send("Pour éviter le flood, on ne peut jouer qu’en messages privés.");
	else if(commands("venture "+cmd.toLowerCase(), msg) === null)
		msg.channel.send("Commande inconnue.");
}, {inline});


register({names: "chargée?", hidden: ["chargée", "chargee?", "chargee"]},
"Indique si la Venture a chargé (99,9% du temps, oui) et s’il est possible de jouer.",
({channel}) => channel.send(initialisé === null ? "Non, j’ai foiré. Désolé ;-;"
				: initialisé === false ? "Non, je suis en train."
				: "Ouaip, c’est bon."),
{inline});

register(["charge", "recharge"],
"(Re)charge la Venture.",
msg => charge().then(ok => ok && msg.reply("C’est fait !")), // Sinon la fonction "zut" se charge déjà de dire désolé
{masterOnly, inline});


register({names: "commentJouer?", hidden: ["commentJouer", "commentjouer?", "commentjouer ?", "comment", "comment jouer?", "comment jouer ?"]},
"Explications sur le jeu. Utilisable sur les serveurs.",
({channel}) => channel.send(`La Venture est un jeu textuel où vous incarnez un pignouf. Je vous décrirai une situation, et vous proposerai plusieurs actions à prendre. Il y a quinze manières différentes de finir le jeu, si l’on ne compte pas les innombrables manières de mourir ; et certaines de ces fins ont plusieurs manières d’être atteintes, donc n’hésitez pas à essayer toutes les routes possibles :)
Le jeu a également une certaine part d’aléatoire, donc même essayer de prendre exactement la même route peut aboutir à un résultat légèrement différent.
Quand je vous donne des choix, envoyez-moi simplement le numéro de l’action que vous voulez envoyer.
Pour commencer une nouvelle partie, envoyez \`>venture nouvelle\`, \`>venture commence\` ou \`>venture recommence\` (ces trois commandes font la même chose)`));


register(["nouvelle", "commence", "recommence"],
"Commence une nouvelle venture.",
msg => {
	currentMsg = msg;
	reinitialise("go");
}, {inline});

register(["rappel", "rappelle"],
"Répète la situation dans laquelle se trouve votre personnage, et les choix qui lui sont offerts.",
({author: {id}, channel}) => {
	if(sauvegardes.has(id))
	{
		const sauvegarde = sauvegardes.get(id);
		show(sauvegarde.lastText, sauvegarde.next, channel);
	}
	else
		channel.send("Vous n’avez aucune venture en cours.");
}, {inline});


register("victoires",
"Affiche les victoires que vous avez réussi à obtenir. Utilisable sur les serveurs, pour que vous puissiez vous la péter auprès de vos amis.",
(msg) => {
	const sauvegarde = sauvegardes.get(msg.author.id);
	let message;

	if(!sauvegarde)
		message = " n’as jamais essayé de jouer, comment aurais-tu la moindre victoire ?..";
	else
	{
		const {victoires: v} = sauvegarde;
		v.affiche = function() {
			return this.map(v => victoires[v]).join(", ");
		}
		const nv = v.length;
		message = nv === 0 ? " n’as aucune victoire, LOOOOOL"
			: nv === 1 ? ` as une seule victoire : ${victoires[v[0]]}. C’est un début.`
			: nv  <  6 ? ` as ${nv} victoires : ${v.affiche()}. C’est pas mal.`
			: nv  < 11 ? ` as ${nv} victoires : ${v.affiche()}. Tu l’aimes ce jeu, hein ?`
			: nv < victoires.length ? ` as ${nv} victoires : ${v.affiche()}. Tu y es presque !`
			: ` as __toutes__ les victoires ! C’est incroyable ! ${v.affiche()}. Il n’en manque aucune ! Tu es un${_e(msg.author)} véritable Venturi${er(msg.author)}.`;
	}

	msg.reply((msg.guild ? "tu" : "Tu") + message);
});

register("routes",
"Affiche les routes existantes et le nombre de victoires pour chaque. Utilisable sur les serveurs.",
({channel}) => channel.send("Il y a la route des humains qui comporte 7 victoires, la route des elfes qui en a 5, et la route des nains qui en a 3, pour un total de 15."),
{inline});

register({names: "listeVictoires", hidden: ["liste victoires", "victoires?", "victoires ?"]},
"Affiche la liste des victoires possibles. **N’est pas** utilisable sur les serveurs pour éviter le spoil.",
({channel}) => channel.send(listeVictoires),
{inline});

register({names: "Venturiers", hidden: ["venturier·e·s", "venturièr·e·s"]},
"Affiche la liste des véritables Venturièr·e·s, c’est-à-dire celleux qui sont parvenu·e·s à obtenir toutes les victoires. Utilisable sur les serveurs.",
c_venturiers);
commands.register(null, ["venturiers", "venturier·e·s", "venturièr·e·s"],
"", c_venturiers);
async function c_venturiers({channel, guild}) {
	const nVictoires = victoires.length;
	const venturiers = [];
	let unknown = 0;

	for(const [id, venturier] of sauvegardes)
	{
		if(venturier.victoires.length === nVictoires)
		{
			let user = await (guild ? getMember(guild, id) : getUser(id));
			if(user)
				venturiers.push(tn(user, true, true, {nom: "**"}));
			else
				unknown++;
		}
	}

	if(unknown)
		unknown = `${unknown} personne${unknown === 1 ? "" : "s"} dont je n’ai pas réussi à retrouver le nom.`

	splitMessage(
		venturiers.length === 0 ? `Il y a aucun·e véritable Venturièr·e pour l’instant${unknown ? `, hormis ${unknown}` : "."}`
		: venturiers.length === 1 ? `Il n’y a qu’un·e seul·e véritable Venturièr·e, et c’est ${venturiers[0]} !${unknown ? `\nQuoi qu’il y a aussi ${unknown}` : ""}`
		: `Il y a ${venturiers.length} véritables Venturièr·e·s à ce jour :\n${venturiers.join("\n")}${unknown ? `\nIl y a aussi ${unknown}` : ""}`
	)
		.forEach(channel.send.bind(channel));
};


register("donneVictoires", "@mentions",
"Donne toutes les victoires aux personnes mentionnées.",
(msg) => {
	if(!msg.mentions.users.first())
		return msg.channel.send("Vous devez mentionner les futures Venturièr·e·s.");

	for(const [id, player] of msg.mentions.users)
	{
		if(player.bot)
			continue;
		sauvegardes.get(id).victoires = idsVictoires;
		sauvegardes.save();
	}

	msg.react("✅");
}, {masterOnly});

register({names: "reset", hidden: ["reset, connard !", "reset, connard !"]}, // espace insécable
"Réinitialise tout votre profil de venturier·e. Progression, victoires, tout... Ce sera effacé.",
({content, author}) => {
	if(content.includes("connard"))
	{
		sauvegardes.delete(author.id);
		author.send("Vous avez été **effacé.**");
	}
	else
		author.send(`Vous êtes sûr${_e(author)} ? C’est irréversible. Tapez \`>venture reset, connard !\` pour confirmer.`);
});


module.exports = {
	advance: function(msg) {
		let n = ~~msg.content, data = sauvegardes.get(msg.author.id);

		if(!data || !data.next)
		{
			msg.reply("Vous n’avez aucune venture en cours.");
			return;
		}

		if(n < 1 || n > data.next.length)
			msg.reply(`Il n’y a pas de choix n°${n}.`);
		else
		{
			let next = data.next[n-1], cmd;

			if(!next)
			{
				sendToMaster(`Bug de la Venture à cause de ${msg.author}. Voir la console.`);
				console.error("n: ", n, "data: ", data);
				msg.reply("Erreur dans la Venture, désolé du désagrément. Veuillez réessayer plus tard (genre dans quelques heures, ou demain).");
				return;
			}

			cmd = "fonctions." + next[1] + '(';

			if(next.length === 3)
				cmd += `"${next[2]}", `;

			cmd += ");";
			currentMsg = msg;
			vars = data.vars;
			eval(cmd);
		}
	}
};



function show(texte, next, channel)
{
	let i, nextTxt = "";

	if(next)
	{
		for(i = 0 ; i < next.length - 1 ; i++)
			nextTxt += (i+1) + ". " + next[i][0] + "\n";

		nextTxt += next.length + ". " + next[next.length - 1][0];
	}

	if(typeof texte === "string")
		channel.send(texte);
	else for(const line of texte)
		channel.send(line);

	if(nextTxt)
		channel.send(nextTxt);
}


function update(texte, next)
{
	const d = sauvegardes.get(currentMsg.author.id);
	d.lastText = texte;
	d.next = next; // To avoid putting empty strings in
	show(texte, next, currentMsg.channel);

	sauvegardes.save();
}

function reinitialise(pourquoi)
{
	if(pourquoi === "mort")
		return lv.ded.random();
	else if(pourquoi === "prepare")
		return "";
	// autre valeur possible : "go"
	update(["**Bienvenue dans la Venture ! (c 2 langlé lol)**",
			 "Voici le scénario : vous vous réveillez au milieu de la forêt. Vous avez un sandwich au jambon dans la poche."],
			[[lv.sandwich_miam, "sandwich", "miam"],
		 	 [lv.sandwich_detruire, "sandwich", "detruire"],
		 	 [lv.sandwich_pangolin, "sandwich", "donner"]]);
}


function trieVictoires(a, b)
{
	return idsVictoires.indexOf(a) > idsVictoires.indexOf(b);
}

function victoire(type)
{
	let texte;
	let v = lv.victoires.humains[type] || lv.victoires.elfes[type] || lv.victoires.nains[type];

	if(type === "menaceMiroir")
	{
		v = lv.victoires.humains.miroir;
		texte = [v.menace, ...v.texte];
		type = "miroir";
	}
	else if(v)
		texte = v.texte;
	else
	{
		v = lv.victoires.nains.rahklur;
		texte = v.texte.map(ligne => ligne.replace("{{NAME}}", type));
		type = "rahklur";
	}

	let concl = "**" + lv.congratulations.replace("{{VICT}}", v.nom) + "**";

	const victoiresEues = sauvegardes.get(currentMsg.author.id).victoires;
	const déjàEue = victoiresEues.includes(type);

	if(déjàEue)
		concl += lv.congrats_alreadyHad;
	else
	{
		victoiresEues.push(type);
		victoiresEues.sort(trieVictoires);
		sauvegardes.save();
	}

	concl += "\n" + lv.congrats_count.replace("{{COUNT}}", victoiresEues.length);

	if(!déjàEue && victoiresEues.length === victoires.length)
	{
		concl += "\n\n**Vous avez fini la Venture ! Pour cet exploit, tu as gagné 10 000 ƶ.**\nSi tu en veux plus, rends-toi à cette adresse : https://brylan.fr/games/La%20Venture/\nC’est strictement la même chose que ce que tu viens de faire, ce pourquoi je te donne la technique : clique en bas à droite. On te demandera un mot de passe, qui est `balabamba`. Et paf, le mode ORC vient d’apparaître en haut à droite ! Amuse-toi bien.";
		don(10000, currentMsg.author.id);
	}

	// comme texte = v.texte, on ne doit pas push sur texte au risque de polluer la trad
	update(texte instanceof Array ? [...texte, concl] : [texte, concl], null);
}




//////////////////////////////////// IMPORTATION DU CODE ////////////////////////////////////

const root = "https://brylan.fr/games/La%20Venture/";
const cache = { lang: sauvegardes.folder + "/fr.json", funcs: sauvegardes.folder + "/Venture.js" };


charge();


function zut(err)
{
	initialisé = null;
	let usedCache = existsSync(cache.lang) && existsSync(cache.funcs);

	console.error(err);
	let fin = "";

	if(usedCache)
	{
		try {
			loadTranslation(require(cache.lang));
			loadFunctions(readFileSync(cache.funcs, {encoding: "utf-8"}));
			fin = "Du coup j’ai chargé le cache à la place.";
			initialisé = "cache";
		}
		catch(e) {
			usedCache = false;
			error(e);
			fin = "J’ai essayé de charger le cache mais j’ai foiré ça aussi. ";
		}
	}

	if(!usedCache)
		fin += "Désolé ;-;";

	sendToMaster("J’ai foiré le chargement de la Venture. " + fin);

	return false;
}


function charge()
{
	return Promise.all([
		fetch(root+"lang/fr.js"),
		fetch(root+"game/venture.js"),
	]).then(async files => {
		for(const file of files)
			if(!file.ok)
				return zut(file);

		const [lang, funcs] = files;

		let locale;
		eval((await lang.text()).replace("const l = Object.freeze", "locale = "));
		delete locale.Nouventure;
		traiter(locale);
		function traiter(ligne) {
			if(ligne instanceof Array)
				return ligne.map(traiter);
			else if(ligne instanceof Object)
			{
				for(const [key, val] of Object.entries(ligne))
					ligne[key] = traiter(val);
				return ligne;
			}
			else
				return ligne.replace(/\*/g, "\\*").replace(/<\/?em>/g, "_").replace(/<\/?strong>/g, "**").replace(/<\/?strike>/g, "~~").replace(/<br ?\/?>/g, "\n");
		}
		writeFile(cache.lang, JSON.stringify(locale)).catch(cachingError);
		loadTranslation(locale);

		const functions = (await funcs.text())
			.replace(/function ([a-zA-Z0-9]*)/g, "fonctions.$1 = function");
		writeFile(cache.funcs, functions).catch(cachingError);
		loadFunctions(functions);

		console.info("Venture initialisée.");
		return initialisé = true;
	})
	.catch(zut);
}

function cachingError(err)
{
	error(err);
	splitMessage("J’ai pas réussi à mettre la Venture en cache :\n" + err.message)
		.forEach(sendToMaster);
}

function loadTranslation(l)
{
	lv = l.Venture;
	lr = l.roi; lrd = lr.dragon; lrc = lr.collier; lrm = lr.miroir;
	le = l.elfes; lem = le.maison; lec = le.ceremonie;
	ln = l.nains;

	idsVictoires.length = 0;
	listeVictoires = {};

	for(const [nom, route] of Object.entries(lv.victoires))
		idsVictoires.push(...(listeVictoires[nom] = Object.keys(route).filter(v => v !== "titre")));

	const {humains, elfes, nains} = lv.victoires;
	victoires = {};
	Object.defineProperty(victoires, "length", {value: idsVictoires.length});

	for(const v of idsVictoires)
		victoires[v] = (humains[v] || elfes[v] || nains[v]).nom;


	function victsList(list) {
		list = list.map(v => `“${victoires[v]}”`);
		const last = list.pop();
		return list.join(", ") + " et " + last;
	}
	listeVictoires = `La route des **humains** comporte ${listeVictoires.humains.length} victoires : ${victsList(listeVictoires.humains)}.
La route des **elfes** comporte ${listeVictoires.elfes.length} victoires : ${victsList(listeVictoires.elfes)}.
Enfin, la route des **nains** en a ${listeVictoires.nains.length} : ${victsList(listeVictoires.nains)}.`;
}

function loadFunctions(funcs)
{
	fonctions = { victoire };
	eval(funcs);
}
