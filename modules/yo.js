"use strict";

const { fetchMaster } = require("../discordCore");

const historique = new Map();

const messagePasMis = "Ce message a pas été mis lol";

/**
 * Les réponses dites "génériques" sont déclenchées si au moins une des expression de la première liste est présente.
 * Si une réponse a déjà été donnée dans le même salon il y a moins d'une heure, une réponse est choisie au hasard dans la troisième liste, et sinon dans la deuxième.
 *
 * Les réponses dites "heuristiques" vérifient que au moins x expressions de chaque sous-liste de la première liste sont présentes, x étant le 1er élément de la sous-liste.
 * Si une réponse a déjà été donnée blablabla, troisième liste, sinon la deuxième.
 */
var réponses = {
	// Pas besoin de code spécial, c'est juste un cas pour lequel il n'y a aucune réponse
	interdictions: { triggers: ["marre", "ta gueule", "tg", /(ferme|boucle)[ -]l[aà]/, /tais[ -]toi/, "ça va pas", /passe[ -]lui/] /*le bonjour*/ },

	// Génériques
	auRevoir: {
		triggers: [/(j'y|je m'en) vais([ ,][^p].*|$)/, /je pars([ ,][^ps].*|$)/, /(^| )à plus(( [^d])|$)/, "au revoir"], // j'y vais p[...] pour ignorer "j'y vais pas"
		replies: ["Salut.", "À tout’.", "À plus."],
	},

	helloThere: {
		triggers: ["hello there"],
		replies: ["Général Kenobi."],
	},

	bonjour: {
		triggers: ["bonjour", "salule", "bonjoir", /(^| )yo( |\.|!|$)/, "hello", "heya"], // Yo est comme ça pour qu'il ne réponde pas à "youtube".
		replies: ["Yo !", "Bonjour !", "Bien le bonjour."],
		repeats: ["Ouais, ouais, bonjour.", "Mais oui, mais oui, salut."],
	},

	bonsoir: {
		triggers: ["bonsoir"],
		replies: ["Bonsoir.", "Bonsoir !"],
	},

	nenuit: {
		triggers: ["bonne nuit", "nenuit", /je vais (me coucher|dodo|au lit|au pieu|au plumard)/],
		replies: ["Bonne nuit !", "Dors bien !", "À demain."],
	},

	hugu: {
		triggers: [/(câlin|hug[u ]).*(((le|au) bot)|strun)/, /(((le|au) bot)|strun).*(câlin|hug(u|\s|$))/],
		replies: ["Nan, je fais pas de câlin.", "Je ne câline personne, moi."],
		repeats: ["J’ai dit **non !**", "J’ai dit pas de câlin, dégage !", "Arrière !", "Mais dégage !", "Va chier !"],
	},

	faim: {
		triggers: ["j'ai faim", "j'ai très faim", "j'ai trop faim"],
		replies: ["Mange ta main.", "Mange ta main.", "Eh bah mange tes morts !"],
	},

	ennui: {
		triggers: ["je m'ennuie", /je m'emmerde( [^àa]|$)/, /je me fais chier( [^àa]|$)/],
		replies: ["Tu veux faire un pierre-feuille-ciseaux ?", "Une petite Venture ?"],
	},

	tartiflette: {
		triggers: ["tartiflette"],
		replies: ["Vous avez dit tartiflette ?!", "Owi, tartiflette ! :yum:"],
		repeats: ["Arrêtez de parler de tartiflette ou donnez-m’en !", "Vous me donnez faim, suffit !"],
	},

	cheh: {
		triggers: ["cheh"],
		replies: ["Cheh !", "https://cdn.discordapp.com/emojis/740460836174430228.png", "https://cdn.discordapp.com/emojis/740460836174430228.png", "https://www.youtube.com/watch?v=9M2Ce50Hle8", "https://tenor.com/view/ha-cheh-take-that-gif-14055512"],
	},


	// Heuristiques
	çaVa: {
		...heuristic(
			[1, "ça va", "vous allez bien", "tu vas bien"],
			[2, "strun", "?"],
		),
		replies: ["Ça va.", "Ouais, ouais.", "Ouais."],
		repeats: ["Mais oui, vous dis-je !", "J’ai déjà répondu à ça."],
	},
	commentVa: {
		...heuristic(
			[1, "comment vas-tu", "comment allez-vous"],
			[2, "strun", "?"],
		),
		replies: ["Ça va.", "Pas mal.", "On fait aller."],
		repeats: ["Mais oui, ça va !", "J’ai déjà répondu à ça."],
	},

	créateurice: {
		...heuristic(
			[1, "strun"],
			[1, "qui"],
			[1, "créateur", "créateur", "créé"],
		),
		replies: [messagePasMis],
		repeats: [messagePasMis],
	},

	tki: {
		...heuristic(
			[1, "strun"],
			[1, /qui.*est?|est?.*qui/, /[aà] quoi.*ser(s|t)|que.*fai(s|t|re)|(ser(s|t)|fai(s|t|re)) ([aà] )?quoi/],
		),
		replies: ["Fais `>tki` et je te répondrai."],
		repeats: ["J’ai déjà répondu à ça, non ?", "Je l’ai dit, il faut faire `>tki` pour savoir qui je suis. Ou `>présentation`."],
	},

	ilParle: {
		...heuristic(
			[0, /arr[eéèê]te/],
			[1, "parle", /[^aà] cause/],
			[1, "strun", "bot", "celui-là"],
			[1, "wait", "attend", "tiens", "maintenant"],
		),
		replies: ["Ouaip. Ça t’étonne ?", "Eeeeh ouais, je cause.", "Oh oui, je dis des choses avec ma bouche."],
		repeats: ["Oui, oui, je parle, c’est bon.", "Oui, je parle, on sait.", "Ouais, je cause, je dois le dire en quelle langue ?\nSachant que je vous préviens, je ne parle que français."],
	},

	nEstCePas: {
		...heuristic(
			[1, "strun"],
			[1, /pas vrai.*\?/, "n'est-ce pas"],
		),
		replies: ["Ouais, si tu veux, je m’en fous.", "On va dire que oui, je m’en fiche.", "Oui, non, je sais pas, je m’en tape."],
	},

	merci: {
		...heuristic(
			[1, "strun"],
			[1, "merci", "cimer"],
		),
		replies: ["De rien."],
	},

	non: {
		...heuristic(
			[1, "strun"],
			[1, "non"],
		),
		replies: ["Strun, si !"],
	},

	// Pour éviter "réponses is not iterable" quand fetchMaster n'a pas encore accompli
	[Symbol.iterator]() { return Object.values(this); }
};

function test(text, trigger) {
	return trigger instanceof RegExp ? trigger.test(text) : text.includes(trigger);
}
function triggered(text)
{
	return this.triggers.some(test.bind(null, text));
}
function heuristic(...triggers)
{
	return {
		triggers: triggers.map(([required, ...triggers]) => {
			// Soit on demande un ou plus, et on vérifie qu'il y a 1 ou plus. Soit on demande 0, et on vérifie qu'il y a 0.
			triggers.refuse = required ? n => n < required : Boolean;
			return triggers;
		}),
		triggered: heuristicTriggered,
	};
}
function heuristicTriggered(text)
{
	for(const triggers of this.triggers)
	{
		const { length } = triggers.filter(test.bind(null, text));
		if(triggers.refuse(length))
			return false;
	}
	return true;
}


for(const réponse of Object.values(réponses))
{
	if(réponse.triggered) // Les heuristiques, plus précises, ont prio
		réponse.weight = 1;
	else
	{
		réponse.weight = 0.8;
		réponse.triggered = triggered;
	}
}
réponses.interdictions.weight = 1.1;
réponses.bonjour.weight = 1;
réponses.nenuit.weight = 1;
réponses.faim.weight = 1;
réponses.tartiflette.weight = 1;
réponses.hugu.weight = 0.9;
réponses.cheh.weight = 0.75;


var myId;
fetchMaster().then(master => {
	myId = master.client.user.id;

	const créateurice = require("./zouz").tn(master, true);
	réponses.créateurice.replies = [`J’ai été créé par ${créateurice}.`, `C’est ${créateurice} qui m’a créé.`, `Je suis né des mains de ${créateurice}.`];
	réponses.créateurice.repeats = [`Je te dis que c’est ${master.username} qui m’a créé.`, `Tu te fous de moi ? Je viens de le dire.`];

	réponses = Object.entries(réponses).sort(([,a], [,b]) => b.weight - a.weight);
});


function déjàDit(channelId, code)
{
	return historique.has(channelId)
		? Date.now() - historique.get(channelId)[code] < 3600_000
		: false;
}

function send(channel, msg)
{
	setTimeout(() => {
		channel.sendTyping().catch(Function());
		setTimeout(() => channel.send(msg),
			(msg.length * (0.1 + Math.random() / 20) + 0.1) * 1000
		);
	}, Math.random() * 1000 + 200);
}


exports.analyse = msg => {
	let { content } = msg;

	if(content.length > 50)
		return false;

	const text = content.toLowerCase();
	const mention = text.includes("strun") || text.includes(myId);

	if(!mention && Math.random() < 0.15)
		return false;

	const { channel, channel: { id: cId } } = msg;

	function react(code, replies)
	{
		send(channel, replies.rand());
		const histo = historique.get(cId);
		if(histo) histo[code] = Date.now();
		else historique.set(cId, { [code]: Date.now() });
	}

	for(const [code, réponse] of réponses) // Object.entries done in the fetchMaster().then
	{
		if(Math.random() > réponse.weight)
			return;

		if(réponse.triggered(text))
		{
			if(déjàDit(cId, code))
			{
				if(réponse.repeats && (!msg.guild || mention))
				{
					react(code, réponse.repeats);
					return true;
				}
			}
			else if(réponse.replies)
			{
				react(code, réponse.replies);
				return true;
			}
			return false;
		}
	}
};
