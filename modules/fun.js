"use strict";

const commands = require("../commands");

commands.registerCategory(["🎉", "sympa", "sympas"], {
	nom: "Les sympas",
	color: 0x1166DD,
	title: "Commandes sympas",
	description: "J’ai quelques commandes ni très utiles, ni très strunesques, juste sympatoches.",
	shortDescription: "Quelques commandes ni très utiles, ni très strunesques, juste sympatoches.",
});

const {donne: aide} = require("./aide");
commands.register(null, ["fun", "sympa", "sympas"], "",
({channel}) => aide(channel, "🎉"));

const register = exports.register = commands.register.bind(null, "🎉");
const inline = true;

const { licence, insulte } = require("../utils/Strun");
const {
	client, getMaster, getMember,
	PermissionFlags: { MANAGE_MESSAGES, MANAGE_WEBHOOKS },
} = require("../discordCore");
const { genre: {_e} } = require("./zouz");


register("licence",
"Affiche la licence du bot.",
({channel}) => channel.send(licence),
{inline});


require("../utils");

// Référence au Bourgeois Gentillhomme
register({names: ["marquise", "mélange"], hidden: ["melange", "mélanger", "melanger"]}, "<phrase>",
"Mélange les mots d’une phrase. S’il n’y a qu’un seul mot, ses lettres sont mélangées à la place.",
({channel}, mots) => {
	if(!mots?.length)
		channel.send("Et je mélange quoi, exactement ?");
	else
	{
		mots[0] = mots[0].toLowerCase();

		if(mots.length === 1)
			channel.send(mots[0].split("").shuffle().join(""));
		else
		{
			const phrase = mots.shuffle().join(" ");
			channel.send(phrase[0].toUpperCase() + phrase.substring(1));
		}
	}
});


const spoy = require("./moderation/spoy");

register("imite", "<@mention> <message>",
"Envoyez un message comme s’il venait de la personne mentionnée !",
async (msg, [idCible = "", ...message]) => {
	const {author, member, channel, guild} = msg;
	if(!guild)
		channel.send(`Il faut être sur un serveur, ${insulte(author)} !`);
	else
	{
		message = message.join(" ");
		const cible = msg.mentions.members.first() || await getMember(guild, idCible);
		const myself = guild.members.resolve(client.user);
		const masterId = getMaster().id;

		if(!cible)
			channel.send(`Tu es censé${_e(author)} mentionner quelqu’un, patate !`);
		else if(cible.id === masterId && author.id !== masterId)
			channel.send("Imiter mon maître ? Haha, **non.**");
		else if(cible.id === myself.id && author.id !== masterId)
			channel.send(`Je ne vais pas m’imiter tout seul, ${insulte(author)} !`);
		else if(!message)
			channel.send("Tu dois aussi indiquer un message, banane.");
		else if(!myself.permissionsIn(channel).has(MANAGE_WEBHOOKS))
			channel.send("Ah, heu, ici, je ne peux pas. Dans un autre salon, peut-être ?");
		else
		{
			if(myself.permissionsIn(channel).has(MANAGE_MESSAGES))
			{
				spoy.ignoreMessage(msg);
				msg.delete();
			}

			// In PNG because Discord turns webp to JPG for some reason
			channel.createWebhook({ name: cible.displayName, avatar: cible.user.displayAvatarURL({format: "png"}), reason: `Pour imiter ${cible.user.tag}, à la demande de ${author.tag}.` })
			.then(webhook => webhook.send(message)
			.then(whMsg => webhook.delete(`J’ai fini d’imiter ${cible.tag}.`)))
			.catch(err => {
				if(err.status === 403)
					channel.send("Ah, heu, ici, je ne peux pas. J'ai besoin de pouvoir créer un webhook.").catch(Function());
				else if(err.status !== 404)
					error(err);
			});
		}
	}
});



const {
	invention,
	horaire,
	benedict,
	kebab,
} = require("./generators");

register({names: "invention", hidden: "invente"},
"Lance des « dés d’invention ». Faites `>invention ?` pour avoir une explication.",
({channel}, [prm]) => {
	if(prm && ["?", "aide", "help", "info", "infos", "explication", "explications"].includes(prm.toLowerCase()))
		channel.send({embeds: [invention.embed]});
	else
		channel.send(invention());
}, {inline});

register(["sorcierHoraire", "sorcier"],
"Recevez un nom de Sorcier Horaire !",
({channel}) => channel.send(horaire() + " !"),
{inline});

register({names: "benedict", hidden: ["cumberbatch", "benadryl", "pumpkinpatch"]},
"Récupérez le nom de cete acteur, là, Benadryl Pumpkinpatch... Bandicoot Scobysnack... Raah, je sais plus.",
({channel}) => channel.send(benedict() + " !"),
{inline});

register("kebab",
"KEBAB",
({channel}) => channel.send(kebab()));


function basic(names, txt) {
	register(names, txt, ({channel}) => channel.send(txt), {inline});
}
basic("ohWell", "¯\\_(ツ)_/¯");
basic({names: "lenny", hidden: "lennyFace"}, "( ͡° ͜ʖ ͡°)");
basic({names: "désapprobation", hidden: ["desapprobation", "désapprouve", "desapprouve", "disapproval"]}, "ಠ_ಠ");
