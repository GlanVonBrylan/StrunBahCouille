"use strict";

const splitMessage = require("../../utils/splitMessage");
const util = require("../../utils");
const {
	client,
	fetchMaster, checkMaster,
	getUser, getMember,
} = require("../../discordCore");
const {
	insulte,
	voyelles,
} = require("../../utils/Strun");

const {isIgnored} = require("../moderation/spoy");
const crisseBarbes = [];

for(const nom in require("../../utils/Strun").crisseBarbes)
	crisseBarbes.push(crisseBarbes[nom]);


Number.prototype.toZouz = function() {
	return `${this.toLocaleString("fr")} ƶ`;
}

const prix = { respect: 5000, VIP: 15000, titres: 4000, changeTitre: 10 };
const seuil = { pauvreté: 1000, richesse: 50000 };
const Z = "Ƶƶ";
const titres = [
	["granguin·e", "kleptride", "brabandou"],
	["archi-granguin·e", "kleptride en chef", "barabradarandou"],
	["grandaron·ne", "ludomagister", "micholangalo", "saint·e", "Va"],
	["Ecce Homo", "Pansophos", "Vi"],
	["Von", "Sacro-Saint·e", "Iklenas"],
];
// Variable car il est complété un peu plus loin
var titres_infos = `Il existe cinq rang de titres. Acheter un titre coûte ${prix.titres.toZouz()}, multiplié par son rang (jusqu’à ${(prix.titres * 5).toLocaleString("fr")} donc). Vous pouvez changer le titre que vous portez pour la modique somme de ${prix.changeTitre} ƶ. Vous pouvez également créer un titre personnalisé pour ${(prix.titres * 10).toZouz()}. Vous ne pouvez posséder qu’un titre personnalisé, mais le changer ne coûte que ${(prix.titres * 5).toZouz()}.
Notez bien que pour les riches c’est plus cher.
**Titres disponibles :**\n`;

const titres_infos_embed = {
	color: 0xF9EE4E,
	title: "Commandes relatives aux titres",
	fields: [],
};
var titres_infos_embedDF = null; // titres_infos_embed Default Fields
const titres_rangs = new Map();

const insultes = ["plébéien·ne", "pauvre", "bobo gauchiste", "hippie", "clodo", "anarchiste"];
const hilarant = ["Très drôle.", "Hilarant.", "Désopilant.", "Quel humour.", "Qu’iel est comique !", "Que c’est cocasse !"];

const lastMsgs = {};

var master;
fetchMaster().then(m => { master = m; titres_infos_embedDF = titresEmbed(">", true).fields; });

const zouz = require("../../savedData/DataSaver.class")("zouz", { zouz: 100 });
zouz._clear = zouz.clear;
zouz.clear = function() {
	const bank = this.get("bank");
	this._clear();
	this.set("bank", bank);
}

if(!zouz.has("topGlobal"))
{
	const bank = zouz.get("bank");
	zouz.quickDelete("bank", false);
	const topGlobal = Array.from(zouz).map(([id, {zouz}]) => ({id, zouz}));
	topGlobal.sort((a, b) => b.zouz - a.zouz);
	topGlobal.length = 20;
	zouz.assign({ bank, topGlobal });
}

zouz.update = function(id) {
	const z = this.get(id).zouz;
	const topGlobal = this.get("topGlobal");
	let alreadyInIt = false;

	// Je sais qu'avec ce système, un type qui devrait sortir du top global va à la place y rester, en dernière position...
	// Mais si je charge tout pour cherche le nouveau dernier, ça fout en l'air tout l'intérêt de ce truc...
	// Ça se mettra à jour tout seul dès que le vrai dernier écrira un truc de toutes façons, donc ça ira pour l'instant.
	for(const zz of topGlobal)
	{
		if(zz.id === id)
		{
			alreadyInIt = true;
			zz.zouz = z;
			topGlobal.sort((a, b) => b.zouz - a.zouz);
			break;
		}
	}

	if(!alreadyInIt) for(let i = 0 ; i < 20 ; i++)
	{
		if(!topGlobal[i] || z > topGlobal[i].zouz)
		{
			for(let j = 19 ; j > i ; j--)
				topGlobal[j] = topGlobal[j-1];

			topGlobal[i] = { id, zouz: z };
			break;
		}
	}

	this.save();
}


if(!zouz.has("bank"))
	zouz.set("bank", 0);


const titres_cmds = [
	["", "Affiche les infos sur les titres, lol."],
];

function titresEmbed(p, first)
{
	if(!p)
		p = ">";

	const prefix = p;

	p += "zouz titres ";

	titres_infos_embed.fields = (prefix === ">" && !first)
		? titres_infos_embedDF
		: [
			...titres_cmds.map(([name, desc]) => ({ name: p+name, value: desc.replace(/>/g, prefix) })),
			{ name: prefix+"zouz titre [@mention]", value: "Vous permet de voir le titre de la personne mentionnée (ou le vôtre si vous ne mentionnez personne)." },
		];

	return titres_infos_embed;
}


for(let i = 0 ; i < titres.length ; i++)
{
	titres_infos += `**Rang ${i+1} :** ${titres[i].join(", ")}\n`;
	for(const t of titres[i])
		titres_rangs.set(t, i+1);
}



function insulteFric(capitalise)
{
	let insulte = insultes.rand();

	switch(util.rand(2))
	{
		case 1: insulte = `sale ${insulte}`; break;
		case 2: insulte = voyelles.includes(insulte[0]) ? `espèce d’${insulte}` : `espèce de ${insulte}`; break;
	}

	return capitalise ? insulte.capitalize() : insulte;
}

function mois(m)
{
	switch(m)
	{
		case 0: return "janvier";
		case 1: return "février";
		case 2: return "mars";
		case 3: return "avril";
		case 4: return "mai";
		case 5: return "juin";
		case 6: return "juillet";
		case 7: return "août";
		case 8: return "septembre";
		case 9: return "octobre";
		case 10: return "novembre";
		case 11: return "décembre";
		default: return `${m} ? mois inconnu`;
	}
}

function formateTitre(article, titre)
{
	article = article.trim();
	if(article === "le" || article === "el")
		return titre.replace(/·[^  ]*/g, ""); // espace normal et insécable
	else if(article === "la")
		return titre.replace(/·/g, "");

	return titre;
}

/**
 * @param {GuildMember|User} MouU
 * @returns {string|undefined} "m", "f" ou undefined
 */
function genre(MouU)
{
	const article = zouz.get(MouU.id).titres?.article?.toLowerCase().trim();
	if(article === "la")
		return "f";
	if(article === "le" || article === "el")
		return "m";
}

genre.pronom = MouU => {
	const mf = genre(MouU);
	return mf === "m" ? "il" : mf === "f" ? "elle" : "iel";
}
genre._e = MouU => {
	const mf = genre(MouU);
	return mf === "m" ? "" : mf === "f" ? "e" : "·e";
}
genre.eurse = MouU => {
	const mf = genre(MouU);
	return mf === "m" ? "eur" : mf === "f" ? "euse" : "eur·se";
}
genre.er = MouU => {
	const mf = genre(MouU);
	return mf === "m" ? "er" : mf === "f" ? "ère" : "èr·e";
}
genre._1 = MouU => {
	const mf = genre(MouU);
	return mf === "m" ? "1er" : mf === "f" ? "1re" : "1er·e";
}




client.on("messageCreate", msg => {
	const authorId = msg.author.id,
		  z = zouz.get(authorId);

	if(!msg.author.bot && msg.guild && !isIgnored(msg.channel)
	   && !(msg.createdTimestamp - lastMsgs[authorId] < 60000)) // 1 minute ; inversé au cas où lastMsgs[authorId] est undefined
	{
		lastMsgs[authorId] = msg.createdTimestamp;

		const current = z.zouz;
		let revenu = z.VIP ? 10 : 7;

		if(current < seuil.pauvreté)
			revenu += 2;
		else if(current > seuil.richesse)
			revenu -= Math.min(10, ~~((current - seuil.richesse) / seuil.richesse * 2) + 1); // 100% -> 1 ; 150% -> 2 ; 200% -> 3 ; etc

		if(revenu)
		{
			z.zouz += revenu;
			zouz.update(authorId);
		}
	}
});



module.exports = exports = {
	register,

	seuil,

	respect: id => zouz.get(id.id || id).respect > Date.now(),
	vip: id => zouz.get(id.id || id).VIP,

	pauvre: id => zouz.get(id.id || id).zouz < seuil.pauvreté,
	riche: id => zouz.get(id.id || id).zouz >= seuil.richesse,

	don,

	titre: id => zouz.get(id.id || id)?.titres?.actuel,
	insulteFric,

	genre, tn,
};



const commands = require("../../commands");
const inline = true, masterOnly = true;

commands.registerCategory(["💰", "zouz", "casinouz"], {
	nom: "Zouz",
	color: 0xF9EE4E,
	thumbnail: { url: "https://vignette.wikia.nocookie.net/capcomdatabase/images/4/46/AkashicZenny.png" },
	title: "Les Ƶouz",
	description: "Le zouz est l’unité monétaire utilisée par les Crisse-Barbes tels que Strun Bah Couille, représentée par un ƶ (c’est un vrai caractère, pas un simple z barré avec `~~z~~`). Personne ne sait vraiment ce qu’iels en font mais c’est rigolo. À chaque message (avec un écart minimum d’une minute entre chaque), vous recevez quelques zouz. Vous pouvez les donner à vos amis, parce que vous avez perdu un pari par exemple, ou vous en servir auprès de Strun, pour acheter diverses choses comme le respect.\nTous vos zouz sont bien à l’abri dans les coffres de la BCB (Banque des Crisse-Barbes).\nVeuillez noter que les zouz sont globaux, c’est-à-dire que vous avez les mêmes possessions quel que soit le serveur.\n\n**Commandes :**",
	shortDescription: "Module d'économie",
});


function zouzify(name) { return name instanceof Array ? name.map(zouzify) : ("zouz "+name); }
function register(names, ...args)
{
	if(names.hidden)
	{
		names.hidden = zouzify(names.hidden);
		names.names  = zouzify(names.names );
	}
	else
		names = zouzify(names);

	commands.register("💰", names, ...args);
}

const titres_command = new commands.constructor;
function register_titres(names, params, description, command, options)
{
	if(!(names instanceof Array))
	{
		if(typeof names === "string")
			names = [names];
		else
			throw new TypeError("'names' should be a string or array of strings.");
	}

	if(typeof command !== "function") // was params ommitted?
	{
		options = command;
		command = description;
		description = params;
		params = null;
	}

	let mainName = names[0];
	if(params)
		mainName += " " + params;

	titres_cmds.push([mainName, description]);
	titres_command.register(null, names, params, description, command, options);
}

const {donne: aide} = require("../aide");

register({names: "", hidden: ["?", "aide", "presentation", "présentation"]}, "",
"Présente le zouz.",
(msg, [cmd, ...prms]) => {
	const {prefix = ">", channel, author} = msg;
	if(!cmd)
		aide(channel, "💰", checkMaster(author));
	else if(commands("zouz "+cmd.toLowerCase(), msg, prms) === null)
	{
		if(goCasinouz(cmd, prms, msg, zouz.get(author.id)))
			zouz.update(author.id);
		else
			channel.send("Commande inconnue. Essayez `"+prefix+"zouz aide`.");
	}
},
{inline});


register("revenu",
"Explique comment fonctionne le gain régulier de zouz.",
({channel}) => channel.send(`**== Résumé du revenu ==**
À chaque message, vous recevez 7 zouz.
Vous en gagnez 2 de plus si vous êtes pauvre, et 3 de plus si vous êtes VIP (jusqu’à +5 donc).
Si vous êtes riche, vous en gagnez 1 de moins. Si vous avez 1,5 fois le seuil de richesse, 2 de moins. 3 de moins si vous avez deux fois le seuil de richesse, 4 si vous êtes 2,5 fois riche, etc jusuq’à un maximum de 10 de moins (ce qui signifie que oui, si vous êtes très riche mais pas VIP, vous allez perdre de l’argent ; c’est la **taxe**).

Notez que pour éviter le spam, vous ne gagnez d’argent que si votre dernier message date d’il y a plus d’une minute.

En outre, si vous votez pour Strun sur Top.gg (<https://top.gg/fr/bot/412220365700595712>), il vous donnera 300 zouz, ou 500 si vous êtes VIP ou pauvre.

Vous êtes pauvre si vous avez moins de ${seuil.pauvreté.toZouz()} et riche si vous en avez plus de ${seuil.richesse.toLocaleString("fr")}.`),
{inline});


register("compte",
"Affiche votre richesse.",
async ({mentions, author, channel}, prm) => {
	const cible = mentions.users.first() || await getUser(prm.join(" "));

	if(cible)
	{
		if(checkMaster(author))
			channel.send(`${cible} possède ${zouz.get(cible.id).zouz.toZouz()}.`);
		else if(cible.id === author.id)
			channel.send("Pourquoi tu te mentionnes toi-même ?");
		else if(cible.bot && !crisseBarbes.includes(cible.id))
			channel.send("Les robots n’ont pas d’argent.");
		else
			channel.send(`Non mais ${insulte(author)}, tu crois vraiment que je vais te laisser fourrer ton nez dans les comptes des autres ?`);
	}
	else
	{
		let commentaire, fric = 0;

		if(!zouz.has(author.id))
			zouz.set(author.id, { zouz: 0 });
		else
			fric = zouz.get(author.id).zouz;

		if(fric < seuil.pauvreté)
			commentaire = " LOL, sale pauvre !";
		else if(fric > seuil.richesse)
			commentaire = " Putain, cette richesse !";
		else
			commentaire = "";

		channel.send(`Tu as ${fric.toZouz()}.${commentaire}`);
	}
}, {inline});


register("top", "[n] [global]",
"Affiche les plus riches du serveur, ou de tous les serveurs si `global` est précisé. `n` indique combien de personne à afficher (doit être compris entre 1 et 20). Exemple : `>zouz top 10 global`",
async ({channel}, [n, _global]) => {
	const mondial = ["global", "mondial", "total"];

	if(mondial.includes(n))
	{
		n = _global;
		_global = "global";
	}

	const duServeur = !mondial.includes(_global);
	const srv = channel.guild;

	if(duServeur && !srv)
		return channel.send("Si tu ne précises pas `global`, tu dois faire ça sur un serveur.");

	if(!n)
		n = 5;
	else if(!isFinite(n) || n < 1)
		return channel.send({content: `Haha, top ${n}, c’est rigolo ça.`, allowedMentions: {parse: []}});
	else if(n > 20)
	{
		n = 20;
		channel.send("On va dire un top 20, ok ?");
	}

	let content;
	let zouzz;

	if(duServeur)
	{
		if(srv.memberCount <= 1000)
			await srv.members.fetch();
		else
			content = "Ce serveur contenant plus de 1000 membres, seuls les membres récemment actifs sont pris en compte.";

		zouzz = [];
		for(const [id, user] of srv.members.cache)
		{
			if(zouz.has(id))
				zouzz.push({ user, zouz: zouz.get(id).zouz });
		}

		zouzz.sort((a, b) => b.zouz - a.zouz);

		if(zouzz.length > n)
			zouzz.length = n;
	}
	else
	{
		zouzz = zouz.get("topGlobal").map(({id, zouz}) => ({id, zouz})); // Cloning so the entries don't get polluted with a User
		zouzz.length = n;
		for(const zz of zouzz)
			zz.user = await getUser(zz.id);
	}

	if(!zouzz.length)
		return channel.send("Bizarrement, absolument personne n’a d’argent.");

	const top = zouzz.shift();
	const fields = zouzz.length ? zouzz.map(z => ({name: "💸 " + tn(z.user, true, true), value: `avec **${z.zouz.toZouz()}**`})) : undefined;
	if(zouzz.includes(undefined))
		fields.push({name: "Ha zut", value: "Il y a des gens dont j’ai pas pu récupérer les infos parce que Discord est méchant."});

	channel.send({ content, embeds: [{
		color: 0xF9EE4E,
		title: `**💰 ${tn(top.user, true, true)}** domine${duServeur ? ` _${srv.name}_` : ""}\navec **${top.zouz.toZouz()}**`,
		description: zouzz.length ? `\nSuivi${genre._e(top.user)} de :\n` : undefined,
		fields,
	}]});
});


register("donne", "<somme> <@mention>",
"Donne la somme donnée au membre mentionné. Si vous mentionnez plusieurs personnes, la première recevra l’argent. Si vous avez envoyé cette commande en message privé à Strun (pour des transaction secrètes...), utilisez par le tag de l’utilisateur à la place (les mentions ne fonctionnent pas en MP).",
async (msg, [somme, ...mention]) => {
	if(!isFinite(somme))
		msg.channel.send({content: somme + " ? C’est quoi ce nombre ?", allowedMentions: {parse: []}});
	else
	{
		const {guild, channel, author} = msg;
		const cible = guild ? (msg.mentions.members.first() || await getMember(guild, mention.join(" ")))
							: await getUser(mention.join(" "));
		somme = ~~somme;

		if(!cible)
			channel.send("Et tu veux que je donne ça à qui exactement ?..");
		else if(cible.user?.bot || cible.bot)
			channel.send("Les bots ne peuvent pas avoir d’argent.");
		else if(somme === 0 || msg.author.id === cible.id)
			channel.send(hilarant.rand());
		else
			transfer(somme, cible, msg);
	}
});


register("boutique",
"Affiche ce que vous pouvez acheter auprès de Strun.",
({channel, prefix}) => channel.send({ embeds: [{
	thumbnail: { url: "https://vignette.wikia.nocookie.net/capcomdatabase/images/4/46/AkashicZenny.png/revision/latest?cb=20120906222421" },
	color: 0xF9EE4E,
	title: "Ƶouz Boutique",
	description: `La Ƶouz Boutique dispose de divers articles plus ou moins inutiles que vous pouvez acheter en faisant \`${prefix}zouz achète <article>\`.\nLes riches payent 50% de plus. Le seuil de richesse est actuellement à ${seuil.richesse.toZouz()}.`,
	fields: [
		{ name: "Respect", value: `Pour la modique somme de ${prix.respect.toZouz()}, achetez le respect de Strun pour une semaine. (notez que Lok s’en bat les reins)\nLe respect vous évite d’être insulté. C’est cool, non ?` },
		{ name: "Titre", value: "Pour gagner un titre honorifique, ou améliorer celui que vous avez déjà. Faites `"+prefix+"zouz titres` pour voir tous les titres." },
		{ name: "VIP", value: `Le pass VIP coûte ${prix.VIP.toZouz()} et vous permet de gagner 3 zouz de plus par message ! Valable à vie, bien sûr.` }
	],
	footer: {
		text: "Créé par " + master.username,
		icon_url: master.avatarURL()
	}
}]}),
{inline});


register({names: "matos", hidden: "possessions"},
"Affiche ce que vous possédez.",
async ({author, channel, mentions}, prm) => {
	let snoop = mentions.users.first() || await getUser(prm.join(" ")), z, message = "";

	if(snoop && snoop.id !== author.id)
	{
		if(checkMaster(author))
		{
			z = zouz.get(snoop.id);
			snoop = tn(snoop, true);
		}
		else
		{
			channel.send(`Tu crois que je vais te laisser fouiner dans les affaires d’un${genre._e(snoop)} autre ?`);
			return;
		}
	}
	else
		z = zouz.get(author.id);


	if(!z)
		channel.send(`${snoop ? snoop.username.capitalize() + " n’a" : "Tu n’as"} RIEN. Heh.`);
	else
	{
		if(z.respect && z.respect > Date.now())
		{
			const date = new Date(z.respect);
			message += `${snoop ? snoop.username.capitalize() + " a" : "Vous avez"} mon respect jusqu’au ${date.getDate()} ${mois(date.getMonth())} ${date.getFullYear()} à ${date.getHours()}h${date.getMinutes().toString().padStart(2, 0)}`;
		}

		if(z.VIP)
		{
			if(message)
				message += `, et ${snoop ? "est" : "vous êtes"} VIP !`;
			else
				message = snoop ? snoop.username.capitalize() + " est VIP." : "Vous êtes VIP.";
		}
		else if(message)
			message += '.';

		if(z.titres)
		{
			const vousPossédez = `\n${snoop ? (message ? genre.pronom(snoop) : snoop.username).capitalize() + " possède" : "Vous possédez"} ${message ? "également " : ""}`;

			if(z.titres.possédés.length)
				message += `\n${vousPossédez}${z.titres.possédés.length} titre${z.titres.possédés.length === 1 ? "" : "s"}${z.titres.perso ? ", plus un personnalisé." : "."}`;
			else if(z.titres.perso)
				message += `\n${vousPossédez}un titre personnalisé.`;
		}

		if(!message)
			message = `${snoop ? snoop.username.capitalize() + " n’a" : "Tu n’as"} RIEN. Heh.`;

		channel.send(message);
	}
}, {inline});


register({names: "achète", hidden: ["achete", "achat"]}, "<article>",
"Achète l’article demandé.",
async ({channel, author, mentions, prefix}, prm) => {
	const z = zouz.get(author.id);
	prm = prm.join(" ");

	if(!z)
		channel.send(`T’as pas le fric pour, ${insulteFric().toUpperCase()} !`);
	else
	{
		let aAchetéUnTruc = true;

		switch(prm.toLowerCase())
		{
			case "respect":
				achète_respect(z, channel);
				break;

			case "titre":
			case "titres":
				channel.send({ content: titres_infos, embeds: [titresEmbed(prefix)] });
				break;

			case "vip":
				achète_vip(z, channel);
				break;


			case "strun":
			case "strunbahcouille":
			case "strun bah couille":
			case `<@${client.user.id}>`:
				aAchetéUnTruc = false;
				channel.send(checkMaster(author)
					? "Mais... je suis déjà à votre service."
					: `Tu ne peux pas m’acheter, ${insulte(author)} ! Personne ne le peut !`
				);
				break;

			default:
				aAchetéUnTruc = false;
				channel.send(mentions.users.first() || await getUser(prm)
					? `Tu ne peux pas acheter quelqu’un, espèce de dégénéré${genre._e(author)} !`
					: "Demande un vrai produit ou dégage de ma boutique !"
				);
				break;
		}

		if(aAchetéUnTruc)
			zouz.update(author.id);
	}
}, {inline});



commands.register(null, ["titre", "titres"], "[@mention]",
"Affiche le titre de quelqu’un (ou le vôtre si vous ne mentionnez personne).",
async (msg, prm, cmd) => {
	const {author, channel, guild} = msg;
	const mention = prm.join(" ");
	const target = guild ? (msg.mentions.members.first() || await getMember(guild, mention))
						 : await getUser(mention);

	if(target)
	{
		if(guild ? target.user.bot : target.bot)
			channel.send("Les bots ne peuvent pas avoir de titres.");
		else
			afficheTitre(target, channel);
	}
	else if(!mention)
	{
		const z = zouz.get(author.id);

		channel.send(!z?.titres?.actuel
			? "Tu n’as présentement aucun titre."
			: `Vous êtes ${tn(guild ? msg.member : author, true)} !`
		);
	}
	else
		c_titres(msg, prm, cmd);
});


register_titres(["achète", "achete", "chat"], "<titre>",
"Achète le titre indiqué.",
({prefix, author, member, channel}, [z, ...prm]) => {
	const titre = prm.join(" ");

	if(!titre)
		channel.send("Haha, d’accord. Lequel ?");
	else
	{
		const rang = titres_rangs.get(titre);

		if(rang)
		{
			const coût = rang * (z.zouz > seuil.richesse ? prix.titres * 1.5 : prix.titres);

			if(z.titres.possédés.includes(titre))
				channel.send("Mais... vous possédez déjà ce titre.");
			else if(z.zouz >= coût)
			{
				z.zouz -= coût;
				zouz.set("bank", zouz.get("bank") + coût);
				z.titres.possédés.push(titre);

				channel.send(`Merci pour votre précieux don de ${coût.toZouz()}, ${titre} ${member.displayName}. N’oubliez pas de faire \`${prefix}zouz titres porter ${titre}\` si vous désirez le porter.`);
			}
			else
				channel.send(`Ce titre coûte ${coût.toZouz()} et tu ne les as pas, ${insulteFric()} !`);
		}
		else
			channel.send("Ce titre n’existe pas. Pour créer un titre custom, faites `"+prefix+"zouz titres crée <titre>`.");
	}
});

register_titres(["crée", "cree", "créer", "creer", "création", "creation"], "<titre>",
"Crée une titre personnalisé. **Attention,** cela écrasera votre titre perso précédent, si vous en aviez un.",
({author, member, channel}, [z, ...prm]) => {
	let titre = prm.join(" ");
	const prixTitre = (z.titres.perso ? 5 * prix.titres : 10 * prix.titres) * (z.zouz > seuil.richesse ? 1.5 : 1);

	if(z.zouz < prixTitre)
		channel.send(`Tu n’as pas de quoi te faire un titre personnalisé, ${insulteFric()} !`);
	else if(!titre)
		channel.send("Haha, d’accord. Je te met quoi du coup ?");
	else
	{
		if(titre.length <= 30)
		{
			titre = titre.replace(/'/g, "’");
			z.zouz -= prixTitre;
			zouz.set("bank", zouz.get("bank") + prixTitre);
			z.titres.actuel = z.titres.perso = titre;
			channel.send(`Merci pour votre précieux don de ${prixTitre.toZouz()}, ${tn(member || author)}. (ce titre vous a été automatiquement assigné)`);
		}
		else
			channel.send("Votre titre ne peut pas dépasser 30 caractères.");
	}
});

register_titres(["possédés", "possedes", "possède"],
"Affiche les titres que vous possédez.",
async ({author, member, channel, guild, mentions}, [z, ...prm]) => {
	let autre = false;
	let target = guild ? (mentions.members.first() || await getMember(guild, prm.join(" ")))
					   : (mentions.users.first()   || await getUser(prm.join(" ")));

	if(target)
	{
		if(guild ? target.user.bot : target.bot)
		{
			channel.send("Les bots ne peuvent pas avoir de titres.");
			return;
		}

		if(checkMaster(author))
			autre = true;
		else
		{
			channel.send("Et de quel droit viens-tu fouiner dans les documents des autres ?");
			return;
		}
	}
	else
		target = member || author;

	afficheTitres(target, channel, autre);
});

register_titres(["porter", "porte", "mettre"], "<titre>",
"Vous fait porter le titre indiqué, si vous le possédez.",
({channel, member, author}, [z, ...prm]) => {
	const coût = z.zouz > seuil.richesse ? prix.changeTitre * 1.5 : prix.changeTitre;
	prm = prm.join(" ");

	if(z.zouz < coût)
		channel.send(`Cela coûte ${coût} ƶ de changer de titre... et tu n’as même pas ça ! ${insulteFric(true)} !`);
	if(z.titres.possédés.includes(prm))
	{
		z.zouz -= coût;
		zouz.set("bank", zouz.get("bank") + coût);
		z.titres.actuel = prm;
		channel.send(`Voilà qui est fait, ${tn(member || author)}.`);
	}
	else if(prm === z.titres.perso || ["perso", "personnel", "personnalisé"].includes(prm.toLowerCase()))
	{
		z.zouz -= coût;
		zouz.set("bank", zouz.get("bank") + coût);
		z.titres.actuel = z.titres.perso;
		channel.send(`Voilà qui est fait, ${tn(member || author)}.`);
	}
	else
		channel.send("Vous ne possédez pas ce titre.");
});

register_titres("article", "[article]",
"Définit l’article à mettre devant votre titre, pour vous faire appeler “le grandaron” au lieu de “lia grandaron·ne”, par exemple. Un article ne peut pas faire plus de trois lettres. Mettez “supprimer” pour ne pas avoir d’article, ce qui peut être préférable pour certains titres, comme Ecce Homo ou Von.\nSi vous ne précisez pas d’article ou que vous mettez “actuel”, votre article actuel vous sera affiché.",
({channel}, [z, ...prm]) => {
	prm = prm.join(" ").trim().toLowerCase();
	const efface = ["aucun", "efface", "effacer", "supprime", "supprimer"].includes(prm.replace(/["“”]/g, "")); // on ne sait jamais avec ces andouilles

	if(!prm || prm === "actuel")
		channel.send(z.titres.article ? `Votre titre actuel est "${z.titres.article.trim()}".` : "Vous n’avez présentement pas d’article.");
	else if(prm.length < 4 || efface)
	{
		if(efface)
			prm = "";

		prm = prm.replace(/'/g, "’");

		if(!prm || prm.includes('l') && /^[a-zæœ’]*$/.exec(prm))
		{
			prm = z.titres.article = prm ? prm + (prm[prm.length-1] === "’" ? "" : " ") : "";
			channel.send("Article mis à jour." + (z.titres.actuel ? ` Vous êtes désormais ${(prm ? `**${prm}**` : "") + formateTitre(prm, z.titres.actuel)}.` : ""));
		}
		else
			channel.send("Z’êtes sûr·e que c’est un article, ça ?");
	}
	else
		channel.send("C’est trop long pour un simple article.");
});


register({names: "titres", hidden: "titre"},
"Affiche les informations sur les titres.",
c_titres);
async function c_titres(msg, [cmd, ...prm], cmdName)
{
	const {author, member, channel, guild} = msg;
	const z = zouz.get(msg.author.id);

	if(!z.titres)
		z.titres = { possédés: [], article: "lia " };

	if(!cmd && cmdName.toLowerCase() === "titre")
		return channel.send(!z?.titres?.actuel
			? "Tu n’as présentement aucun titre."
			: `Vous êtes ${tn(guild ? member : author, true)} !`
		);

	if(!cmd || cmd === "?" || cmd === "aide")
		return channel.send({ content: titres_infos, embeds: [titresEmbed(msg.prefix)] });

	if(titres_command(cmd, msg, [z, ...prm]) === null)
	{
		const mention = [cmd, ...prm].join(" ");
		const target = guild
			? (msg.mentions.members.first() || await getMember(guild, mention))
			: await getUser(mention);

		if(mention)
		{
			if(!target)
				channel.send({content: `Je n’ai pas trouvé ${mention}.`, allowedMentions: {parse: []}});
			else if(target.id === author.id)
				afficheTitres(target, channel);
			else
				afficheTitre(target, channel);
		}
		else
			channel.send("Commande inconnue. Essayez `" + msg.prefix + "zouz titres aide`.");
	}

	zouz.update(msg.author.id);
}


commands.register(null, "zouz article", "[article]",
"Affiche votre article (le, la...) ou le modifie.",
(msg, prms, cmd) => c_titres(msg, ["article", ...prms], cmd));


register("liste", "[options]",
`Affiche la liste de toutes les personnes à la BCB.
Options : \`filter=x\` où \`x\` est le nom de la propriété requise. \`filter=!x\`.  \`sort=x\`. \`purge\` (supprime les comptes n'ayant rien ou presque), \`purgeOny\` (idem, mais n'affiche rien)
Exemple : \`>zouz liste filter=VIP sort=zouz\``,
({channel}, filters) => {
	const now = Date.now();

	function toStr(compte) {
		let str = "", zouz;

		for(const [id, elm] of Object.entries(compte))
		{
			switch(id)
			{
				case "zouz":
					zouz = compte.zouz.toLocaleString("fr");
					break;
				case "VIP":
					str += " **VIP.**";
					break;
				case "respect":
					if(elm > now)
						str += " **Respect** jusqu'au " + (new Date(elm)).toLocaleString("fr") + ".";
					break;
				default: {
					const subElms = Object.entries(elm).map(([id, elm]) => `__${id}__: ${elm instanceof Array ? `[${elm.join(", ")}]` :JSON.stringify(elm)}`);

					str += ` **${id.capitalize()} :** ${subElms.join(", ")}`;
				}
			}
		}

		return `**${Z[0]}ouz :** ${zouz}.${str}`;
	}

	// total à -1 pour ne pas compter la banque
	let purgés = 0, bank;
	filters = filters.map(filter => filter.toLowerCase());
	const purgeOnly = filters.includes("purgeonly");
	const purge = purgeOnly || filters.includes("purge") || filters.includes("purger");
	let accounts = []; // not const because it might be filtered
	if(purgeOnly)
		accounts.push = Function();

	for(const [id, account] of zouz)
	{
		if(id === "topGlobal")
			continue;
		else if(id === "bank")
			bank = account;
		else if(purge && account.zouz < 110 && !account.titres && !account.VIP && (!account.respect || account.respect < now))
		{
			zouz.delete(id);
			purgés++;
		}
		else
			accounts.push({id, account});
	}

	if(purgeOnly)
		return channel.send(`Purgé ${purgés} compte(s).`);


	for(const f of filters)
	{
		const [filter, val] = f.split("=");

		switch(filter)
		{
			case "order":
			case "sort":
			case "tri":
			case "trier":
				accounts.sort((a, b) => a.account[val] - b.account[val]);
				break;
			case "filter":
			case "filtre":
			case "filtrer": {
				const not = val.startsWith("!"), filter = not ? val.substring(1) : val;
				accounts = accounts.filter(({account}) => {
					const ok = filter === "respect" ? (account.respect && account.respect < now) : account[filter];
					return not ? !ok : ok;
				});
				break;
			}
		}
	}

	let message = "";

	for(const {id, account} of accounts)
		message += `<@${id}> : ${toStr(account)}\n`;

	message += `BCB : ${bank.toLocaleString("fr")}\n**Total :** ${accounts.length}`;

	if(purge)
		message += ` _(${purgés} purgé${purgés > 1 ? "s" : ""})_`;

	splitMessage(message).forEach(channel.send.bind(channel));
}, {masterOnly});

register("amende", "<id> <somme> [raison]",
"Retire la quantité d’argent demandée l’utilisateurice indiqué·e. (ne peut pas aller en dessous de 0)",
async ({channel}, [id, somme]) => {
	if(!isFinite(somme))
	{
		if(!isFinite(id))
			return channel.send(`Nombre incorrect : ${somme}.`);

		const temp = somme;
		somme = id;
		id = temp;
	}

	const cible = await getUser(id);

	if(!cible || !zouz.has(cible.id))
		channel.send(`Id inconnu : ${id}.`);
	else
	{
		const z = zouz.get(cible.id);
		somme = ~~somme;

		if(typeof z === "object")
			z.zouz -= z.zouz < somme ? (z.zouz < 0 ? 0 : z.zouz) : somme;

		channel.send(`${["Vualà", "Tiens", "Et bim"].rand()} ${cible}, ${somme.toZouz()} de moins, ${["ça t’apprendra", "ça te fera les pieds", "dans ton cul", "non mais"].rand()} !`);
		zouz.update(cible.id);
	}
}, {masterOnly});

register("ruine", "<somme>",
"Retire la quantité d’argent demandée à TOUT LE MONDE. (ne peut pas aller en dessous de 0)",
({channel}, somme) => {
	if(!isFinite(somme))
		channel.send(`Nombre incorrect : ${somme}.`);
	else
	{
		somme = ~~somme;

		for(const [id, z] of zouz)
			if(id !== "bank" && typeof z === "object")
				z.zouz -= z.zouz < somme ? (z.zouz < 0 ? 0 : z.zouz) : somme;

		channel.send(`Vualà, tout le monde a été ruiné de ${somme} ƶ !`);
		zouz.save();
	}
}, {masterOnly});

register("reset", "<@mention|id>",
"Remet à 0 les zouz de la personne mentionnée.",
async ({channel}, user) => {
	user = await getUser(user);

	if(!user)
		channel.send("Je lia trouve pas.");
	else
	{
		const z = zouz.get(user.id);
		z.zouz = 0;
		zouz.update(user.id);
		channel.send(`Les zouz de ${user} ont été remis à 0.`);
	}
}, {masterOnly});

register("don", "<somme> <@mention|id>",
"Donne la somme donnée à la personne mentionnée.",
async ({author, channel}, [sommePrm, heureuxElu]) => {
	const somme = ~~sommePrm;
	heureuxElu = await getUser(heureuxElu);

	if(!somme)
		channel.send(`${sommePrm} zouz, très drôle.`);
	else if(!heureuxElu)
		channel.send("J’ai pas trouvé cette personne.");
	else
	{
		don(somme, heureuxElu.id);
		const finPhrase = somme > 0
			? `généreusement ajoutés ${heureuxElu === author ? "à votre poche" : `aux poches de ${heureuxElu}`}`
			: `confisqués à ${heureuxElu}`;
		channel.send(`${(somme < 0 ? -somme : somme).toZouz()} ont été ${finPhrase}.`);
	}
}, {masterOnly});


function don(somme, idReceveur)
{
	zouz.get(idReceveur).zouz += ~~somme;
	zouz.update(idReceveur);
}



/**
 * tn : titre + nom
 * @param {GuildMember|User} MouU
 * @param {boolean} metArticle
 * @param {boolean} capitalise
 * @param {object} optionsFormattage indique si l'article, le titre ou le nom doivent être encadrés de caractères de formattage. Exemple : { article: "__", nom: "**" }
 * @returns {string} titre + nom
 */
function tn(MouU, metArticle, capitalise, optionsFormattage)
{
	if(!optionsFormattage)
		optionsFormattage = {};

	const f = {
		a: optionsFormattage.article || "",
		t: optionsFormattage.titre || "",
		n: optionsFormattage.nom || "",
	};

	if(!zouz.has(MouU.id))
		return MouU.displayName || MouU.username;

	const z = zouz.get(MouU.id);
	let titre = "";


	if(z.titres?.actuel)
	{
		const article = z.titres.article;
		let titreActuel = z.titres.actuel;

		metArticle = metArticle && article;

		if(titreActuel && titreActuel !== z.titres.perso)
			titreActuel = formateTitre(article, titreActuel);

		titre = `${metArticle ? `${f.a}${article}${f.a.reverse()}` : ""}${f.t}${titreActuel} ${f.n}${MouU.displayName || MouU.username}${f.n.reverse()}${f.t.reverse()}`;
	}
	else
		titre = `${f.n}${MouU.displayName || MouU.username}${f.n.reverse()}`;

	return capitalise ? titre.capitalize() : titre;
}



function transfer(somme, cible, msg)
{
	const {guild, channel, author} = msg;
	const donneur = zouz.get(author.id);

	if(somme < 0)
	{
		let message = "Est-ce que tu es en train d’essayer de voler cette personne ?..\nC’est pathétique. ";

		const _24h = 86400000; // 1000*3600*24
		if(donneur.respect > Date.now() - _24h)
		{
			donneur.respect -= _24h;
			message += "Tu as perdu un peu de mon respect, tiens.";
		}
		else
		{
			donneur.zouz -= 100;
			message += "Pour la peine, tu écopes de 100 ƶ d’amende !";
		}

		channel.send(message);
		zouz.update(author.id);
	}
	else if(donneur.zouz < somme)
		msg.reply(`${insulteFric(!guild)}, tu n’a pas cette somme !`);
	else
	{
		const d = zouz.get(cible.id);
		let préposition = "à ";

		donneur.zouz -= somme;
		don(somme, cible.id); // don sauvegarde les zouz

		if(!d.titres)
			d.titres = { possédés: [], article: "lia " };

		if(d.titres.actuel)
			préposition = d.titres.article === "le " ? "au " : "à " + d.titres.article;

		if(guild)
			channel.send(`Virement effectué ${préposition + tn(cible)}.`);
		else
		{
			msg.reply(`Virement effectué ${préposition + tn(cible)}.`);
			cible.send(`${tn(author, true, true)} vient de vous donner ${somme.toZouz()}.`);
		}
	}
}


function achète_respect(z, channel)
{
	const coût = z.zouz > seuil.richesse ? prix.respect * 1.5 : prix.respect;

	if(z.zouz < coût)
		channel.send(`T’as pas le fric pour, ${insulteFric()}.`);
	else
	{
		z.zouz -= coût;
		if(z.respect > Date.now())
		{
			z.respect += 604800000; // 1000*3600*24*7
			channel.send("Vous avez gagné mon respect pour une semaine de plus.");
		}
		else
		{
			z.respect = Date.now() + 604800000;
			channel.send("Voilà ! Vous avez gagné mon respect pour une semaine. (lol)");
		}

		zouz.set("bank", zouz.get("bank") + coût);
	}
}
function achète_vip(z, channel)
{
	const coût = z.zouz > seuil.richesse ? prix.VIP * 1.5 : prix.VIP;

	if(z.VIP)
		channel.send("Mais... vous êtes déjà VIP.");
	else if(z.zouz < coût)
		channel.send(`T’as pas le fric pour, ${insulteFric()}.`);
	else
	{
		z.zouz -= coût;
		zouz.set("bank", zouz.get("bank") + coût);
		z.VIP = true;
		channel.send("Bienvenue au club VIP.");
	}
}

function afficheTitre(target, channel)
{
	if(target.bot || target.user?.bot)
		return channel.send("Les bots ne peuvent pas avoir de titre.");

	const z = zouz.get(target.id);

	if(!z || !z.titres || !z.titres.actuel)
		channel.send(`${tn(target)} n’a actuellement aucun titre.`);
	else
	{
		const du = z.titres.article === "le ",
			  messages = ["Bien sûr,", "Aah,", `Vous parlez ${du ? "du" : "de"}`],
			  quelDonc = util.rand(messages.length - 1);
		channel.send(`${messages[quelDonc]} ${tn(target, quelDonc !== 2 || !du)} !`);
	}
}
function afficheTitres(target, channel, autre)
{
	if(target.bot || target.user?.bot)
		return channel.send("Les bots ne peuvent pas avoir de titre.");

	const z = zouz.get(target.id);
	const {possédés} = z.titres;

	if(possédés.length || z.titres.perso)
	{
		let message = `${autre ? tn(target, true, true) + " possède" : "Vous possédez"} les titres suivants :`,
			ligne;

		for(let i = 0 ; i < titres.length ; i++)
		{
			ligne = [];
			message += `\nRang ${i+1} : `;
			for(const t of titres[i])
				if(possédés.includes(t))
					ligne.push(t);

			message += ligne.length ? ligne.join(", ") : "*aucun*";
		}

		if(z.titres.perso)
			message += `\nPlus un titre personnalisé qui est ${z.titres.perso}.`;

		if(z.titres.actuel && !autre)
			message += `\nVotre titre actuel est **${z.titres.article + z.titres.actuel}**.`;

		channel.send(message);
	}
	else
		channel.send(
			autre ? `${tn(target, true, true)} n’a aucun titre.`
			: `Tu n’as aucun titre, espèce de ${["pécore", `péquenaud${genre._e(target)}`, "clampin"].rand()}.`
		);
}



const goCasinouz = require("./casinouz");
