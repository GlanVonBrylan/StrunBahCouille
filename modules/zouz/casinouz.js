"use strict";

const { existsSync, readFileSync, promises: {writeFile} } = require("fs");
const { clone, rand } = require("../../utils");
const { niceJSONDisplay } = require("../../utils/Strun");
const { checkMaster, getMaster } = require("../../discordCore");

const { register, insulteFric, seuil } = require(".");

const casinouzFile = __dirname + "/../../savedData/casinouz.json";


const blankCasinouzStats = { // j = jeux, b = balance
	pfc: { j: 0, b: 0 },
	màs: { j: 0, b: 0 },
	pariBarbu: { j: 0, b: 0 },
	roulette: { j: 0, b: 0 }
};
var statsCasinouz = clone(blankCasinouzStats), savingCasinouz = false;

if(existsSync(casinouzFile))
{
	try {
		statsCasinouz = JSON.parse(readFileSync(casinouzFile));
	}
	catch(e) {
		console.error(e);
	}
}


function saveCasinouz()
{
	if(savingCasinouz)
	{
		if(typeof savingCasinouz !== "boolean")
			clearTimeout(savingCasinouz);

		savingCasinouz = setTimeout(saveCasinouz, 500);
	}
	else
	{
		savingCasinouz = true;
		writeFile(casinouzFile, JSON.stringify(statsCasinouz)).finally(() => savingCasinouz = false);
	}
}



register({names: ["cazinouz", "casino", "jeux"], hidden: ["jeu", "casinouz"]},
"Affiche les jeux disponibles.",
({prefix: p = ">", channel}) => {
	const master = getMaster();
	channel.send({ embeds: [{
		thumbnail: { url: "https://vignette.wikia.nocookie.net/capcomdatabase/images/4/46/AkashicZenny.png" },
		color: 0xF9EE4E,
		title: "Le Casinouz",
		description: "Venez, venez ! Dépensez des fortunes dans l’espoir d’en gagner une ! Qui sait, peut-être remporterez-vous le gros lot ?\n\Jeux disponibles :",
		fields: [
			{ name: p+"zouz pfc <mise> <pierre|feuille|ciseaux>", value: "Un jeu de pierre-feuille-ciseaux tout à fait banal. Si vous gagnez, vous remportez deux fois votre mise." },
			{ name: p+"zouz banditManchot [n]", value: `Insérez ${màsPrix} ƶ dans la fente, et si vous alignez trois symboles identiques, vous gagnerez quelque chose ! (faites \`${p}zouz banditManchot infos\` pour connaître les combinaisons gagnantes)\nFonctionne aussi avec \`${p}zouz machineÀSous\` et \`${p}zouz màs\`.\n\`n\` est le nombre de fois que vous voulez jouer (maximum ${màsLimite}). Si vous ne l’indiquez pas, vous ne jouerez qu’une fois.` },
			{ name: p+"zouz pariBarbu <mise>", value: `Le principe du pari barbu est simple : plus vous misez, moins vous avez de chances de gagner... mais plus le jackpot est important ! Vous devez miser au moins 100 ƶ.` },
			{ name: p+"zouz roulette <mise> <pari>", value: `Jouez à la roulette. Faites \`${p}zouz roulette\` sans paramètre pour plus de détails.` }
		],
		footer: {
			text: "Créé par " + master.username,
			icon_url: master.avatarURL()
		}
	}]});
});

register("statsCasinouz", "[reset]",
"Affiche les stats du casinouz, ou les réinitialise si `reset` est fourni.",
(msg, [reset = ""]) => {
	if(reset.toLowerCase() === "reset")
	{
		statsCasinouz = clone(blankCasinouzStats);
		saveCasinouz();
		msg.react("✅");
	}
	else
		msg.channel.send(niceJSONDisplay(statsCasinouz));
}, {masterOnly: true});


const PFC = ["PIERRE", "FEUILLE", "CISEAUX"];
const màs = [
	":skull_crossbones:", ":skull_crossbones:", ":skull_crossbones:", ":skull_crossbones:", ":skull_crossbones:",
	":cookie:", ":cookie:", ":cookie:", ":cookie:", ":cactus:", ":cactus:", ":cactus:", ":cactus:",
	":cookie:", ":cookie:", ":cookie:", ":cookie:", ":cactus:", ":cactus:", ":cactus:", ":cactus:",
	":notes:", ":art:", ":performing_arts:", ":four_leaf_clover:", ":fleur_de_lis:", ":gem:",
	":notes:", ":art:", ":performing_arts:", ":four_leaf_clover:", ":fleur_de_lis:", ":gem:",
	":notes:", ":art:", ":performing_arts:", ":four_leaf_clover:", ":fleur_de_lis:",
	":first_place:", ":second_place:", ":third_place:", ":trident:",
	":first_place:", ":second_place:", ":third_place:", ":trident:",
	":first_place:", ":second_place:", ":third_place:",
	":zero:", ":one:", ":two:", ":three:", ":four:", ":five:", ":six:", ":seven:", ":eight:", ":nine:",
];
const màsPrix = 10;
const màsLimite = 20; // nombre maximum d'essais d'un coup
const màsChiffres = [":zero:", ":one:", ":two:", ":three:", ":four:", ":five:", ":six:", ":seven:", ":eight:", ":nine:"];
const màsGains = {
	cookies: 30,
	//cactus: 4747, // Dutronc est né le 28/04/1943 // "Deux :notes: et un :cactus:, ou l'inverse — ${màsGains.cactus.toZouz()}"
	arts: 333,
	brelanArts: 500,
	trèfles: 777,
	lys: 1789,
	//lys2: 2608, // le 26/08/1789 est la date de la Déclaration des droits de l'homme et du citoyen
	diamants: 3000,
	médailles: 5000,
	tridents: 30000,
};
// Cactus : année de sortie de la chanson et sa durée en secondes
const màsE = `Triple :cookie: — ${màsGains.cookies} ƶ
Triple :cactus: — 67 ou 161 ƶ (une chance sur deux)
Trois chiffres : la moitié de la somme indiquée (par exemple, :one::three::zero: rapporte 65 ƶ)
Trois fois le même : :notes::art::performing_arts: — ${màsGains.arts} ƶ
Un de chaque : :notes::art::performing_arts: — ${màsGains.brelanArts} ƶ
Triple :four_leaf_clover: — ${màsGains.trèfles} ƶ
Triple :fleur_de_lis: — ${màsGains.lys.toZouz()}
Triple :gem: — ${màsGains.diamants.toZouz()}
Triple :third_place: — ${(màsGains.médailles/20).toZouz()}
Triple :second_place: — ${(màsGains.médailles/10).toZouz()}
Triple :first_place: — ${(màsGains.médailles).toZouz()}
Une de chaque dans le désordre — ${(màsGains.médailles/5).toZouz()}
:first_place::second_place::third_place: (dans l’ordre) — ${màsGains.médailles.toZouz()}
Deux :trident: et un chiffre — mille fois le chiffre
Deux :trident: et un :gem: — ${((màsGains.diamants + màsGains.tridents)/2).toZouz()}
Triple :trident: — ${màsGains.tridents.toZouz()}`;

const rouletteNumérosNoirs = [2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35];


module.exports = exports = function goCasinouz(jeu, [prem = "", ...prm], {author, channel, reply}, z) // prem = premier
{
	if(z.zouz < 3)
	{
		reply(`Tu es sans le sou, fiche le camp de mon casino, ${insulteFric()} !`);
		return true;
	}

	prm = prm.join(" ");

	switch(jeu.toLowerCase())
	{
		case "pfc":
		case "pierrefeuilleciseaux":
		case "pierre-feuille-ciseaux":
		case "chifoumi": {
			if(!isFinite(prem))
			{
				const temp = prem;
				prem = prm;
				prm = temp;
			}

			if(prem === "")
				reply("Hm... tu es censé miser quelque chose.");
			else if(!isFinite(prem))
				reply({content: `${prem} ? C’est quoi ce nombre ?`, allowedMentions: {parse: []}});
			else
			{
				prem = Number(prem);
				if(prem <= 0)
					reply(`${prem} ƶ. ${hilarant.rand()}`);
				else if(prem !== ~~prem)
					reply("Tu dois miser un nombre entier.");
				else if(prem > z.zouz)
					reply(`${insulteFric(true)}, tu n’as même pas assez pour miser ça !`);
				else
				{
					const pfc = PFC.rand();
					let v, m;

					switch(prm.toLowerCase())
					{
						case "p":
						case "pierre":
							switch(pfc)
							{
								case "FEUILLE": v = "strun"; break;
								case "CISEAUX": v = "joueur"; break;
							}
							break;

						case "f":
						case "feuille":
							switch(pfc)
							{
								case "CISEAUX": v = "strun"; break;
								case "PIERRE": v = "joueur"; break;
							}
							break;

						case "c":
						case "ciseau":
						case "ciseaux":
							switch(pfc)
							{
								case "PIERRE": v = "strun"; break;
								case "FEUILLE": v = "joueur"; break;
							}
							break;

						default:
							reply("Vous êtes censé jouer pierre, feuille ou ciseaux (fonctionne aussi avec p, f et c).");
							return true;
					}

					m = pfc + " !\n";

					if(v === "strun")
					{
						m += ["HA ! J’ai gagné !", "Et bim, qui c’est le patron ?", "Haha, facile !"].rand() + (Math.random() < 0.3 ? " Et vous pouvez aller vérifier mon code, je ne triche pas !" : " Par ici la monnaie !");
						statsCasinouz.pfc.b += prem;
						z.zouz -= prem;
					}
					else if(v === "joueur")
					{
						m += `${["Arf, bien joué.", "Pff, que d’la veine.", "Un coup de chance."].rand()} Tu récupères ta mise et ${prem.toZouz()} de plus.`;

						statsCasinouz.pfc.b -= prem;
						z.zouz += prem;
					}
					else
						m += ["Zut, égalité.", "Mince, égalité.", "Rha, on a fait pareil."].rand();

					statsCasinouz.pfc.j++;
					reply(m);
				}
			}
			break;
		}

		case "banditmanchot":
		case "machineasous":
		case "machineàsous":
		case "machineazouz":
		case "machineàzouz":
		case "màs":
		case "màz": {
			if(prem && ["?", "aide", "info", "infos"].includes(prem.toLowerCase()))
			{
				channel.send(màsE);
				break;
			}

			if(z.zouz < màsPrix)
				reply(`LOL ${insulteFric()}, t’as même pas dix balles à mettre dans une machine à sous ?`);
			else
			{
				const riche = z.zouz > seuil.richesse, trèsRiche = z.zouz > (seuil.richesse * 2);
				let nEssais = 1,
					nEffectués = 0,
					coût = màsPrix,
					réponse = "";

				if(riche)
					coût += 2;

				if(prem && isFinite(prem))
				{
					nEssais = ~~prem;

					if(nEssais < 1)
					{
						reply(nEssais + " essais ? Bah très bien, on joue et tu me donnes du fric si tu gagnes, c’est ça ? " + ["Banane.", "Patate.", "Clampion."].rand());
						break;
					}

					if(nEssais > màsLimite)
					{
						nEssais = màsLimite;
						reply("Hou, ça fait beaucoup quand même, non ? On va dire "+màsLimite+", d’accord ?");
					}

					coût *= nEssais;

					if(z.zouz < coût)
					{
						reply(`Ça coûte ${coût} ƶ de jouer ${nEssais} fois et aussi incroyable que cela puisse paraître, tu ne les as pas.`);
						break;
					}
				}

				z.zouz -= coût;

				for(let i = 0 ; i < nEssais ; i++)
				{
					let gain = 0, r;
					nEffectués++;

					r = Array.from({length: 3}, () => màs.rand());

					if(riche)
					{
						if((r.includes(":trident:") || r.includes(":gem:")) && Math.random() < (trèsRiche ? 0.5 : 0.2))
							r[rand(2)] = ":skull_crossbones:";
					}
					else
					{
						const symboles = màs.slice();

						r = [symboles.rand()];

						if(r[0] !== ":skull_crossbones:" && r[0] !== ":trident:" && !màsChiffres.includes(r[0]))
							symboles.push(r[0]);

						r.push(symboles.rand());

						if(r[1] !== ":skull_crossbones:" && r[0] !== ":trident:" && !màsChiffres.includes(r[1]))
						{
							if(r[0] === r[1])
								symboles.push(r[1], r[1]);
							else
								symboles.push(r[1]);
						}

						r.push(symboles.rand());
					}

					const nCactus = r.count(":cactus:"), nNotes = r.count(":notes:"),
						  nMedailles = [r.count(":first_place:"), r.count(":second_place:"), r.count(":third_place:")];

					if(màsChiffres.includes(r[0]) && r[1] !== ":trident:")
					{
						gain = "";

						for(let i = 0 ; i < 3 ; i++)
							gain += màsChiffres.indexOf(r[i]);

						gain = gain === "001" ? 1 : ~~(gain / 2); // si "gain" n'est pas un vrai nombre, gain / 2 = NaN, et ~~NaN = 0
					}
					else if(r.count(":cookie:") === 3)
						gain = màsGains.cookies;
					else if(nCactus === 3)
						gain = Math.random() < 0.5 ? 67 : 161;
					//else if(nCactus === 2 && nNotes === 1 || nCactus === 1 && nNotes === 2)
					//	gain = màsGains.cactus;
					else if(nNotes === 3 || r.count(":art:") === 3 || r.count(":performing_arts:") === 3)
						gain = màsGains.arts;
					else if(nNotes && r.includes(":art:") && r.includes(":performing_arts:"))
						gain = màsGains.brelanArts;
					else if(r.count(":four_leaf_clover:") === 3)
						gain = màsGains.trèfles;
					else if(r.count(":fleur_de_lis:") === 3)
						gain = màsGains.lys;
					else if(r.count(":gem:") === 3)
						gain = màsGains.diamants;
					else if(nMedailles[2] === 3)
						gain = màsGains.médailles / 20;
					else if(nMedailles[1] === 3)
						gain = màsGains.médailles / 10;
					else if(nMedailles[0] === 3 || r[0] === ":first_place:" && r[1] === ":second_place:" && r[2] === ":third_place:")
						gain = màsGains.médailles;
					else if(nMedailles[0] && nMedailles[1] && nMedailles[2])
						gain = màsGains.médailles / 5;
					else if(r.count(":trident:") === 2)
					{
						for(let i = 0 ; i < 3 ; i++)
						{
							if(r[i] === ":trident:")
								continue;

							gain = màsChiffres.indexOf(r[i]);
							if(gain > 0)
								gain *= 1000;
							else
								gain = r[i] === ":gem:" ? (màsGains.diamants + màsGains.tridents) / 2 : 0;

							break;
						}
					}
					else if(r.count(":trident:") === 3)
						gain = màsGains.tridents;

					statsCasinouz.màs.j++;
					statsCasinouz.màs.b += gain ? -gain : 10;


					réponse += "\n" + r.join("");

					if(nEffectués === 8 || gain)
					{
						channel.send(réponse);
						réponse = "";
						nEffectués = 0;
					}

					if(gain)
					{
						if(gain === màsGains.tridents)
							channel.send(`JACKPOT ${author} ! **JAAAACKPOOOOOT !!!** (${gain.toZouz()})`);
						else
							channel.send(`${gain > 1000 ? "Wow " : ""}${author}, tu as gagné ${gain.toZouz()} !`);
						z.zouz += gain;
					}
				} // for(let i = 0 ; i < nEssais ; i++)

				if(réponse)
					channel.send(réponse);
			}
			break;
		}

		case "paribarbu": {
			if(prem === "")
				reply("Hm... tu es censé miser quelque chose.");
			else if(!isFinite(prem))
				reply({content: `${prem} ? C’est quoi ce nombre ?`, allowedMentions: {parse: []}});
			else
			{
				prem = Number(prem);
				if(prem < 100)
					reply(`Tu dois miser au moins 100 ƶ.`);
				else if(prem !== ~~prem)
					reply("Tu dois miser un nombre entier.");
				else if(prem > z.zouz)
					reply(`${insulteFric(true)}, tu n’as même pas assez pour miser ça !`);
				else
				{
					/* Mise 100 = une chance sur deux
					 * Mise 1000 = une chance sur trois
					 * Mise 10000 = une chance sur quatre
					 * etc
					 */
					let chances = 3 / Math.log10(prem ** 3), // maudits soient les connard·sse·s qui écrivent "log" pour le logarithme népérien
						gains = Math.ceil(prem / chances),
						m = "Popopopo...\n\nP-P-";

					if(checkMaster(author))
						chances += 0.1;
					else
					{
						if(prem >= 30000)
							chances *= 0.85;
						else if(prem >= 20000)
							chances *= 0.9;
						else if(prem >= 10000)
							chances *= 0.95;
					}

					if(z.zouz < seuil.pauvreté)
						chances *= 1.3;
					else if(z.zouz < seuil.pauvreté * 2)
						chances *= 1.15;
					else if(z.zouz > seuil.richesse && !checkMaster(author))
						chances -= 0.1;

					if(!statsCasinouz.pariBarbu)
						statsCasinouz.pariBarbu = { j: 0, b: 0 };

					z.zouz -= prem;

					if(Math.random() < chances)
					{
						const gainsM = gains.toLocaleString("fr");
						m += ["PUTAIN !.. ", "PUNAISE !.. ", "PURÉE !.. "].rand()
							+ [`Bon, tiens, ${gainsM} ƶ.`, `T’as gagné. Prends tes ${gainsM} ƶ et fais pas chier.`, `Tu remportes ${gainsM} ƶ.`].rand()
							+ ["", " Allez, je suis sûr que tu veux les remettre en jeu.", " On remet ça ? Faut miser plus, je sens que c’est ton jour de chance.", " Allez, un autre. Tant que tu gagnes, tu joues, pas vrai ?"].rand();
						statsCasinouz.pariBarbu.b -= gains;
						z.zouz += gains;
					}
					else
					{
						const article = z.titres?.article || "lia";
						const chanceuxe = article === "le " ? "chanceux" : article === "la " ? "chanceuse" : "chanceuxe";
						m += ["PERDU ! ", "PAS D’BOL ! ", "PAS D’POT ! "].rand()
							+ ["Je garde ta mise.", "Par ici les pépettes !"].rand()
							+ [` Héhé, tu seras peut-être plus ${chanceuxe} la prochaine fois.`, " Bon, te laisse pas abattre. On recommence ?", " Allez, un autre, tu finiras bien par gagner.", ""].rand();
						statsCasinouz.pariBarbu.b += prem;
					}

					statsCasinouz.pariBarbu.j++;
					reply(m);
				}
			}
			break;
		}

		case "rlt":
		case "roulette": {
			if(["", "?", "aide", "info", "infos"].includes(prem.toLowerCase()))
			{
				channel.send(`Vous devez miser au moins 100 ƶ, et effectuer un pari sur le résultat, qui sera entre 0 et 36 (inclus). Plus votre pari est précis, plus vous gagnez de zouz. Les paris possibles sont réduits par rapport à la vraie roulette parce qu’en ligne de commande, c’est pas facile à jouer. Votre pari peut être :
__Un numéro__ : remportez 36 fois votre mise
__Deux numéros__ (par ex. 8,12) : 18 fois la mise
__Une ligne__ (par ex. L7, ce qui correspond aux numéros 7 à 9) : 12 fois la mise *(note : L8 et L9 correspondent à la même ligne que L7)*
__Un tiers__ (par ex. t2, ce qui correspond aux numéros 13 à 24) : 3 fois la mise
__Pair__, __impair__, __rouge__, __noir__, __manque__ ou __passe__ : 2 fois la mise

Le zéro vous fait perdre quel que soit votre pari ! Vous ne pouvez donc pas parier sur le zéro.
https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Roulette_frz.png/371px-Roulette_frz.png
*Rappel de vocabulaire : "manque" signifie inférieur ou égal à 18, "passe" signifie supérieur à 18.*`);
				break;
			}
			else if(!Number(prem) && !Number(prm))
				reply("Je pige pas, c’est quoi ta mise là-dedans ?");
			else
			{
				let mise = prem, pari = prm;
				const res = ~~(Math.random() * 37);

				if(!isFinite(mise) || Number(mise) < 100)
				{
					const temp = mise;
					mise = pari;
					pari = temp;
				}

				mise = Number(mise);

				if(!mise || mise < 100)
					return reply("Tu dois miser au moins 100 ƶ !");

				mise = Number(mise);

				if(mise > z.zouz)
					reply(`${insulteFric(true)}, tu n’as même pas assez pour miser ça !`);
				else if(!pari)
					reply("Tu dois parier sur la case sur laquelle tombera la bille ! (fais `>zouz roulette` pour plus d’infos)");
				else
				{
					if(Number(pari))
					{
						pari = Number(pari);
						if(pari < 1 || pari > 36)
						{
							reply("Les numéros sont compris entre 0 et 36, et vous ne pouvez pas parier sur le zéro.");
							break;
						}
					}
					else if(pari[0] === "t" || pari[0] === "T")
					{
						pari = Number(pari[1]);

						if(!isFinite(pari) || pari < 1 || pari > 3 && pari !== 13 && pari !== 25)
						{
							reply("Si vous misez sur un tiers, cela doit être le tiers 1, 2 ou 3.");
							break;
						}
						else if(pari === 13)
							pari = -2;
						else if(pari === 25)
							pari = -3;
						else
							pari *= -1;
					}
					else if(pari[0] === "l" || pari[0] === "L")
					{
						const nLigne = parseInt(pari.substring(1));

						if(!isFinite(nLigne) || nLigne < 1 || nLigne > 36)
						{
							reply("Les numéros sont compris entre 0 et 36, et vous ne pouvez pas parier sur le zéro.");
							break;
						}

						pari = [nLigne];

						switch(nLigne % 3)
						{
							case 1: pari.push(nLigne + 1, nLigne + 2); break;
							case 2: pari.push(nLigne - 1, nLigne + 1); break;
							case 0: pari.push(nLigne - 1, nLigne - 2); break;
						}
					}
					else if((pari.match(/,/g) || []).length === 1)
					{
						pari = pari.split(/ *, */);

						if(pari.length !== 2)
						{
							reply("Vous ne pouvez parier que sur deux numéros avec cette syntaxe.");
							break;
						}
						else for(let i = 0 ; i < 2 ; i++)
						{
							if(!isFinite(pari[i]))
							{
								reply("Un de ces nombres n’est pas un nombre !");
								pari = null;
								break;
							}
							pari[i] = parseInt(pari[i]);

							if(pari[i] < 1 || pari[i] > 36)
							{
								reply("Les numéros sont compris entre 0 et 36, et vous ne pouvez pas parier sur le zéro.");
								pari = null;
								break;
							}
						}

						if(pari === null)
							break;
					}
					else switch(pari = pari.toLowerCase())
					{
						case "pair": case "impair": case "rouge": case "noir": case "manque": case "passe":
							break;
						default:
							reply("J’ai pas compris ton pari... Renseigne-toi avec `>zouz roulette`.");
							return false;
					}


					statsCasinouz.roulette.j++;

					const bille = `Rien ne va plus... **${res}** ! ${rouletteNumérosNoirs.includes(res) ? "Noir" : "Rouge"}, ${res % 2 ? "impair" : "pair"} et ${res > 18 ? "passe" : "manque"}.\n`;
					let gagné = false;

					if(res === 0)
						reply("Rien ne va plus... **0** !\nDommage !");
					else if(typeof pari === "number")
					{
						if(pari > 0)
						{
							if(pari === res)
							{
								gagné = true;
								z.zouz += mise * 35;
								statsCasinouz.roulette.b -= mise * 35;
								reply(`${bille}${["BORDEL DE CUL", "PUTAIN DE SA RACE", "CORNECOUILLE"].rand()} tu remportes 36 fois ta mise.`);
							}
						}
						else
						{
							switch(pari)
							{
								case -1:
									gagné = res < 13;
									break;

								case -2:
									gagné = res > 12 && res < 25;
									break;

								case -3:
									gagné = res > 24;
									break;

								default:
									reply(`Putain le buuuug, le pari valait ${pari} WTF`);
									return false;
							}

							if(gagné)
							{
								z.zouz += mise * 2;
								statsCasinouz.roulette.b -= mise * 2;
								reply(`${bille}Félicitations ! Tu remportes 3 fois ta mise.`);
							}
						}
					}
					else if(typeof pari === "string")
					{
						switch(pari)
						{
							case "pair":
								gagné = res % 2 === 0;
								break;

							case "impair":
								gagné = res % 2 === 1;
								break;

							case "rouge":
								gagné = !rouletteNumérosNoirs.includes(res);
								break;

							case "noir":
								gagné = rouletteNumérosNoirs.includes(res);
								break;

							case "manque":
								gagné = res <= 18;
								break;

							case "passe":
								gagné = res > 18;
								break;
						}

						if(gagné)
						{
							z.zouz += mise;
							statsCasinouz.roulette.b -= mise;
							reply(`${bille}Félicitations ! Tu remportes 2 fois ta mise.`);
						}
					}
					else if(pari instanceof Array)
					{
						for(const n of pari)
						{
							if(n === res)
							{
								gagné = true;
								break;
							}
						}

						if(gagné)
						{
							const coef = 36 / (pari.length - 1); // -1 pour compenser le fait qu'il perd sa mise et la regagne
							z.zouz += coef * mise;
							statsCasinouz.roulette.b -= coef * mise;
							reply(`${bille}Félicitations ! Tu remportes ${36 / pari.length} fois ta mise.`);
						}
					}


					if(!gagné)
					{
						z.zouz -= mise;
						statsCasinouz.roulette.b += mise;
						if(res)
							reply(bille + "Dommage !");
					}
				}
			}

			break;
		}

		default:
			return false;
	}

	saveCasinouz();
	return true;
}
