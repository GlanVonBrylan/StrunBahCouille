"use strict";

const commands = require("../../commands");

commands.registerCategory(["🚔", "moderation", "modération"], {
	nom: "Modération",
	color: 0xDD6611,
	title: "Commandes de modération",
	description: "Je peux les taper, dites, je peux ?",
	shortDescription: "Nettoyer les salons, bannir, réduire au silence...",
}, 20);

const {donne: aide} = require("../aide");
commands.register(null, ["modération", "moderation"], "",
({channel}) => aide(channel, "🚔"));

const register = exports.register = commands.register.bind(null, "🚔");
const inline = true;

const {insulte} = require("../../utils/Strun");
const {genre: {pronom, _e}} = require("../zouz");
const {
	client,
	checkMaster,
	getChannel,
	getUser, getMembers,
	PermissionFlags: {
		MANAGE_MESSAGES, MANAGE_CHANNELS, MANAGE_ROLES,
		KICK_MEMBERS,
		SEND_MESSAGES,
	},
} = require("../../discordCore");
const {
	ChannelType: { GuildCategory: GUILD_CATEGORY },
	OverwriteType,
} = require("discord.js");



const { ignoreMessage } = require("./spoy");


require("fs").readdirSync(__dirname).map(f => `./${f}`).forEach(require);


register(["nettoie", "clear", "clean"], "<n> [@mention]",
`Supprimer \`n\` messages (max 100), en plus de la commande. Ne peut pas supprimer les messages vieux de plus de 2 semaines.
Optionnellement, vous pouvez mentionner un utilisateur pour supprimer ses \`n\` derniers messages, tout en laissant les autres tranquilles. Exemple : \`>nettoie 10 @JeanMichel#1234\``,
async (msg, [n, ...prm]) => {
	const {author, member, channel, guild} = msg;

	if(!guild) return;

	const myself = await guild.members.fetch(client.user);

	if(!guild)
		return msg.reply("Je ne peux faire ça que dans un serveur.");

	prm = prm.join(" ");

	if(!member.permissions.has(MANAGE_MESSAGES))
		channel.send(`Je ne vais pas supprimer des messages pour toi alors que tu n’en as pas toi-même le pouvoir, ${insulte(author)} !`);
	if(!myself.permissions.has(MANAGE_MESSAGES))
		channel.send("Alors je veux bien mais je peux pas.");
	else if(!n)
		channel.send("Tu dois me dire combien en supprimer.");
	else if(!isFinite(n))
		channel.send({content: `${n} ? C’est quoi ce nombre ?`, allowedMentions: {parse: []}});
	else if(n < 1 || !Number.isInteger(+n))
		channel.send("Haha, très drôle.");
	else if(!prm)
	{
		ignoreMessage(msg);
		channel.bulkDelete(Math.max(2, Math.min(+n+1, 100)), true); // +1 pour supprimer la commande
	}
	else
	{
		const offender = await getUser(prm);

		if(!offender)
			return channel.send("Je n’ai pas trouvé ce membre.");

		let deleted = 0;

		try {
			await msg.delete();
			do {
				const msgs = [...(await channel.messages.fetch({limit: 100})).filter(m => m.author === offender).values()];

				if(!msgs.length)
					break;
				else if(msgs.length + deleted > n)
					msgs.length = n - deleted;

				ignoreMessage(msg);
				await channel.bulkDelete(msgs, true);
				deleted += msgs.length;
			}
			while(deleted < n);
		}
		catch(e) { error(e); }
	}
});


register(["expulse", "kick", "FusRoDah"], "<@mention>",
"Expulse du serveur la personne mentionnée. Ne fonctionne que si vous avez le droit d’expulser des membres, et si j’ai la permission d’expulser la personne demandée.",
async ({author, member: asker, channel, guild, mentions}, prm) => {
	if(!guild) return;

	const myself = await guild.members.fetch(client.user);

	if(!asker.permissions.has(KICK_MEMBERS))
		channel.send(`Je ne vais pas expulser un membre en ton nom alors que tu n’en as pas toi-même le pouvoir, ${insulte(author)} !`);
	else if(!myself.permissions.has(KICK_MEMBERS))
		channel.send("FUS REUh \\*teuh\\* \\*teuh\*. Ahem. Je suis un peu enroué.");
	else
	{
		const {position: myPosition} = myself.roles.highest;
		const {position: askerPosition} = asker.roles.highest;
		const reason = `Parce que ${author.tag} me l’a demandé.`;
		let nm = 0;

		for(const member of mentions.members.size ? mentions.members.values() : await getMembers(guild, prm.join(" ")))
		{
			nm++;
			const {position} = member.roles.highest;

			if(member === myself)
				channel.send(`Je ne peux pas me FUS RO DAH moi-même !`);
			else if(checkMaster(member))
				channel.send(`Hors de question que je FUS RO DAH mon créateur !`);
			else if(position >= askerPosition)
				channel.send(`${author} ${insulte(author)}, je ne vais pas expulser ${member.displayName} en ton nom, ${pronom(member)} est est ${position === askerPosition ? "aussi" : "plus"} haut gradé${_e(member)} que toi !`);
			else if(!member.kickable)
				channel.send(`Je ne peux pas expulser ${member.displayName}, ${pronom(member)} est ${position === myPosition ? "aussi" : "plus"} haut gradé${_e(member)} que moi ! 🙁`);
			else
			{
				channel.send("**FUS RO DAH !**");
				member.kick(reason).then(m => channel.send(`Eh voilà, ${m} a été éjecté${_e(m)} !`), error);
			}
		}

		if(!nm)
			channel.send("Attends, qui ?");
	}
});


register(["silence", "mute", "tg"], "<@mentions>",
"Réduit au silence un ou plusieurs membres dans le salon où la commande a été envoyée. Vous pouvez ajouter une raison après un retour à la ligne.",
async (msg, prm) => {
	const {author, member: asker, channel, guild, mentions} = msg;
	if(!guild) return;

	const myself = await guild.members.fetch(client.user);

	if(!(asker.permissionsIn(channel).has(MANAGE_ROLES)))
		channel.send(`Je ne vais pas réduire au silence un membre en ton nom alors que tu n’en as pas toi-même le pouvoir, ${insulte(author)} !`);
	else if(!(myself.permissionsIn(channel).has(MANAGE_ROLES)))
		channel.send(`Je l’f’rais bien, mais j’peux pô, j’ai pô la permission dans c’salon.`);
	else
	{
		let reason = msg.content.indexOf("\n") + 1;
		reason = reason ? `${author.tag} a dit : ${msg.content.substring(reason)}` : `Demandé par ${author.tag}`;

		const {position: myPosition} = myself.roles.highest;
		const {position: askerPosition} = asker.roles.highest;

		let nm = 0;
		const overwrites = channel.permissionOverwrites;

		for(const member of mentions.members.size ? mentions.members.values() : await getMembers(guild, prm.join(" ")))
		{
			nm++;
			const {position} = member.roles.highest;

			if(member === myself)
			{
				msg.reply(`${insulte(author)}, je ne vais pas me réduire au silence tout seul !`);
				continue;
			}
			if(position >= askerPosition)
			{
				msg.reply(`${insulte(author)}, je ne vais pas réduire ${member.displayName} au silence en ton nom, ${pronom(member)} est est ${position === askerPosition ? "aussi" : "plus"} haut gradé${_e(member)} que toi !`);
				continue;
			}
			if(position >= myPosition)
			{
				channel.send(`Je ne peux pas réduire ${member.displayName} au silence, ${pronom(member)} est ${position === myPosition ? "aussi" : "plus"} haut gradé${_e(member)} que moi ! 🙁`);
				continue;
			}

			if(checkMaster(member.user))
			{
				if(checkMaster(author))
				{
					msg.reply("Je dois vraiment vous réduire au silence ? Nan...");
					continue;
				}
				else
					channel.send(`Je suis désolé, cher créateur... Ce sont ses ordres et ${pronom(asker)} est plus haut gradé que vous !`);
			}
			overwrites.edit(member, {SendMessages: false}, {reason}).then(c => c.send(`${member}, **FERHM TA GUEULL !**`), error);
		}

		if(!nm)
			msg.reply("attends, qui ?");
	}
}, {inline});


register({names: ["parle", "unmute", "dé-silence"], hidden: ["de-silence", "désilence", "desilence"]}, "<@mentions>",
"Rend la parole à un ou plusieurs membres dans le salon où la commande a été envoyée. La raison est facultative et doit être mise après un retour à la ligne.",
async (msg, prm) => {
	const {author, member: asker, channel, guild, mentions} = msg;
	if(!guild) return;

	const myself = await guild.members.fetch(client.user);

	if(!(asker.permissionsIn(channel).has(MANAGE_ROLES)))
		channel.send(`Je ne vais pas rendre la parole à un membre en ton nom alors que tu n’en as pas toi-même le pouvoir, ${insulte(author)} !`);
	else if(!(myself.permissionsIn(channel).has(MANAGE_ROLES)))
		channel.send(`Je l’f’rais bien, mais j’peux pô, j’ai pô la permission dans c’salon.`);
	else
	{
		let reason = msg.content.indexOf("\n") + 1;
		reason = reason ? `${author.tag} a dit : ${msg.content.substring(reason)}` : `Demandé par ${author.tag}`;

		const overwrites = channel.permissionOverwrites.cache;
		const {position: myPosition} = myself.roles.highest;
		const {position: askerPosition} = asker.roles.highest;

		let nm = 0;

		for(const member of mentions.members.size ? mentions.members.values() : await getMembers(guild, prm.join(" ")))
		{
			nm++;
			const {position} = member.roles.highest;

			if(position >= askerPosition)
			{
				msg.reply(`${insulte(author)}, je ne vais pas rendre la parole à ${member.displayName} en ton nom, ${pronom(member)} est est ${position === askerPosition ? "aussi" : "plus"} haut gradé${_e(member)} que toi !`);
				continue;
			}
			if(position >= myPosition)
			{
				channel.send(`Je ne peux pas rendre la parole à ${member}, ${pronom(member)} est ${position === myPosition ? "aussi" : "plus"} haut gradé${_e(member)} que moi ! 🙁`);
				continue;
			}

			const overwrite = overwrites.get(member.id);

			if(overwrite?.type === OverwriteType.Member)
			{
				if(overwrite.deny.bitfield === SEND_MESSAGES && !overwrite.allow.bitfield)
					overwrite.delete(reason).then(o => channel.send(`${member}, **PAHRL CHA KAL !**`), error);
				else
					overwrite.edit({SendMessages: null}, {reason}).then(o => channel.send(`${member}, **PAHRL CHA KAL !**`), error);
			}
		}

		if(!nm)
			msg.reply("attends, qui ?");
	}
}, {inline});



require("discord.js").GuildMember.prototype.canDeleteChannel = function(chan) {
	return chan.permissionsFor(this).has(MANAGE_CHANNELS);
};
register({names: ["supprimeCatégorie", "suppCat"], hidden: ["supprimeCategorie", "supprimeCat", "supprCat", "supCat"]}, "<catégorie>",
"Supprime la catégorie indiquée et tous les salons qu’elle contient. `<catégorie>` doit être l’ID ou le nom exact de la catégorie à supprimer.",
(msg, prm) => {
	const {channel, guild} = msg;
	if(!guild)
		return channel.send("C’est une commande pour serveurs, patate !");

	if(prm.length)
		getChannel(prm.join(" ").trim(), guild).then(deleteCategory.bind(null, msg), error);
	else
		channel.send("Vous devez préciser la catégorie à supprimer en donnant son ID ou son nom exact.");
});

function deleteCategory({author, member, channel, guild}, category)
{
	if(!category)
		return channel.send("g pa trouvé\nTu as bien écrit le nom ? Il doit être exact, avec la casse, les accents et tout.");
	if(category.type !== GUILD_CATEGORY)
		return channel.send(`${category} n’est pas une catégorie.`);
	if(!member.canDeleteChannel(category))
		return channel.send(`Tu n’as pas la permission de supprimer ${category}, ${insulte(author)} !`);
	if(!category.deletable)
		return channel.send(`Je supprimerai bien ${category} mais j’ai pas les perms.`);

	const {children} = category;
	const cantDel = children.filter(({deletable}) => !deletable);
	const memberCantDel = children.filter(chan => !member.canDeleteChannel(chan));

	if(memberCantDel.length)
		return channel.send(memberCantDel.length === 1 ? `Tu ne peux pas supprimer ${memberCantDel[0]}, donc c’est non.` : `${category} contient ${memberCantDel.length} salons que tu ne peux pas supprimer, donc c’est non.`);
	if(cantDel.length)
	{
		const n = cantDel.length;
		return channel.send(`Je ne peux pas supprimer ${n > 1 ? `${n} salons de cette catégorie` : cantDel[0]}. Merci de me donner les droits ou de ${n > 1 ? "les" : "le"} retirer de ${category}.`);
	}

	const nChans = children.size;
	const replyChannel = channel.parent?.id === category.id ? author : channel;

	Promise.allSettled([...children.map(chan => chan.delete()), category.delete()]).then(results => {
		let failed = 0;
		const catResult = results.pop();

		if(catResult.status === "rejected")
		{
			error(catResult.reason);
			replyChannel.send(failed === nChans
				? "Pour des raisons obscures, je n’ai rien réussi à supprimer."
				: `Je n’ai pas réussi à supprimer ${category}, mais j’ai supprimé ${nChans - failed} de ses salons.`
			);
		}
		else
		{
			for(const result of results)
			{
				if(result.status === "rejected")
				{
					error(result.reason);
					failed++;
				}
			}
			replyChannel.send(failed ?
				`J’ai supprimé **${category.name}** mais seulement ${nChans - failed} de ses ${nChans} salons.`
				: `Vualà, j’ai supprimé **${category.name}**${nChans ? (` et ${nChans > 1 ? `ses ${nChans} salons` : "son seul salon"}`) : ""}.`
			);
		}
	}, error);
}
