"use strict";


module.exports = exports = {
	default: {
		maxRolls: 30,
		slowMode: 0,
		prefix: ">",
		yo: false,
		ephemeride: null,
		retenue: false,
	},
	version: 13, // PENSER À METTRE À JOUR EN AJOUTANT DES OPTIONS

	rules: {
		maxRolls: {
			type: "integer",
			bounds: [0, 100],
		},
		slowMode: {
			type: "integer",
			bounds: [0, 3600],
		},
		prefix: {
			type: "protected",
		},
		yo: {
			type: "boolean",
		},
		ephemeride: {
			type: "protected",
		},
		retenue: {
			type: "boolean",
		},
	},
};
