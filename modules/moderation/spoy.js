"use strict";

const { Message } = require("discord.js");
const {
	client,
	checkMaster,
	isAdmin, jpeuxÉcrire,
	getChannel,
	confirm,
	PermissionFlags: { VIEW_CHANNEL },
} = require("../../discordCore");
const { insulte } = require("../../utils/Strun");
const servers = require("../../savedData/DataSaver.class")("spoy", { channel: null, logEdits: true, logAdmins: false, ignoredChannels: [] });

const ignoreDeletion = new Set();

const commands = require("../../commands");
const inline = true;

commands.registerCategory(["🕵🏽", "spoy"], {
	nom: "SPOY",
	thumbnail: { url: "https://i62.servimg.com/u/f62/17/10/54/90/gentle10.png" },
	color: 0xFF1111,
	title: "Système Protecteur Observateur et Yssentiel",
	description: `Le SPOY est un système qui permet d’archiver dans un salon tous les messages supprimés ou modifiés dans votre serveur. Ainsi, les divers margoulins de bas-étage qui croient s’en tirer en effaçant les preuves de leurs méfaits se trompent lourdement !\n\n**Commandes :**`,
	shortDescription: "Le système d'archivage des messages supprimés",
}, 21);


function spoyify(name) { return name instanceof Array ? name.map(spoyify) : ("spoy "+name); }
function register(names, ...args)
{
	if(names.hidden)
	{
		names.hidden = spoyify(names.hidden);
		names.names  = spoyify(names.names );
	}
	else
		names = spoyify(names);

	commands.register("🕵🏽", names, ...args);
}


const {donne: aide} = require("../aide");

commands.register("🕵🏽", "spoy", "<#salon>",
"Active le SPOY sur ce serveur dans le salon indiqué. Mettez `stop` pour désactiver le SPOY.",
(msg, [cmd, ...prms]) => {
	if(!msg.guild)
		return channel.send("Le SPOY n’a de sens que dans un serveur, banane.");

	if(cmd)
	{
		if(cmd === "?" || cmd === "aide" || cmd === "infos")
			aide(msg.channel, "🕵🏽", checkMaster(msg.author));
		else if(commands("spoy "+cmd.toLowerCase(), msg, prms) === null)
			c_set(msg, cmd);
	}
	else
	{
		const {channel, member, guild} = msg;
		const chan = guild.channels.cache.get(servers.get(guild.id).channel);

		if(chan)
		{
			if(chan.permissionsFor(member).has(VIEW_CHANNEL))
				channel.send(`Sur ce serveur, le SPOY est activé dans ${channel}.`);
			else
				channel.send("Cette information est classifiée.");
		}
		else
		{
			if(isAdmin(member))
				channel.send("Le SPOY est désactivé sur ce serveur.");
			else
				channel.send("Cette information est classifiée.");
		}
	}
});



const loggable = { logEdits: "modification", logAdmins: "admins" };
function log(what, msg, enable = "")
{
	const {guild, channel} = msg;

	if(!guild)
		return channel.send("Vous devez utiliser cette commande dans un serveur.");

	what = "log"+what;
	enable = enable.toLowerCase();
	const settings = servers.get(guild.id);

	if(enable === "oui" || enable === "true")
		enable = true;
	else if(enable === "non" || enable === "false")
		enable = false;
	else
	{
		channel.send(enable
			? "Les valeurs possibles sont `oui` et `non`."
			: `L’archivage des ${loggable[what]} est ${settings[what] ? "activé" : "désactivé"}${settings.channel || !settings[what] ? "." : ", quoiqu’inactif car le SPOY lui-même est désactivé."}`
		);
		return;
	}

	settings[what] = enable;
	servers.save();
	confirm(msg);
}

register({names: "logEdits", hidden: ["edits", "edit"]}, "<oui|non>",
"(Dés)active l’archivage des modifications de messages. Exemple : `>logEdits oui`",
(msg, [logEdits]) => log("Edits", msg, logEdits),
{inline});

register({names: "logAdmins", hidden: ["logadmin", "admins"]}, "<oui|non>",
"(Dés)active le SPOY pour les admins. Exemple : `>logAdmins non`",
(msg, [logAdmins]) => log("Admins", msg, logAdmins),
{inline});



register("ignore", "<#salons>",
"Ajoute le(s) salon(s) mentionné(s) à la liste des salons ignorés, c’est-à-dire ceux dont les messages supprimés/modifiés ne seront pas archivés. Vous pouvez soit mentionner les salons, soit donner directement leurs ids.",
c_ignore_ouPas.bind(null, true));

register({names: "ignorePas", hidden: "ignorePlus"}, "<#salons>",
"Retire le(s) salon(s) mentionné(s) de la la liste des salons ignorés. Vous pouvez soit mentionner les salons, soit donner directement leurs ids.",
c_ignore_ouPas.bind(null, false),
{inline});

register({names: ["ignorés", "listeIgnorés"], hidden: ["ignores", "listeIgnores"]},
"Affiche la liste des salons ignorés par le SPOY sur ce serveur, ou sur tous les serveurs où vous êtes admin si vous envoyez cette commande en privé.",
c_ignorés,
{inline});
async function c_ignorés({guild, member, author, channel}) {
	if(guild)
	{
		if(!isAdmin(member))
			channel.send(`Tu dois être admin pour voir ça, ${insulte(author)} !`);
		else
		{
			const ignored = servers.get(guild.id).ignoredChannels;
			channel.send(ignored.length
				? `Salons ignorés sur ce serveur : ${ignored.map(chanId => `<#${chanId}>`).join(", ")}`
				: "Aucun salon n’est ignoré sur ce serveur.");
		}
	}
	else
	{
		let message = "Salons ignorés sur les serveurs où vous êtes admin :";
		const ignored = [], authId = author.id;

		for(const [id, stg] of servers)
		{
			const srv = client.guilds.cache.get(id);

			if(!srv)
				continue;

			const member = await srv.members.fetch(authId);

			if(member && isAdmin(member))
				ignored.push(`${srv} : ` + (stg.ignoredChannels.length ? stg.ignoredChannels.map(chanId => `<#${chanId}>`).join(", ") : "*aucun*"));
		}

		if(ignored.length)
			message += "\n" + ignored.join("\n");
		else
			message = "Heh, tu n’es admin nulle part !";

		channel.send(message);
	}
}


module.exports = exports = {
	ignoreMessage: msg => ignoreDeletion.add(msg.id),

	isIgnored: channel => {
		if(typeof channel === "string")
			channel = client.channels.cache.get(channel) || {};
		return channel.guild && servers.get(channel.guild.id).ignoredChannels.includes(channel.id);
	},
};


client.on("channelDelete", ({guild, id}) => {
	const settings = servers.get(guild.id);

	if(id === settings.channel)
	{
		settings.channel = null;
		servers.save();
		guild.fetchOwner().then(owner => owner.send(`Le salon de SPOY de ${guild} vient d’être supprimé.`), error);
	}
});



async function handleDeletedMsgs(msgs)
{
	msgs = msgs instanceof Message ? [msgs] : [...msgs.values()].reverse();

	const {guild, channel} = msgs[0];
	if(!guild)
		return;

	const settings = servers.get(guild.id);
	const spoyChanId = settings.channel;

	if(!spoyChanId || settings.ignoredChannels.includes(channel.id))
		return;

	msgs = msgs.filter(msg => {
		const { id, system, author: { bot }, member } = msg;
		if(bot || system)
			return false;

		if(ignoreDeletion.has(id))
		{
			ignoreDeletion.delete(id);
			return false;
		}
		return settings.logAdmins || !member || !isAdmin(member);
	});

	if(!msgs.length)
		return;

	let spoyChan = await guild.channels.fetch(spoyChanId);

	if(!spoyChan || !jpeuxÉcrire(spoyChan))
	{
		spoyChan = await guild.fetchOwner();
		if(!spoyChan)
			return;

		spoyChan.send(`Je ${spoyChan ? "peux pas écrire dans le" : "n’ai pas accès au"} salon de SPOY de ${guild} (<#${spoyChanId}>), du coup je vais envoyer ça là.`);
	}

	if(msgs.length === 1)
	{
		const [msg] = msgs;
		return spoyChan.send({embeds: [{
			...msgDeletionEmbed(msg),
			description: `**Message de ${msg.author} supprimé dans ${channel}**\n\n${msg.content}`
				.substring(0, 2056),
			timestamp: new Date().toISOString(),
		}]});
	}

	const embeds = msgs.map(msgDeletionEmbed);
	embeds[embeds.length - 1].timestamp = new Date().toISOString();

	spoyChan.send({ embeds: [{
		color: 0xDD4444,
		description: `**${msgs.length} messages supprimés en masse dans ${channel}**`,
	},
		...embeds.slice(0, 9),
	]});

	for(let i = 9 ; i < msgs.length ; i += 10)
		spoyChan.send({ embeds: embeds.slice(i, i+10) });
}

function msgDeletionEmbed({content, author, attachments})
{
	let attachmentN = 0;
	const embed = {
		color: 0xDD4444,
		author: { name: author.tag, icon_url: author.avatarURL() },
		description: content,
		fields: attachments.size
			? attachments.map(({url}) => ({ name: `Attachement ${++attachmentN}`, value: url }))
			: undefined,
	};

	return embed;
}

client.on("messageDelete", handleDeletedMsgs);
client.on("messageDeleteBulk", handleDeletedMsgs);


async function handleEditedMsg(oldM, newM)
{
	const guild = newM.guild;

	if(!guild)
		return;

	const settings = servers.get(guild.id);
	const spoyChanId = settings.channel;

	if(newM.author.bot
	   || !spoyChanId
	   || !settings.logEdits
	   || settings.ignoredChannels.includes(newM.channel.id)
	   || ( !settings.logAdmins && isAdmin(newM.member) )
	   || oldM.content === newM.content // For some weird reason embeds and previews cause an "update"
	   || newM.system)
		return;

	let spoyChan = await guild.channels.fetch(spoyChanId);

	if(!spoyChan || !jpeuxÉcrire(spoyChan))
	{
		spoyChan = await guild.fetchOwner();
		if(!spoyChan)
			return;

		spoyChan.send(`Je ${spoyChan ? "peux pas écrire dans le" : "n’ai pas accès au"} salon de SPOY de ${guild} (<#${spoyChanId}>), du coup je vais envoyer ça là.`);
	}

	const {author, channel} = newM;

	spoyChan.send({ embeds: [{
		color: 0xDD6D0C,
		author: { name: author.tag, icon_url: author.avatarURL() },
		description: `**Message de ${author} modifié dans ${channel}. [Voir](${newM.url})\nAncien message :**\n\n${oldM.content}`.substring(0, 2056),
		timestamp: (new Date()).toISOString(),
	}]});
}

client.on("messageUpdate", handleEditedMsg);



async function c_set(msg, spoyChannel)
{
	const {member, channel, guild} = msg;

	if(!guild)
		channel.send("Vous devez utiliser cette commande dans un serveur.");
	else if(!isAdmin(member))
		channel.send(`Tu dois être admin pour toucher à ça, ${insulte(member.user)} !`);
	else
	{
		if(spoyChannel.toLowerCase() === "stop")
		{
			spoyChannel = null;
			confirm(msg);
		}
		else
		{
			spoyChannel = await getChannel(spoyChannel, guild);

			if(!spoyChannel)
				return channel.send("Vous devez mentionner un salon de ce serveur auquel j’ai accès.\nSi vous tentiez de faire une autre commande, essayez `>spoy aide`.");
			else if(!spoyChannel.guild)
				return channel.send("Hé, ce salon n’est même pas dans un serveur !");
			else if(channel.guild !== guild)
				return channel.send("Hé, ce salon n’est pas dans le même serveur !");

			channel.send(`SPOY activé dans ${spoyChannel}${jpeuxÉcrire(spoyChannel) ? "." : ", enfin en principe, parce que je n’ai pas le droit d’y écrire."}`);

			spoyChannel = spoyChannel.id;
		}

		servers.get(guild.id).channel = spoyChannel;
		servers.save();
	}
}


async function c_ignore_ouPas(ignore, msg, prms)
{
	if(!prms.length)
		return c_ignorés(msg);
	const {chanel, author} = msg;

	for(const chan of prms)
	{
		if(!/[0-9]+/.exec(chan))
			channel.send(`Format de salon incorrect : \`${chan}\`. Vous devez faire une mention de salon ou donner son id.`);
		else
		{
			const target = await getChannel(chan);

			if(!target)
				channel.send(`Salon \`${chan}\` inconnu.`);
			else
			{
				let member = await target.guild.members.fetch(author.id);

				if(!member)
					channel.send("Vous n’êtes même pas dans le serveur où se trouve ce salon. -_-'");
				else if(!isAdmin(member))
					channel.send(`Tu dois être admin sur ${target.guild} pour toucher à ça, ${insulte(author)} !`);
				else
					setIgnored(ignore, target, msg);
			}
		}
	}
}

function setIgnored(ignored, chan, {channel, guild})
{
	const {ignoredChannels} = servers.get(guild.id);
	if(ignored)
	{
		if(ignoredChannels.includes(chan.id))
			channel.send("Mais... ce salon est déjà ignoré.");
		else
		{
			ignoredChannels.push(chan.id);
			channel.send(`${chan} ajouté à la liste des salons ignorés.`);
			servers.save();
		}
	}
	else
	{
		const index = ignoredChannels.indexOf(chan.id);

		if(index < 0)
			channel.send("Hm, ce salon n’était déjà pas ignoré.");
		else
		{
			ignoredChannels.splice(index, 1);
			channel.send(`Et voilà, ${chan} ne sera plus ignoré.`);
			servers.save();
		}
	}
}
