"use strict";

const commands = require("../../commands");

commands.registerCategory(["🛠", "reglage", "réglage", "reglages", "réglages"], {
	nom: "Réglages",
	color: 0xDD6611,
	title: "Réglages du bot",
	description: "Vous avez probablement envie de régler un truc ou deux... Le préfixe ou l’éphéméride, par exemple.",
	shortDescription: "Préfixe, éphéméride, réglages de commandes...",
}, 1);

const {donne: aide} = require("../aide");


const register = exports.register = commands.register.bind(null, "🛠");
const inline = true;

const { insulte, parseParam, niceJSONDisplay } = require("../../utils/Strun");
const { clone } = require("../../utils");
const { getMaster, checkMaster, getServer, isAdmin, confirm } = require("../../discordCore");


const {
	default: defaultSettings,
	version: settingsVersion,
	rules: settingsRules,
} = require("./réglages - défaut.js");


const servers = require("../../savedData/DataSaver.class")("servers", { settings: defaultSettings });
const { settings, saveSettings } = require("../../_settings");


if(settings.version < settingsVersion)
{
	for(const [id, srv] of servers)
	{
		// Get all missing settings
		srv.settings = Object.assign(clone(defaultSettings), srv.settings);

		// Delete obsolete ones
		for(const setting in srv.settings)
			if(!(setting in defaultSettings))
				delete srv.settings[setting];
	}

	settings.version = settingsVersion;

	saveSettings();
	servers.save();
	console.info(`Settings updated to version ${settings.version}.`);
}


function checkParam(name, value)
{
	const rule = settingsRules[name];
	switch(rule.type)
	{
		case "all":
			if(rule.bounds && !(rule.bounds[0] <= value && value <= rule.bounds[1]))
				return `Hors limite : doit être entre ${rule.bounds[0]} et ${rule.bounds[1]}.`;
			return true;

		case "boolean":
			return typeof value === "boolean" ? true : "Type incorrect : doit être booléen.";

		case "number":
		case "integer":
			if(isFinite(value))
			{
				if(rule.bounds && !(rule.bounds[0] <= value && value <= rule.bounds[1]))
					return `Hors limite : doit être entre ${rule.bounds[0]} et ${rule.bounds[1]}.`;
				if(rule.type === "integer" && value !== ~~value)
					return `Type incorrect : doit être un entier.`;
				return true;
			}
			else
				return `Type incorrect : doit être un ${rule.type === "number" ? "nombre" : "entier"}.`;

		case "string":
			return rule.regexp ? rule.regexp.test(value) : true;

		case "protected":
			return "Vous ne pouvez pas modifier ce paramètre. (ou du moins pas avec cette commande)";

		default:
			return `Type demandé inconnu : ${rule.type}.`;
	}
}



register({names: "infosRéglages", hidden: ["infosReglages", "infoRéglages", "infoReglages"]},
"Afficher les informations sur les différents réglages.",
({channel}) => {
	const master = getMaster();
	channel.send({ embeds: [{
		color: 0xDD6611,
		title: "Présentation des réglages",
		description: "Les réglages dits \"booléens\" doivent avoir la valeur \"oui\" ou \"non\" (sans les guillemets)\n——————————————————————————————————",
		fields: [
			{ name: "maxRolls", value: "Entier. Le nombre de dés maximum pouvant être lancés en une seule commande. Doit être compris entre 0 et 100, 0 désactivant la commande.\nDéfaut : 10" },
			{ name: "yo", value: "Booléen. Si activé, Strun répondra à certains mots-clés pour faire un (tout petit) peu de conversation, en disant « bonjour » ou « bonne nuit », par exemple.\nDéfaut : oui"},
			{ name: "slowMode", value: "Entier. Durée en secondes du mode lent que Strun activera dans le salon si un admin lance `FEHR MEH LAA`, `CAL MEH VUU` ou `ZA WARUDO`. De même, `LII BER TEH`, `DIIS QU TEH` et `PAA PO TEH` désactiveront le mode lent. Doit être compris entre 0 et 3600, 0 le désactivant.\nDéfaut : 30" },
		],
		footer: {
			text: "Créé par " + master.username,
			icon_url: master.avatarURL()
		}
	}]});
}, {inline});


register({names: "réglages", hidden: ["reglages", "getSettings"]},
"Obtenir les réglages du bot pour ce serveur.",
async ({author, member, channel, guild}, prms) => {
	if(prms.length && ["?", "aide", "info", "infos"].includes(prms[0].toLowerCase()))
		return aide(channel, "🛠");

	const target = await getServer(prms.join(" "));

	if(target && (!guild || guild.id !== target.id))
	{
		const member = await target.members.fetch(author.id);

		if(member && isAdmin(member) || checkMaster(author))
			author.send(`Réglages pour le serveur ${target.name} : \n${JSON.stringify(servers.get(target.id).settings)}`);
		else
			channel.send(`Tu n’es pas admin sur ${target}, ${insulte(author)} !`);
	}
	else if(guild)
		channel.send(isAdmin(member) || checkMaster(author)
						? `Réglages pour le serveur ${guild.name} : \n${niceJSONDisplay(servers.get(guild.id).settings)}`
						: `Cette commande est réservée aux admins, ${insulte(author)} !`);
}, {inline});


register({names: ["réinitialise", "reset"], hidden: ["reinitialise", "réinitialiser", "reinitialiser"]},
"Remet tous les réglages du serveur à leurs valeur par défaut.",
c_reset, {inline});
function c_reset(msg) {
	const {member, channel, guild} = msg;
	if(!guild)
		return;

	if(isAdmin(member))
	{
		servers.get(guild.id).settings = clone(defaultSettings);
		servers.save();
		confirm(msg);
	}
	else
		channel.send(`Cette commande est réservée aux admins, ${insulte(member.user)} !`);
}


register({names: "règle", hidden: ["regle", "set"]}, "<option> <valeur>",
"Changer une option. Exemple :\n`>règle yo non`\nLes valeurs possibles sont oui, non, et le reste est interprété comme une chaîne de caractères. Si vous laissez une valeur vide, elle sera interprétée comme \"non\".\nNotez que la casse est importante pour le nom de l’option, c’est-à-dire que `maxRolls` fonctionne, mais pas `maxrolls` ni `mAxRoLlS`.",
(msg, [règle, ...valeur]) => {
	const {member, channel, guild} = msg;
	if(!guild)
		return;

	if(!isAdmin(member))
		return channel.send(`Cette commande est réservée aux admins, ${insulte(member.user)} !`);

	if(!(règle in defaultSettings))
		channel.send("Cette option n’existe pas.");
	else
	{
		const check = checkParam(règle, valeur = parseParam(valeur.join(" ")));

		if(check === true)
		{
			servers.get(guild.id).settings[règle] = valeur;
			servers.save();
			confirm(msg);
		}
		else
			channel.send("Erreur : " + check);
	}
}, {inline});


register({names: "metÀDéfaut", hidden: ["metADefaut, setToDefault"]}, "<option>",
"Remet l’option indiquée à sa valeur par défaut.",
(msg, [prm]) => {
	const {member, channel, guild} = msg;
	if(!guild)
		return;

	if(!isAdmin(member))
		channel.send(`Cette commande est réservée aux admins, ${insulte(member.user)} !`);
	else if(prm === "all")
		c_reset(msg);
	else if(!(prm in defaultSettings))
		channel.send("Cette option n’existe pas.");
	else
	{
		servers.get(guild.id).settings[prm] = defaultSettings[prm];
		servers.save();
		confirm(msg);
	}
}, {inline});



register({names: "préfixe", hidden: ["prefixe", "prefix"]}, "[préfixe]",
"Change le préfixe des commandes de Strun (qui par défaut est `>`) pour ce serveur. Ne pas mettre le paramètre vous affiche le préfixe actuel.\nExemples : `>préfixe !`. Mettons qu’on le change encore : `!préfixe strun/`. Et encore : `strun/préfixe >`.",
(msg, [prm]) => {
	const {member, channel, guild} = msg;
	if(guild)
	{
		const {settings} = servers.get(guild.id);
		if(prm)
		{
			if(isAdmin(member))
			{
				settings.prefix = prm;
				servers.save();
				confirm(msg);
			}
			else
				channel.send(`Cette commande est réservée aux admins, ${insulte(member.user)} !`);
		}
		else
			channel.send(`Le préfixe pour ce serveur est \`${settings.prefix}\`.`);
	}
	else if(prm)
		channel.send("Vous devez être sur un serveur pour changer le préfixe.");
	else
		channel.send("Le préfixe par défaut est `>`.");
});
