"use strict";

const mois = [
	undefined, // Comme ça le 1er mois a l'index 1
	"vendémiaire", "brumaire", "frimaire", "nivôse", "pluviôse", "ventôse", "germinal", "floréal", "prairial", "messidor", "thermidor", "fructidor",
	"sans-culottides"
];
const sansCulottides = [
	undefined,
	"jour de la vertu",
	"jour du génie",
	"jour du travail",
	"jour de l’opinion",
	"jour des récompenses",
	"jour de la révolution" // bissextile
];
const jours = [
	"décadi", // Pour que cela fonctionne avec modulo 10
	"primidi",
	"duodi",
	"tridi",
	"quartidi",
	"quintidi",
	"sextidi",
	"septidi",
	"octidi",
	"nonidi",
	"décadi"
];
const vrai = true, faux = false;

const TableauAssociatif = Map, Promesse = Promise, ErreurDePlage = RangeError;

const erreur = error;
const { insulte } = require("../utils/Strun");
const { getChannel: obtientSalon, isAdmin: estAdmin, client } = require("../discordCore");
const serveurs = require("../savedData/DataSaver.class")("servers");
const éphémérides = new TableauAssociatif();


Promesse.prototype.attrape = Promesse.prototype.catch;
Math.arondit = Math.round;

Date.maintenant = Date.now;
Date.prototype.règleHeures = Date.prototype.setHours;
Date.prototype.règleMinutes = Date.prototype.setMinutes;
Date.prototype.règleSecondes = Date.prototype.setSeconds;
Date.prototype.obtientAnnée = Date.prototype.getFullYear;
Date.prototype.obtientMois = Date.prototype.getMonth;
Date.prototype.obtientJour = Date.prototype.getDate;
Date.prototype.obtientHeure = Date.prototype.getHours;
Date.prototype.obtientMinutes = Date.prototype.getMinutes;

String.prototype.commencePar = String.prototype.startsWith;

TableauAssociatif.prototype.obtient = TableauAssociatif.prototype.get;
TableauAssociatif.prototype.associe = TableauAssociatif.prototype.set;
TableauAssociatif.prototype.a = TableauAssociatif.prototype.has;
TableauAssociatif.prototype.supprime = TableauAssociatif.prototype.delete;
TableauAssociatif.prototype.valeurs = TableauAssociatif.prototype.values;


client.once("ready", async () => {
	function msAvantMinuit() {
		const demain = new Date();
		demain.règleHeures(24);
		demain.règleMinutes(0);
		demain.règleSecondes(1);
		return demain - Date.maintenant();
	}

	function prepareEphemeride() {
		informeDeLaDate();
		setTimeout(prepareEphemeride, msAvantMinuit());
	}
	setTimeout(prepareEphemeride, msAvantMinuit());

	for(const [id, { settings }] of serveurs)
	{
		const éphéméride = settings.ephemeride;
		if(!éphéméride) continue;
		
		try {
			const salon = await client.channels.fetch(éphéméride);
			éphémérides.associe(id, salon);
		}
		catch(err) {
			if(err.message.includes("Unknown Channel"))
				settings.ephemeride = null;
			else
			{
				err.serverId = id;
				erreur(err);
			}
		}
	}
});


require("./moderation/réglages")
.register({names: "éphéméride", hidden: "ephemeride"}, "[stop|#salon]",
"Définit le salon dans lequel, tous les jours à minuit, Strun dira la date. Mettez `stop` pour le désactiver. Ne pas mettre le paramètre vous affiche le salon actuellement désigné pour ça.",
async (msg, [prm]) => {
	const {author, member, channel, guild} = msg;
	if(!guild)
		return channel.send(`Non, je ne serai pas ton calendrier personnel, ${insulte(author)}.`).attrape(erreur);
	if(!estAdmin(member))
		return channel.send(`Cette commande est réservée aux admins, ${insulte(author)} !`).attrape(erreur);

	const {settings} = serveurs.get(guild.id);
	if(prm === "stop")
	{
		supprimeÉphéméride(guild.id);
		msg.react("✅").attrape(erreur);
	}
	else if(prm)
	{
		const salon = msg.mentions.channels.first() || await obtientSalon(prm);
		if(salon)
		{
			éphémérides.associe(guild.id, salon);
			settings.ephemeride = salon.id;
			msg.react("✅").attrape(erreur);
			serveurs.save();
		}
		else
			channel.send("Vous êtes censé mentionner un salon.").attrape(erreur);
	}
	else
	{
		const channelId = settings.ephemeride;
		channel.send(channelId ? `Sur ce serveur, l’éphéméride est activé dans <#${channelId}>.` : "L’éphéméride est désactivé sur ce serveur.").attrape(erreur);
	}
});



function supprimeÉphéméride(srvId)
{
	const stg = serveurs.get(srvId).settings;
	éphémérides.supprime(srvId);
	stg.ephemeride = null;
	serveurs.save();
}

client.on("guildDelete", srv => {
	const stg = serveurs.get(srv.id),
		  éphéméride = stg ? stg.settings.ephemeride : null;

	if(éphéméride)
		supprimeÉphéméride(éphéméride);
});
client.on("channelDelete", chan => {
	const stg = serveurs.get(chan.guild.id);

	if(stg && chan.id === stg.settings.ephemeride)
		supprimeÉphéméride(chan.guild.id);
});


function informeDeLaDate()
{
	const date = exports.datePrésente();
	const message = `T${['a','e','i','o','u'].rand()}ng, nous sommes désormais ${date[0] === 'o' ? "l’" : "le "}${date} !${date.commencePar("primidi 1er vendémiaire") ? "\nBonne année !  🎉 🇫🇷" : ""}`;

	for(const salon of éphémérides.valeurs())
		salon.send(message).attrape(erreur);
}


module.exports = exports = {
	jour(jour) {
		if(jour < 1 || jour > 30)
			throw new ErreurDePlage("Un jour républicain doit être compris entre 1 et 30 (inclus).");
		return jours[jour % 10];
	},

	mois(mois) {
		if(mois < 1 || mois > 12)
			throw new ErreurDePlage("Un mois républicain doit être compris entre 1 et 12.");
		return mois[mois];
	},

	sansCulottide(quelleDonc) {
		if(quelleDonc < 1 || quelleDonc > 6)
			throw new ErreurDePlage("Une sans-culottide doit être comprise entre 1 et 6.");
		return sansCulottides[quelleDonc];
	},

	// https://fr.geneawiki.com/index.php/Concordance_des_dates_des_calendriers_républicain_et_grégorien
	grégorienVersRépublicain(jour, mois, année) {
		let date = { année: année - 1792 };
		// On arondit plutôt que de tronquer à cause des imprécisions des floats
		let jourA = Math.arondit((new Date(année, mois - 1, jour) - new Date(année, 0, 0)) / 86400000) - 1; // -1 car n fait Wikipédia est décalé
		let dernier = new Date(année, 1, 29).obtientMois() === 2 ? 264 : 265;

		if(mois > 9 || mois === 9 && jour > 22)
			date.année++;

		if(jourA > dernier)
			jourA -= (dernier - 1);
		else
			jourA += 132;

		date.mois = ~~(jourA / 30);
		date.jour = jourA % 30;

		if(année > 1800 || année === 1800 && mois >= 3 && jour >= 1)
			date.jour--;

		if(date.jour < 1)
		{
			let finBrumaire = faux;
			if(date.mois !== 1) // Sinon le mois se retrouve à 0, et donc ensuite affecté à 13, et c'est le bordel
			{
				date.mois--;
				finBrumaire = date.mois === 1; // Sinon à la fin de ce if on réduit encore de 1 pour le 29 et 30 brumaire, qui se retrouvent traduits en vendémiaire
			}

			if(date.mois > 0)
				date.jour += 30;
			else
				date.mois = 13;

			if(!finBrumaire && date.mois === 1)
				date.mois--;
		}

		if(date.mois === 13)
		{
			date.jour = sansCulottides[date.jour];
			delete date.mois;
		}

		if(date.mois < 4)
			date.mois++;

		return date;
	},

	datePrésente() {
		let date = new Date();
		let nomJour;

		date = exports.grégorienVersRépublicain(date.obtientJour(), date.obtientMois() + 1, date.obtientAnnée());

		nomJour = jours[date.jour % 10];

		if(date.jour === 1)
			date.jour = "1er";

		if(date.mois)
			return `${nomJour} ${date.jour} ${mois[date.mois]} ${date.année}`;
		else
			return `${date.jour} ${date.année}`;
	},
};
