"use strict";

const splitMessage = require("../utils/splitMessage");
const commands = require("../commands");


const register = exports.register = commands.register.bind(null, null);
const masterOnly = true, inline = true;

const {insulte} = require("../utils/Strun");
const {
	client,
	getMaster, checkMaster,
	getUser, getChannel,
	exit,
	confirm,
} = require("../discordCore");
const {
	dmChan, setDMChan,
} = require("../_settings");



register("eval", "<code>",
"Exécute le code JavaScript donné. Attention à ne pas tout casser !",
(msg, prm) => {
	let res;
	try {
		res = eval(prm.join(" "));
		if(res === undefined)
			confirm(msg);
		else
		{
			const send = msg.channel.send.bind(msg.channel);
			function sendMsg(message) {
				splitMessage(message).forEach(send);
			}

			if(res instanceof Promise)
			{
				msg.channel.send("Le résultat arrive (promis)...");
				res.then(r => sendMsg(""+r), err => sendMsg(`Promise rejected:\n${err}`));
			}
			else
				sendMsg(`Résultat : ${res}`);
		}
	}
	catch(e) {
		getMaster().send(e.toString());
	}
}, {masterOnly, inline});


register("uptime",
"Indique depuis combien de temps je tourne.",
({channel}) => {
    let seconds = ~~process.uptime();
	let days    = ~~(seconds / 84400);
	seconds -= days * 84400;
    let hours   = ~~(seconds / 3600);
	seconds -= hours * 3600;
	let minutes = ~~(seconds / 60);
	seconds -= minutes * 60;

    if(hours   < 10) hours   = "0"+hours;
    if(minutes < 10) minutes = "0"+minutes;
    if(seconds < 10) seconds = "0"+seconds;

	channel.send(`${days} jour${days > 1 ? "s" : ""}, ${hours}:${minutes}:${seconds}`);
}, {masterOnly, inline});


register({names: "serveurs", hidden: ["servers", "guildes", "guilds"]}, "[n|tri]",
"Affiche les serveurs dans lesquels je suis. Si le paramètre `n` est fourni, je ne donnerai que leur nombre. Si `sort` est fourni, ils seront triés par nombre de membres.",
({channel}, [prm = ""]) => {
	let srvs = client.guilds.cache;
	const nSrvs = `**${srvs.size} serveur${srvs.size > 1 ? "s" : ""}**`;
	if(prm === "n")
		return channel.send(nSrvs);

	if(prm.toLowerCase().includes("tri"))
		srvs = [...srvs.values()].sort((a, b) => b.memberCount - a.memberCount);

	srvs = srvs.map(({name, id, memberCount}) => `\`${id} :\` ${name}  *(${memberCount} membres)*`);
	for(const chunk of splitMessage(`${nSrvs}\n${srvs.join("\n")}`))
		channel.send(chunk);
}, {masterOnly});

register(["proprios", "propriétaires", "owners"], "[n|tri]",
"Affiche les propriétaires des serveurs dans lesquels je suis. Si le paramètre `n` est fourni, je ne donnerai que leur nombre. Si `tri` est fourni, ils seront triés par nombre de serveurs.",
async ({channel}, [prm = ""]) => {
	if(prm === "n")
		return channel.send(`**${new Set(client.guilds.cache.map(g => g.ownerId)).size} proprios**`);

	await Promise.all(client.guilds.cache.map(async guild => guild.owner = (await guild.fetchOwner()).user));
	let owners = new Map();
	const tri = prm.toLowerCase().includes("tri");

	for(const { owner, name } of client.guilds.cache.values())
	{
		if(!owners.has(owner))
			owners.set(owner, { id: owner.id, tag: owner.tag, guildCount: 1 });
		else
			owners.get(owner).guildCount++;
	}

	owners = [...owners.values()];
	if(tri)
		owners.sort((a, b) => b.guildCount - a.guildCount);

	owners = owners.map(({ id, tag, guildCount }) =>
		`\`${id} :\` ${tag}  *(${guildCount} serveur${guildCount === 1 ? "" : "s"})*`);
	
	splitMessage(`**${owners.length} proprios**\n${owners.join("\n")}`)
		.forEach(channel.send.bind(channel));
}, {masterOnly});



register({names: "transfèreMP", hidden: ["transfèreMPs", "transfereMP", "transfereMPs"]}, "<?|master|salon|false>",
"Définis où envoyer les MP que je reçois : directement à vous (`master`), dans un salon défini, ou nulle part.\nPassez `?` en paramètre pour connaître le réglage actuel.",
({channel}, [prm]) => {
	if(!prm || prm === "?")
	{
		const chan = dmChan();
		channel.send(
			chan === getMaster() ? "Je vous envoie tous mes MP."
			: chan ? `J’envoie mes MP dans ${chan}.`
			: "Je ne transfère pas mes MP."
		);
	}
	else
	{
		setDMChan(prm);
		const chan = dmChan();

		channel.send(
			chan === getMaster() ? "Ok, je vous envoie tous mes MP."
			: chan ? `Ok, j’enverrai mes MP dans ${chan}.`
			: "J’arrête donc de transférer mes MP."
		);
	}
}, {masterOnly});



register(["mp", "dm"], "<id utilisateur> <message>",
"Envoie un message privé à l’utilisateur dont l’ID est donné.",
async ({channel}, [target, ...prm]) => {
	prm = prm.join(" ");

	if(!target)
		return channel.send("Il faut donner une cible.");
	else if(!prm)
		return channel.send("Il faut donner un message.");

	const user = await getUser(target);

	if(user)
		user.send(prm).then(m => channel.send(`Message envoyé à ${user}.`), error);
	else
		channel.send(`Utilisateur non trouvé : ${target}.`);
}, {masterOnly, inline});

register(["message", "msg"], "<salon> <message>",
"Envoie le message donné dans le salon donné. Le salon peut être son nom, ou directement son ID.",
async ({channel}, [target, ...prm]) => {
	prm = prm.join(" ");

	const chan = await getChannel(target)

	if(chan)
		chan.send(prm).then(({url}) => channel.send(`Message envoyé dans ${chan}.\n${url}`), error);
	else
		channel.send(`Salon non trouvé : ${target}.`);
}, {masterOnly, inline});


const GUILDS_RPLC = "#GUILDS#";
register("annonce", "<message>",
`Envoie un message à tous les propriétaires de serveur. \`${GUILDS_RPLC}\` sera remplacé par le nom de leurs serveurs.`,
async ({channel}, prms) => {
	await Promise.all(client.guilds.cache.map(async guild => guild.owner = (await guild.fetchOwner()).user));
	const owners = new Map();
	const message = prms.join(" ");
	const replace = message.includes(GUILDS_RPLC);
	if(replace && message.length > 1700)
		return channel.send(`Votre message fait ${message.length} caractères et avec les noms de serveur ça risque d'être trop long.`);
	else if(message.length > 1950)
		return channel.send(`Votre message fait ${message.length} caractères, c'est trop long.`);

	for(const { owner, name } of client.guilds.cache.values())
	{
		if(!owners.has(owner))
			owners.set(owner, []);
		if(replace)
			owners.get(owner).push(name);
	}

	owners.delete(getMaster());

	if(replace) for(const [owner, guilds] of owners)
		owner.send(message.replaceAll(GUILDS_RPLC, guilds.join(", "))).catch(Function());
	else for(const owner of owners.keys())
		owner.send(message).catch(Function());

	channel.send(`Message envoyé à ${owners.size} proprios.`);
}, {masterOnly, inline});


register("oublie", "<ids>",
"Efface toute info (zouz, Venture...) que Strun a des personnes indiquées.",
(msg, prms) => {
	prms.forEach(require("../savedData/DataSaver.class").removeFromAll);
	confirm(msg);
}, {masterOnly, inline});


register("quitte", "<serveur> [idSalon]",
"Quitte le serveur dont l’id est fourni. `idSalon` est le salon où Strun envoie le message disant qu’il se barre.",
c_quitte, {masterOnly});
async function c_quitte({channel}, [srvId, chanId]) {
	const srv = client.guilds.cache.get(srvId);

	if(!srv)
		return channel.send("Je ne connais pas ce serveur.");

	if(chanId)
	{
		const chan = await getChannel(chanId, srv);

		if(chan)
			chan.send("Par ordre de mon maître, je me casse d’ici. **WULD NAH KEST !**").finally(() => srv.leave());
		else
			channel.send(`Salon ${chanId} non trouvé dans ce serveur.`);
	}
	else
		srv.leave();
}

const dataSavers = require("../savedData/DataSaver.class");
function formatSize(size) {
	let unit = "o";
	if(size > 10000) {
		size /= 1024;
		unit = "kio";
	}
	if(size > 10000) {
		size /= 1024;
		unit = "Mio";
	}

	return `${size.toLocaleString("fr", {minimumFractionDigits: 2, maximumFractionDigits: 2})} ${unit}`;
}

register("taille", "<saver>",
"Retourne la taille totale des fichiers d’un `DataSaver`.",
({channel}, saverName) => {
	const saver = dataSavers(saverName.join(" "), null, false);
	channel.send(saver ? formatSize(saver.totalSize) : "Ce saver n’existe pas.");
}, {masterOnly, inline});

register(["getZip", "zip"], "[saver]",
"Récupère les fichiers du saver indiqué, ou de tous si rien n'est indiqué.",
(msg, saverName) => {
	saverName = saverName.join(" ");
	const {channel} = msg;
	let promises;

	if(saverName)
	{
		const saver = dataSavers(saverName, null, false);
		if(!saver)
			return channel.send("Ce saver n’existe pas.");

		promises = [
			msg.react("⚙️").catch(Function()),
			saver.zip().then(zip => channel.send({ files: [{attachment: zip, name: `${saverName}.zip`}] })),
		];
	}
	else
		promises = [
			msg.react("⚙️").catch(Function()),
			dataSavers.zipAll().then(zip => channel.send({ files: [{attachment: zip, name: "data.zip"}] })),
		];

	Promise.all(promises)
		.then(([reaction]) => reaction?.users.remove(client.user).catch(Function()));
}, {masterOnly, inline})


const {respect} = require("./zouz");

register({names: ["vire", "dégage", "couché"], hidden: "degage"},
"Me fait me déconnecter, ou quitter un serveur.",
({author, member, channel, guild}) => {
	if(guild)
	{
		if(checkMaster(author))
			c_quitte({channel}, [guild.id, channel.id]);
		else if(isAdmin(member))
			channel.send("Ah ? Ok... À la revoyure.").finally(() => guild.leave());
		else
			channel.send(respect(author) ? "Sauf vot’ respect... **non.**" : `Si je veux, ${insulte(author)}.`);
	}
	else if(checkMaster(author))
		channel.send("Ok, à plus. **WULD !**").catch(err => { error(err); exit(); }).finally(exit);
});
