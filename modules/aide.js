"use strict";

const { clone } = require("../utils");
const { client, fetchMaster, checkMaster } = require("../discordCore");
const {
	ComponentType: {ActionRow: ACTION_ROW, StringSelect: STRING_SELECT},
	InteractionType: {MessageComponent: MESSAGE_COMPONENT}
} = require("discord.js");

const { infos, masterInfos } = require("../commands");


var master;
fetchMaster().then(m => master = m);
var pages, pagesMaster, defaultSelectOptions, selectOptions = {};
const catAliases = {};

const commandsForMaster = {
	color: 0xBBFFBB,
	title: "Commandes rien que pour vous, maître Morgân :",
	description: "————————————————————————————————",
};


require("../commands").registerCategory(["🔨", "utile", "utiles"], {
	nom: "Utiles",
	color: 0x1166DD,
	title: "Commandes utiles",
	description: "Je suis utile, des fois. Si, si !",
	shortDescription: "Plein de commandes utiles",
});

require("../commands").register("🔨", {names: "aide", hidden: "help"},
"Affiche l’aide, lol.",
({prefix, author, channel, guild}, [debut]) => {
	if(guild)
		guild.prefix = prefix;

	donneAide(channel, debut?.toLowerCase(), checkMaster(author));
}, {inline: true});


client.once("ready", function initPages() {
	pages = {};
	pagesMaster = {};
	commandsForMaster.fields = masterInfos.map(cmdInfoToFields);
	const footer = {
		text: `Aide de ${client.user.username}`,
		icon_url: client.user.displayAvatarURL(),
	};

	for(const [emoji, {aliases, commands, embedInfos}] of infos)
	{
		for(const alias of aliases)
			catAliases[alias] = emoji;

		const [forMaster, regular] = commands.partition(cmd => cmd.masterOnly);
		if(forMaster.size)
			pagesMaster[emoji] = { ...commandsForMaster, fields: forMaster.map(cmdInfoToFields) };

		pages[emoji] = { ...embedInfos, fields: regular.map(cmdInfoToFields), footer };
	}

	const sortedPages = [...infos.values()].sort((a, b) => a.priority - b.priority);
	defaultSelectOptions = sortedPages.map(pageToOption);
	for(let i = 0 ; i < sortedPages.length ; i++)
	{
		const options = [...defaultSelectOptions];
		options[i] = pageToOption(sortedPages[i]);
		options[i].default = true;
		selectOptions[options[i].emoji] = options;
	}


	function cmdInfoToFields({aliases, params, description, inline}, name) {
		return {
			name: params ? `${name} ${params}` : name,
			value: aliases?.length ? `*Alias : >${aliases.join(", >")}*\n${description}` : description,
			inline,
		};
	}

	function pageToOption({emoji, embedInfos: {title, shortDescription}},) {
		return { emoji, label: title, value: emoji, description: shortDescription };
	}
});


function prépareEmbed(embed, guild)
{
	const préfixe = guild?.prefix || ">";

	embed = clone(embed);

	for(const field of embed.fields)
	{
		field.name = préfixe + field.name;
		if(préfixe !== ">")
			field.value = field.value.replace(/>/g, préfixe);
	}

	return embed;
}

function selectMenu(pageEmoji)
{
	return [{ type: ACTION_ROW, components: [{
		type: STRING_SELECT, customId: "aide",
		options: pageEmoji ? selectOptions[pageEmoji] : defaultSelectOptions,
	}]}];
}

function switchHelp(interaction, emoji)
{
	const page = pages[emoji];
	if(page)
		interaction.update({ embeds: [prépareEmbed(page, interaction.guild)], components: selectMenu(emoji) });
}


exports.donne = donneAide;
function donneAide(channel, début, isMaster)
{
	if(typeof début !== "string" && isMaster === undefined)
		isMaster = début;

	const {guild} = channel;

	if(typeof début === "string")
	{
		const cat = catAliases[début] || début;
		const page = pages[cat];

		if(!page)
			channel.send(`Rha mince, on me demande d’afficher la page d’aide \`${début}\` mais y a pas.`);
		else
		{
			channel.send({ embeds: [prépareEmbed(page, guild)], components: selectMenu(cat) });
			if(isMaster && !guild && cat in pagesMaster)
				channel.send({ embeds: [pagesMaster[cat]] });
		}
	}
	else
	{
		channel.send({ embeds: [{
			  color: 0x1166DD,
			  title: "Commandes",
			  description: `Bienvenue sur mon aide. Utilisez le menu ci-dessous pour passer d’une catégorie à une autre.

Si vous voulez que je me présente, faites \`${guild?.prefix || ">"}tki\`

Veuillez noter que chaque fois qu’une @mention est demandée, il peut s’agir d’une mention, du tag de l’utilisateur (de la forme *user#1234*), ou de son id. **Notez cependant** que vous ne pouvez pas mélanger les trois.
Pour information également par rapport aux paramètres : les paramètres entre chevrons sont <obligatoires>. Ceux entre crochets sont [facultatifs]. Ceux qui n’ont ni l’un ni l’autre sont obligatoires et doivent être mis tels qu’indiqué.`,
			  footer: {
				  text: "Créé par " + master.username,
				  icon_url: master.avatarURL()
			  }
		}], components: selectMenu()});

		if(isMaster && !guild)
			channel.send({ embeds: [commandsForMaster] });
	}
}


client.on("interactionCreate", inter => {
	const { type, user: {bot}, customId, values } = inter;
	if(inter.type === MESSAGE_COMPONENT && !inter.user.bot && customId === "aide")
		switchHelp(inter, values[0]);
})
