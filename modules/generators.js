"use strict";

const util = require("../utils");

const jurons = ["Par les poils de cul du père Fouettard", "Bordel à queue", "Bordel à cul", "Nom de dieu de putain de bordel de merde de saloperie de connard d’enculé de ta mère", "Bordel de caleçon de Merlin le putain d’enchanteur", "Rogntudju", "Sacré nom d’une pipe en bois de Boulogne", "Cornegidouille", "Cornefoutre", "Bordel de foutrechiasse", "Connerie de foutrecul", "Sa race la chatte à sa mère", "Tasse-toé crisse d’épais", "Tabarnak", "Ostie d’câlice", "Ostie d’câlice de viarge", "Criss à marde", "Bout de bon Dieu", "Bon sang de bois", "Gaz de ville de France de Pute de Neko de Graz", "Tabouère", "Putain de pute", "Merdre", "Fluptre"],
	  juronsProcéduraux = [
	["Bordel", "Nom", "Putain", "Purée", "Punaise", "Connerie", "Vacherie", "Saloperie", "Crévindiou", "Osti"],
	["calebasse", "pangolin", "castagnettes", "ornithorynque", "ostéoblaste", "turlupin", "bigouden", "bouillasse", "bouillabaisse", "baloches", "criss", "pignouf", "Patagonie", "merdasse"],
	["de merde", "des Antilles", "des Carpates", "des Alpes", "des Caraïbes", "de mes deux", "du cul", "du fion", "à la con", "à la noix", "à la mords-moi-le-nœud", "de proto-turbocouille", "de ventripète à bascule"],
],

	  invention = {
		  engin: ["robot", "véhicule", "ordinateur", "jeu", "outil", "art"],
		  action: ["volant", "sous-marin", "furtif", "jetable", "automatique", "aléatoire"],
		  taille: ["géant", "habitable", "mettable", "portable", "de poche", "miniature"],
		  matériau: ["en métal", "en bois", "en plastique", "en papier", "organique", "comestible"],
		  énergie: ["manuelle", "éléctrique", "mécanique", "solaire", "éolienne", "hydraulique"],
		  usage: ["personnel", "famillial", "domestique", "professionnel", "industriel", "public"],
	  },

	  nomsHoraires = [
	["Pétrogène",
	"À-la-pelle", "Allergène", "Allogène", "Amphisbène", "Antigène",
	"Benzène", "Bigouden",
	"Calembredaine", "Catéchumène", "Cérumen", "Collagène",
	"Double-appel", "Dynamogène",
	"Épicène", "Épiphénomène", "Éolienne", "Éthylène", "Exogène",
	"Fumigène",
	"Hétérogène", "Higoumène", "Holocène",
	"Kérosène",
	"Madeleine", "Marjolaine",
	"Néoprène",
	"Obsidienne", "Œstrogène",
	"Pathogène", "Pédiplaine", "Péripatéticienne", "Périsélène", "Pénéplaine", "Pimprenelle", "Phellogène", "Phénanthrène", "Phénomène", "Propylène", "Polyéthylène", "Pyrogène",
	"Schizophrène", "Septentaine", "Spécimen",
	"Trinitrotoluène", "Tungstène",
	"Vespasienne",
	],
	["Choupette", "Champêtre",
	"Allumette", "Altimètre", "Andouillette", "Asbeste", "Averse",
	"Baguette", "Balayette", "Banette", "Barbichette", "Baromètre", "Barrette", "Bavette", "Bicyclette", "Binette", "Bourgmestre", "Branchette", "Branlette", "Brouette", "Budapest",
	"Cachette", "Charrette", "Carpette", "Céleste", "Cheminette", "Chaussette", "Chevrette", "Chevêtre", "Chouquette", "Clarinette", "Comète", "Commerce", "Conquête", "Controverse", "Corvette", "Couchette", "Courgette", "Crapette", "Crevette", "Cueillette", "Cuvette",
	"Disette",
	"Épaulette", "Escampette", "Espagnolette",
	"Facette", "Fauvette", "Fenêtre", "Fillette", "Fourchette", "Funeste",
	"Gâchette", "Gamette", "Galinette", "Goélette", "Goutelette", "Gourmette", "Grassouillette", "Grimpette", "Guillerette",
	"Houpette",
	"Jaquette", "Jupette",
	"Languette", "Lavette", "Luette", "Lorgnette",
	"Machette", "Magnétocassette", "Manette", "Mignonette", "Mal-être", "Manchette", "Maquette", "Minette", "Mirettes", "Modeste", "Mouillette", "Mouffette", "Moulinette",
	"Noisette", "Nuisette",
	"Paillette", "Palette", "Palimpseste", "Pâquerette", "Patinette", "Pédestre", "Pépettes", "Périmètre", "Perpète", "Perplexe", "Pifomètre", "Pirouette", "Plaquette", "Pochette", "Pompette", "Porte-ouverte", "Poussette",
	"Raquette", "Rosette", "Roussette", "Rupestre",
	"Salopette", "Salpêtre", "Sandalette", "Sesterce", "Sonnette", "Serviette", "Squelette", "Starlette", "Sucette", "Socquette", "Sylvestre",
	"Tablette", "Tapette", "Tergiverse", "Terrestre", "Transverse", "Toilette", "Tourette", "Trompette",
	"Vachette", "Valisette", "Vedette", "Vignette", "Violette",
	"Zapette",
]],

	  kebab = [
		  "KEBABAISE", "KEBABARBARABARBARBARBARBIERBIERRHUBARKUCHEN", "KEBABARMITZVAH", "KEBABIBEL", "KEBABOUGNOULE", "KEBABEN-LADEN", "KEBABACOOL", "KEBABATTE DE KEBABASEBALL", "KEBABRUTI", "KEBABOND, JAMES KEBABOND", "KEBABABOUBAKAR",
		  "KEBABOULINET", "KEBABTOU", "KEBABAMBOULA", "KEBABAYAGA", "KEBABABOUCHKA", "KEBABY-FOOT", "KEBABOUCHE", "KEBABOOMER", "KEBABALKANY", "KEBABARACKOBAMA", "KEBABOUCHE", "KEBABABYLONE", "KEBABÉCHAMEL", "KEBABASKET-BALL", "KEBABALEKOUILLE", "KEBABIFFLE", "KEBABEEFSTEAK", "KEBABURQA", "KEBARBALÉTRIER", "KEBABILAL HASSANI", "KEBABAR-TABAC", "KEBABAR", "KEBABACHI-BOUZOUK", "KEBABIGOUDEN", "VERY KEBABADTRIP", "KEBABABRAHAM", "KEBARBAROSSA", "KEBABARRE-À-MINE", "KEBABLYAT", "KEBABILOUTE",
		  "KEBABOUIN", "KEBABOU", "KEBABITE", "KEBABOULE", "KEBABRUTE", "KEBARBIE", "KEBARBELÉ", "KEBARBECUE", "KEBARMAN", "KEBARBAPAPA", "KEBAVAROIS", "KEBALAYETTE", "KEBABASTON", "KEBABOURRAGE", "KEBABASTINGAGE", "KEBABÛCHERON", "KEBABULLE", "KEBABALANÇOIRE", "KEBABATARD", "KEBABAGNE", "KEBABAMBI", "KEBABALEINE", "KEBABAMAKO", "KEBABALKANS", "KEBABATACLAN", "KEBABARBICHETTE", "KEBABAOBAB", "KEBABATMAN", "KEBABANANE", "KEBABANDAGE", "KEBABELGE", "KEBABOULANGER", "KEBABROUETTE", "KEBABADIGEONNER", "KEBABIMBO", "KEBABITCH", "KEBABOOBS", "KEBABALBUTATION", "KEBABARBITURIQUE", "KABABITCOIN", "KEBABASTILLE", "KEBABISMARK", "KEBABERLIN", "KEBABARQUETTE", "KEBABAGUETTE", "KEBABABIOLES", "KEBABIKINI", "KEBABUS MAGIQUE", "KEBABAD COP", "KEBABEURRE SALÉ", "KEBABARAGOUIN", "KEBABANDICOOT", "KEBABIRKENAU", "KEBABICARBONATE", "KEBABY-SITTER", "KEBABEN 10", "KEBABOUROBOROS", "KEBABADABOUM", "KEBABRICE DE NICE", "KEBABIDONVILLE", "KEBABEILLE", "KEBABABABABEGAYER", "KEBABRANQUIGNOLE", "KEBABA-AU-RHUM", "KEBABALAFRE", "KEBABALANCE TON PORC HALLAL", "KEBABABOYER", "KEBABRISER LES KEBABOURSES", "KEBABUBBLE-GUM", "KEBABINIOU", "KEBABARBECUE", "KEBABALBATAR LE KEBABIER DE L'ESPACE", "KEBABURGER", "KEBABOUILLABAISSE", "KEBABÊBÊTE",
	  ],

	  benedict = [
		  ["Beetlejuice", "Butterscotch", "Bandersnatch", "Bumpersticker", "Bumblebee", "Benadryl", "Blenderdick", "Bettyboop", "Bandicoot", "Breadmachine", "Burlington", "Buttercup", "Bumpercar", "Peppermint", "Buckingham", "Bulbizarre", "Baseballmitt", "Bumblesnuff", "Bennyhill", "Bombadil", "Boilerdang", "Buffalo", "Burberry", "Buttermilk", "Beezlebub", "Budapest", "Bumberstump", "Bodybuild", "Burgerking", "Bonaparte", "Bukkake", "Baseballbat", "Bumbleshack", "Bentobox", "Brandenburg", "Barbiturique", "Bouillabaisse", "Buckminster", "Bourgeoisie", "Bernache", "Butternut", "Berezina"],

		  ["Captaincrunch", "Cabbagepatch", "Cosbyspeech", "Cantbekhan", "Snickersnack", "Candysnatch", "Cuminhersnatch", "Combyourhatch", "Tennismatch", "Crackerjack", "Custardbath", "Snickersbar", "Crumblepie", "Scoobysnack", "Federaltax", "Bringasnack", "Cutiebrunch", "Pumpkinpatch", "Überstache", "Candycrush", "Capncrunch", "Countryside", "Cheddarcheese", "Chuckecheese", "Cul-de-sac", "Crinklefries", "Carambar"],
		  ["Benadryl Pumpkinpatch", "Battlefield Counterstrike"]
	],

	lanceNain = [
		"*Le nain est envoyé sur une pente et roule de plus en plus vite. Puis, il percute un arbre et meurt.*",
		"*Le nain est envoyé sur une pente et roule de plus en plus vite. Puis, il arrive sur une rampe et est catapulté à travers le pays.*",
		"*Le nain vole sur quelques mètres et s’éclate au sol, provoquant une secousse qui manque de faire tomber Strun.*",
		"*Strun se luxe l’épaule en tentant de soulever le nain.*",
		"*Le nain est catapulté à travers un précipice, droit sur un camp de gobelins. En un instant, plus aucune peau-verte ne respire.*",
		"*Le nain est envoyé en l’air. Il ne retombe pas et meurt dans l’espace.*",
		"*Le nain est lancé, il vole, touche le sol, roule un peu... et tombe droit dans le trou ! C'est un trou-en-nain !*",
		"*Le nain est lancé sur un frigo. L’existence du frigo cesse.*",
		"*Le nain est lancé dans un trou. Il y est très heureux.*",
		"*Le nain est lancé dans une baignoire. Il coule et se noie.*",
		"*Le nain est lancé dans un bidet. Il a pied mais, allergique à la propreté, il gonfle et étouffe.*",
		"*Le nain est lancé dans une mine. Quelques jours plus tard, le cours de l’or et du diamant s’effondrent.*",
		"*Le nain est lancé dans une mine. L’humanité a désormais assez de fer pour construire une Étoile de la Mort.*",
		"*Le nain est lancé sur une mine. Non non, pas dans, sur. Le nain meurt.*",
		"*Le nain est lancé sur un savon. La SPA accuse Strun de cruauté animale.*",
		"*Le nain est lancé sur un groupe d’autres nains. Strun se dit que ça ressemble beaucoup au billard.*",
		"*Le nain est lancé sur un diamant. Ils vécurent heureux et eurent beaucoup d’enfants.*",
		"*Le nain est lancé dans une cave. Il brise les lois de la physique en ingérant plusieurs fois son propre volume en bière.*",
		"*Le nain est lancé dans une boutique d’aromas. Brutalement allergique à ces trucs d’elfes, il décède.*",
		"*Le nain est lancé dans un restau vegan. En désespoir de trouver de la viande, il mange les clients.*",
	],
	lanceFrisbee = [
		"*Le frisbee vole à mach 3 et va décapiter un futur prix Nobel quelques 300 km plus loin.*",
		"*Le frisbee est catapulté en orbite, oblitérant le vaisseau mère d’une flotte d’invasion extraterrestre qui, terrifiée, fait immédiatement demi-tour.*",
		"*Le frisbee est propulsé à 3 % de la vitesse de la lumière droit vers le Soleil, altérant son espérance de vie de quelques secondes.*",
		"*Le frisbee est propulsé dans l’espace à une fraction de la vitesse de la lumière. D’ici quelques mois, il percutera et oblitérera la sonde Pioneer.*",
	];

const {voyelles} = require("../utils/Strun");


module.exports = {
	juron: vnr => {
		let juron;
		const ponctuation = Math.random() < 0.2 ? (vnr ? " !" : ".") : (Math.random() < (vnr ? 0.7 : 0.1) ? " !!" : " !");

		if(Math.random() < 0.5)
			juron = jurons.rand() + ponctuation;
		else
		{
			const n = util.rand(7);

			if(n <= 2)
			{
				juron = juronsProcéduraux[0].rand();
				const mid = juronsProcéduraux[1].rand();
				juron += (voyelles.includes(mid[0]) ? " d’" : " de ") + mid
						+ " " + juronsProcéduraux[2].rand()
						+ ponctuation;
			}
			else
			{
				const [source] = juronsProcéduraux;
				juron = source.rand();
				for(let i = 1 ; i < n ; i++)
					juron += " de " + source.rand().toLowerCase();
				juron += Math.random() < 0.5 ? ponctuation : " de...";
			}
		}

		return juron;
	},

	invention: () => `${Math.random() < 0.5 ? "Imagine" : "Invente"}... un ${invention.engin.rand()} ${invention.action.rand()} ${invention.taille.rand()} ${invention.matériau.rand()}, fonctionnant à l’énergie ${invention.énergie.rand()}, pour un usage ${invention.usage.rand()}.`,

	horaire: () => `${nomsHoraires[0].rand()} ${nomsHoraires[1].rand()}`,

	kebab: () => kebab.rand(),

	lanceNain: () => `**Humpf.**\n${lanceNain.rand()}`,
	lanceFrisbee: () => (Math.random() < 0.5
		? "*Le frisbee vole sur quelques dizaines de mètres et tombe au sol.*\nCe jeu est nul."
		: lanceFrisbee.rand()),

	benedict: () => (Math.random() < 1/benedict[0].length ? benedict[2].rand() : `${benedict[0].rand()} ${benedict[1].rand()}`),
};


module.exports.invention.embed = {
	color: 0x33BB33,
	title: "Dés d’invention",
	description: "Les dés d’invention sont un concept imaginé par un certain « Mike » (surnommé *Atomic Shrimp* ; http://atomicshrimp.com/post/2014/01/20/Invention-Dice) pour stimuler notre créativité. Basiquement il y a six dés avec chacun un thème (comme le matériau ou la taille), chacun ayant six faces. On lance les six et ça donne un concept.\nSi vous vous dites que quand vous innovez, y a pas 36 solutions… Ben avec ces dés, si. Exactement 36.\n\n**Les « dés » et leurs « faces » sont les suivantes :**",
	fields: Object.entries(invention).map(([name, dé]) => ({name, value: dé.join(", ")})),
	footer: {
		text: "Conçus par Atomic Shrimp",
		icon_url: "https://yt3.ggpht.com/a/AGF-l7-smEFWCy3aLDe4kyW7lfbv6p5FodQRrDXJIg=s176-c-k-c0x00ffffff-no-rj-mo"
	},
	image: { url: "http://atomicshrimp.com/public/i/inventiondicethumb.jpg" }
};
