"use strict";

const commands = require("../commands");

const register = commands.register.bind(null, "🖕🏽");
const inline = true;


const {
	checkCrisseBarbe,
	insulte, insultes, insultePluriel,
	cris, cris_safe, taMère,
} = require("../utils/Strun");
const {
	client,
	checkMaster, getMaster,
	getUser,
	PermissionFlags: { MENTION_EVERYONE },
} = require("../discordCore");
const { juron } = require("./generators");
const { respect, genre: {_e}, tn } = require("./zouz");

const servers = require("../savedData/DataSaver.class")("servers");

function retenue(guild) {
	return guild && servers.get(guild.id).settings.retenue;
}


register(["tuhum", "cri", "crie"],
"Lance un TERRIBLE cri de dracon.",
(msg, [prm = ""]) => {
	prm = prm.toLowerCase();
	if(prm === "?" || prm === "liste")
		return c_tuhums(msg);

	msg.channel.send(`**${(retenue(msg.guild) ? cris_safe : cris).rand()}**`);
}, {inline});

register(["tuhum liste", "tuhums", "tuhum?"],
"Liste les cris que je connais.",
c_tuhums, {inline});
function c_tuhums({channel, guild}) {
	const safe = retenue(guild);
	channel.send(`Je connais **${cris.length}** cris de dracon${safe ? ", mais on m’a demandé de faire preuve de retenue ici, alors je n’en utilise que **"+cris_safe.length+"**" : ""} :\n${(safe ? cris_safe : cris).join(",  ")}`);
}


register({names: ["juron", "bordel", "merde", "putain"], hidden: ["jure", "féchié", "fait"]}, // fait chier
"Me fait cracher un juron.",
({channel}, prm) => channel.send(juron(prm.join().includes('!'))))


function tuCrois(vraiment = true) {
	return `tu ${["crois", "penses", "t’imagines"].rand()}${vraiment ? [" vraiment", " sérieusement", " sincèrement"].rand() : ""}`;
}

register("insulte", "@mention",
"Insulte la personne mentionnée. Si vous n’avez mentionné personne (ou que le paramètre n’est pas une @mention), il vous insulte vous !",
async (msg, prm) => {
	prm = prm.join(" ");
	const {author, member, channel, guild, mentions} = msg;
	const user = mentions.users.first() || await getUser(prm);

	if(user)
	{
		const myself = client.user;

		if(mentions.users.size > 1)
		{
			const noRespect = [...mentions.users.filter(u => u !== myself && !checkMaster(u) && !respect(u)).values()];

			if(!noRespect.length)
				return channel.send(`Je refuse, ces personnes ont mon respect. Pour un temps du moins...`);
			else if(noRespect.length > 1)
				return channel.send(`${noRespect.join(", ")}, ${insultePluriel()} !`);
		}

		if(user === myself)
		{
			if(checkMaster(author))
				channel.send(`Ah ?.. Vraiment ? Bon... je suis un ${insultes.rand()}.`);
			else
				msg.reply(`${insulte(author, true)}, ${tuCrois()} que je vais m’insulter ${["tout seul", "moi-même"].rand()} ?`);
		}
		else if(checkCrisseBarbe(user.id))
			msg.reply(`${insulte(author, true)}, ${tuCrois()} que je vais insulter ma famille ?`);
		else if(!checkMaster(user) || checkMaster(author))
		{
			if(respect(user.id))
				channel.send(`Je refuse, cette personne a mon respect. Pour ${["un temps du moins", "le moment", "l’instant"].rand()}...`);
			else
				channel.send(`${tn(user, false, true)}, ${insulte(user)} !`);
		}
		else
			msg.reply(`${insulte(author)}, ${tuCrois()} que je vais insulter mon créateur ?`);
	}
	else if(prm === "@everyone" || prm === "@here")
	{
		if(!guild)
			channel.send("tou le monde il é caca lol");
		else if(checkMaster(member) || member.permissions.has(MENTION_EVERYONE))
			channel.send(`${prm}, ${insultePluriel()} ! Sauf ${tn(getMaster(), true)}, bien sûr.`);
		else
			msg.reply(`${insulte(author)}, ${tuCrois(false)} que je vais insulter tout le monde au nom d’un${_e(author)} minable comme toi ? Reviens me voir quand tu pourras faire des \`${prm}\` toi-même.`);
	}
	else
		channel.send(`Tu es censé${_e(author)} mentionner quelqu’un, ${insulte(author)} !`);
});

commands.register(null, ["jurons", "insultes", "juron?", "jurons?", "insulte?", "insultes?"],
"",
({channel}) => channel.send("Je ne révèle pas ma super réserve d’insultes et jurons... T’as qu’à utiliser ça : http://strategicalblog.com/liste-dinsultes-francaises-pas-trop-vulgaires/"));


register({names: ["taMère", "tamer", "taMaman", "tmr"], hidden: ["tamere"]},
"Insulte une maman (aucun respect).",
({channel, guild}) => channel.send(taMère(retenue(guild))));


register("hymne",
"Affiche mon hymne (comprenez : ma chanson préférée).",
({ channel }) => {
	const choix = Math.random();
	channel.send(
		choix < 0.15 ? "https://www.youtube.com/watch?v=-uExCBOMsRE" // Capitaine Hard Rock
		: choix < 0.4 ? "https://www.youtube.com/watch?v=jspmyTzjh60" // Halleluya
		: "**Et poil et chiottes et cul ! Bordel à queue, de couille, de jute, de trou, de bite de cheval, oh, merde chier ! Et de pine d’ours, et de pute de pisse de con… et de puanteur de ciel de chiée, de rechiée, de connerie de vacherie, de paire de baloches de saloperie de con ! Et de POIIIL !! ET DE CUUUL !! CONNERIE DE CHIOOOTTES !! POIIIIIIL !! CUUUUUUL !! TROU DE BAAAALLE !! Merde, chier, POIIIIL !! POIIIIIIIIL !!!**\nhttps://youtu.be/zzR215qjxKw?t=3m13s");
});
