"use strict";

const register = exports.register = require("../commands").register.bind(null, "🔨");
const inline = true;


const splitMessage = require("../utils/splitMessage");
const {
	client, auth: {support: invitServeurSupport},
	getMaster, checkMaster,
	getUser, getUsers,
	getMembers,
	isAdmin,
	PERMS_FR, KEY_PERMS,
 } = require("../discordCore");
const { insulte } = require("../utils/Strun");
const { genre: {_e, _1} } = require("./zouz");
const generators = require("./generators");
const { parseDice } = require("../utils/diceParser");
const servers = require("../savedData/DataSaver.class")("servers");

const { ChannelType: {
	GuildCategory: GUILD_CATEGORY,
	GuildForum: GUILD_FORUM,
	GuildVoice: GUILD_VOICE,
	GuildStageVoice: GUILD_STAGE_VOICE,
}} = require("discord.js");


{
	const {donne: aide} = require("./aide");
	require("../commands").register(null, ["utile", "utiles"], "",
	({channel}) => aide(channel, "🔨"));
}


register({names: ["présentation", "description", "tki", "Strun"], hidden: ["presentation", "mrtki", "ptdrtki", "tkilol", "StrunBahCouille"]},
"Affiche une petite description de ce bot.",
({channel, prefix: p = ">"}) => channel.send({ embeds: [{
	color: 0x33BB33,
	title: "Strun Bah Couille",
	description: `Strun Bah Couille est un bot brutal et vulgaire qui a pour but d’animer votre serveur. À l’origine, il servait uniquement d’entraînement, mais s’est tellement développé qu’il est réellement devenu un bot à part entière, avec sa propre utilité.
Il est certes très doué pour déblatérer (\`>insulte\`,  \`>juron\`), mais dispose également de commandes utiles comme \`>avatar\`, quelques commandes de modération (réservées aux administrateurs, bien sûr), et a même un petit jeu dont vous êtes le héros.

Serveur de support : ${invitServeurSupport}
Dépôt : https://framagit.org/GlanVonBrylan/StrunBahCouille
Licence : http://www.wtfpl.net/txt/copying

Quelques commandes :`,
	fields: [
		{ name: p+"aide", value: "Affiche l’aide", inline: true },
		{ name: p+"tuhum", value: "Lance un TERRIBLE cri de dracon", inline: true },
		{ name: p+"insulte @mention", value: "Insulte quelqu’un", inline: true },
		{ name: p+"merde", value: "Lâche un juron", inline: true },
		{ name: p+"avatar @mention", value: "Affiche l’URL de l’avatar de la personne mentionnée.", inline: true },
		{ name: p+"Lenny", value: "( ͡° ͜ʖ ͡°)", inline: true },
		{ name: p+"silence, >parle", value: "Respectivement, coupe ou rend la parole à un membre.", inline: true },
		{ name: p+"venture", value: "Vous prépare à une formidable Venture. (en MP seulement)", inline: true },
		{ name: p+"zouz", value: "Présente les zouz, la monnaie des Crisse-Barbes.", inline: true },
		{ name: p+"date", value: "Affiche la date du jour.", inline: true },
		{ name: p+"règle yo false", value: "Empêche Strun de répondre aux gens pour faire la conversation.", inline: true },
	],
	footer: {
		text: "Créé par " + getMaster().username,
		icon_url: getMaster().avatarURL(),
	},
	image: {
		url: "https://brylan.fr/files/STRUN%20BAH%20COUILLE.png"
	}
}]}),
{inline});

register({names: ["invitation", "invit'", "support"], hidden: ["invit", "invit’"]},
"Affiche l’invitation pour mon serveur de support, et celle pour m’ajouter à votre serveur.",
({channel}) => channel.send(`Pour m’ajouter à votre serveur : https://discordapp.com/oauth2/authorize?&client_id=${client.user.id}&scope=bot&permissions=8\nPour rejoindre mon serveur de support : ${invitServeurSupport}`).then(msg => msg.suppressEmbeds(), error),
{inline});



const imgURLOptions = {size: 512, dynamic: true};

register(["avatar", "pdp", "pfp", "pp"], "[global] @mention",
"Affiche en grand l’avatar des personne(s) mentionnée(s), ou le vôtre si personne n’a été mentionné. Si vous précisez “global”, l’avatar sera montré à la place de celui du serveur.",
async ({author, member, channel, mentions}, prm) => {
	const send = channel.send.bind(channel);
	const global = prm[0]?.includes("global");
	if(global)
		prm.shift();

	if(prm.length)
	{
		prm = prm.join(" ");
		const files = (channel.guild && !global
			? mentions.members.size ? mentions.members : await getMembers(channel.guild, prm)
			: mentions.users.size ? mentions.users : await getUsers(prm)
		)
			.map(u => u.displayAvatarURL(imgURLOptions));

		if(files.length > 10)
			send("Merci de ne pas mentionner plus de 10 personnes.");
		else if(files.length)
			send({files});
		else
			send("Utilisateur non trouvé.");
	}
	else
		send({files: [((!global && member) || author).displayAvatarURL(imgURLOptions)]});
}, {inline});

const bannerURLOptions = {...imgURLOptions, size: 1024};
register({names: "bannière", hidden: ["banniere", "banière", "baniere", "banner"]}, "[globale] @mention",
"Affiche la bannière des personne(s) mentionnée(s), ou le vôtre si personne n’a été mentionné. Si vous précisez “globale”, l’avatar sera montré à la place de celui du serveur.",
async ({author, member, channel, mentions}, prm) => {
	const send = channel.send.bind(channel);
	const global = prm[0]?.includes("global");
	if(global)
		prm.shift();
	const guild = !global && channel.guild;

	if(prm.length)
	{
		prm = prm.join(" ");
		mentions = guild
			? mentions.members.size ? mentions.members : await getMembers(channel.guild, prm)
			: mentions.users.size ? mentions.users : await getUsers(prm);
		if(mentions.length > 10)
			return send("Merci de ne pas mentionner plus de 10 personnes.");

		mentions = await Promise.all(mentions.map(user => user.fetch(true)));
		let files = mentions.map(user => user.bannerURL(bannerURLOptions));
		if(guild) for(let i = 0 ; i < files.length ; i++)
		{
			if(files[i]) continue;
			const {user} = mentions[i];
			await user.fetch(true);
			files[i] = user.bannerURL(bannerURLOptions);
		}
		files = files.filter(Boolean);

		if(files.length)
			send({files});
		else if(mentions.length === 1)
			send("Cet utilisateur n'a pas de bannière.");
		else if(mentions.length)
			send("Ces utilisateurs n'ont pas de bannière.");
		else
			send("Utilisateur non trouvé.");
	}
	else
	{
		let url;
		if(guild)
		{
			await member.fetch(true);
			url = member.bannerURL(bannerURLOptions);
		}
		if(!url)
		{
			await author.fetch(true);
			url = author.bannerURL(bannerURLOptions);
		}
		send(url ? {files: [url]} : "Vous n'avez pas de bannière.");
	}
}, {inline});


register({names: ["icôneServeur", "icône", "logo"], hidden: ["iconeServeur", "imageServeur", "icone", "logoServeur"]},
"Affiche en grand l’icône du serveur.",
({author, channel, guild}) => {
	if(guild)
		channel.send(guild.iconURL(imgURLOptions) || "Ce serveur n’a pas d’icône.");
	else
		channel.send(`On n’est pas sur un serveur, ${insulte(author)} !`);
});



register({names: "infos", hidden: ["info", "infoMembre", "infosMembre"]}, "@mention",
"Affiche les infos sur la ou les personne(s) mentionnée(s) (les mentions peuvent être remplacées par des tags ou des ids)",
({author, channel, guild}, prm) => {
	const members = guild && [...guild.members.cache.values()].sort((a, b) => a.joinedTimestamp - b.joinedTimestamp);

	if(!prm.length)
		sendInfos(author);
	else
		prm.forEach(sendInfos);


	function ordinal(n, user) {
		return n === 1 ? _1(user) : (n + "e");
	}

	async function sendInfos(resolvable) {
		const user = typeof resolvable === "string" ? await getUser(resolvable) : resolvable;

		if(!user)
			return channel.send(`g pa trouvé ${resolvable}`);

		const fields = [
			{ name: `Inscrit${_e(user)} le`, value: user.createdAt.toLocaleString("fr"), inline: true },
			{ name: "Bot", value: user.bot ? "Oui" : "Non", inline: true },
		];

		let colour;

		if(guild)
		{
			const member = guild.members.resolve(user);
			if(member)
			{
				if(member.roles.color)
					colour = member.roles.color.color;

				fields.push(
					{name: "Surnom (à date)", value: member.nickname || "*aucun*", inline: true},
					{name: "A rejoint ce serveur le", value: guild.members.resolve(user).joinedAt.toLocaleString("fr"), inline: true},
					{name: "À rejoindre", value: ordinal(members.indexOf(member) + 1, member), inline: true},
					{name: "Rôles", value: member.roles.cache.size - 1 + "", inline: true}, // -1 because of @everyone
				);

				if(member.premiumSince)
					fields.push({name: "Booste depuis le", value: member.premiumSince.toLocaleString("fr"), inline: true});

				let perms = member.permissions.toArray();
				// Comme ça parce que sinon les perms sont dans un mauvais ordre
				perms = perms.includes("ADMINISTRATOR")
							? ["Admin (donc toutes)"]
							: KEY_PERMS.filter(keyPerm => perms.includes(keyPerm)).map(perm => PERMS_FR[perm]);

				fields.push({name: "Permissions spéciales", value: perms.length ? perms.join(", ") : "*Aucune*"});
			}
			else
				fields.push({name: "N’est pas sur ce serveur.", value: ":("});
		}

		channel.send({ embeds: [{
			color: colour,
			thumbnail: { url: user.displayAvatarURL({dynamic: true}) },
			title: user.tag,
			description: `${user}\nID : ${user.id}`,
			fields,
		}]});
	}
}, {inline});


register({names: "infosServeur", hidden: "infoServeur"},
"Affiche les infos sur le serveur (date de création, nombre de salons, etc).",
async ({author, channel, guild}, [id]) => {
	if(!guild)
	{
		if(checkMaster(author) && id)
		{
			guild = client.guilds.cache.get(id);
			if(!guild)
				return channel.send("Je n’ai pas trouvé ce serveur.");
		}
		else
			return channel.send(`On n’est pas sur un serveur, ${insulte(author)} !`);
	}

	let categories = 0, text = 0, voice = 0, stage = 0, threads = 0, forums = 0, animated = 0;

	for(const [id, channel] of guild.channels.cache)
	{
		const {type} = channel;
		if(type === GUILD_CATEGORY)
			categories++;
		else if(type === GUILD_FORUM)
			forums++;
		else if(type === GUILD_VOICE)
			voice++;
		else if(type === GUILD_STAGE_VOICE)
			stage++;
		else if(channel.isThread())
			threads++;
	}

	for(const [id, emoji] of guild.emojis.cache)
		if(emoji.animated)
			animated++;

	const owner = await guild.fetchOwner();
	if(!owner)
		return channel.send("J’ai pas réussi à récupérer les infos sur le proprio et ça a TOUT CASSÉ");

	const tooMany = guild.memberCount > 1000; // Limite imposée par Discord
	const members = tooMany ? guild.members.cache : await guild.members.fetch();
	if(!members)
		return channel.send("J’ai pas réussi à récupérer les infos sur les membres et ça a TOUT CASSÉ");

	const regularChannels = guild.channels.cache.size - categories - voice - stage - threads - forums;
	function n(number, label, labelPlural = label+"s") {
		return `${number} ${number > 1 ? labelPlural : label}`;
	}

	const premiumTier = typeof guild.premiumTier === "number" ? guild.premiumTier // sometimes it's a number apparently?
	 					: guild.premiumTier === "NONE" ? 0 : guild.premiumTier[5];

	const fields = [
		{ name: "Date de création", value: guild.createdAt.toLocaleString("fr"), inline: true },
		{ name: "Vérifié", value: guild.verified ? "Oui" : "Non", inline: true },
		{ name: "Niveau de boost", value: `Niveau **${premiumTier}** *(${n(guild.premiumSubscriptionCount, "boost")})*`, inline: true },
		{ name: "Membres", value: `${guild.memberCount}${tooMany ? "" : ` (dont ${members.reduce((n, {user:{bot}}) => n+bot, 0)} bots)`}`, inline: true },
		{ name: "Catégories", value: categories, inline: true },
		{ name: "Salons", value: `${n(regularChannels, "écrit")}, ${n(threads, "fil")}, ${n(voice, "vocal", "vocaux")}${stage ? `, ${stage} de conférence` : ""}${forums ? `, ${n(forums, "forum")}` : ""}`, inline: true },
		{ name: "Rôles", value: guild.roles.cache.size, inline: true },
		{ name: "Émojis", value: guild.emojis.cache.size, inline: true },
		{ name: "dont animées", value: animated, inline: true },
		{ name: "J’ai rejoint le", value: guild.joinedAt.toLocaleString("fr") },
	];
	for(const field of fields)
		field.value = ""+field.value;

	channel.send({ embeds: [{
		thumbnail: { url: guild.iconURL() },
		title: guild.name,
		description: `ID : ${guild.id}\nProprio : ${owner} (${owner.user.tag})\n${guild.description || ""}${guild.vanityURLCode ? `\nURL de vanité : https://discord.gg/${res.code}` : ""}`,
		fields,
		footer: isAdmin(guild.members.me) ? undefined : { text: "Note : le nombre de fils est peut-être trop bas à cause de mes permissions." },
	}]});
}, {inline});


register(["membres", "listeMembres"], "<ping|pingPas> [options...]",
`Affiche la liste des membres de ce serveur. Mettez \`ping\` pour les lister par mention *(ce qui va ping toutes les personnes concernées, donc attention...)*, ou \`pingPas\` pour les lister par tag.
__Options disponibles__ :
— \`filtre\`. Les filtres disponibles sont \`rôles\`, \`rôles=aucun\`, \`bots\`, \`bots=non\`, \`admins\`, \`admins=non\`, \`boosteurs\`, \`boost=non\`, \`connectés\`, \`connectés=non\` et \`statut=X=x\` (où \`x\` peut être \`enLigne\`, \`afk\`, \`nePasDéranger\` et \`déco\`, sans espace).
— \`tri=arrivée\` (par date d'arrivée), \`nbRôles\` (nombre de rôles). Vous pouvez ajouter \`=décroissant\` pour inverser le tri.
__Exemples__ :
\`>membres pingPas filtre=bots=non filtre=co tri=nbRôles=décroissant\` (liste des membres non bots connectés, de celui ayant le plus de rôles à celui en ayant le moins)
\`>membres ping filtre=bots filtre=admin\` (liste des bots admins, non triée)`,
(msg, prms) => {
	const { author, member, channel, guild } = msg;
	const send = channel.send.bind(channel);
	if(!msg.guild)
		return msg.reply(`On n’est pas sur un serveur, ${insulte(author)} !`);
	else if(guild.memberCount > 300)
		return send(`Ce serveur compte **${guild.memberCount}** membres... Ce qui est trop ! Je ne vais pas passez un quard d’heure à récupérer la liste entière.`);

	let ping = false;
	prms = prms.map(p => p.toLowerCase());

	if(!prms.length)
		return send("Tu dois préciser si je ping ou pas.");
	else if(prms[0] === "ping")
	{
		if(member.permissions.has(PERMISSIONS.MENTION_EVERYONE))
			ping = true;
		else
			return send("Me faire ping plein de gens alors que toi-même tu ne peux pas `@everyone` ? Je ne crois pas, non.");
	}
	else if(prms[0] !== "pingpas" && prms[0] !== "pasping")
		return send("J’ai pas compris, je le fais en pingant ou pas ?");

	let membres = [...guild.members.cache.values()];

	let trié = false;
	prms.shift();

	for(const prm of prms)
	{
		const [p, val, opt] = prm.split("=");

		switch(p)
		{
			case "order":
			case "sort":
			case "tri":
			case "trier": {
				if(trié)
					return send("Vous ne pouvez pas utiliser plusieurs options de tri.");

				const tri = val;

				if(tri.includes("arrivée") || tri.includes("arrivee"))
					membres.sort((a, b) => a.joinedTimestamp - b.joinedTimestamp);
				else if(tri === "nbrôles" || tri === "nbroles")
					membres.sort((a, b) => a.roles.cache.size - b.roles.cache.size);
				else
					return send("Tri inconnu : " + val);

				if(["inverse", "desc", "descendant", "decroissant", "décroissant", "décroissants", "decroissants"].includes(opt))
					membres.reverse();

				trié = true;
			}
				break;
			case "filter":
			case "filtre":
			case "filtrer": {
				const not = ["non", "pas", "0", "aucun"].includes(opt);

				switch(val)
				{
					case "roles":
					case "rôles":
						membres = membres.filter(e => {
							const ok = e.roles.cache.size > 1; // there is always @everyone
							return not ? !ok : ok;
						});
						break;

					case "bot":
					case "bots":
						membres = membres.filter(not ? e => !e.user.bot : e => e.user.bot);
						break;

					case "admin":
					case "admins":
						membres = membres.filter(not ? e => !isAdmin(e) : e => isAdmin(e));
						break;

					case "boost":
					case "booster":
					case "boosters":
					case "boosteur":
					case "boosteurs":
						membres = membres.filter(not ? e => !e.premiumSinceTimestamp : e => e.premiumSinceTimestamp);
						break;

					case "co":
					case "connecté":
					case "connectés":
						membres = membres.filter(not ? e => e.presence.status === "offline" : e => e.presence.status !== "offline");
						break;

					case "statut":
					case "status":
						if(!opt)
							return send("Vous devez préciser quel statut (enLigne, inactif, nePasDéranger, déco)");

						const statuts = {
							online: "online",
							vert: "online",
							enligne: "online", co: "online",
							connecté: "online", connectée: "online",
							actif: "online", active: "online",
							idle: "idle",
							orange: "idle",
							inactif: "idle", inactive: "idle",
							absent: "idle", absente: "idle",
							afk: "idle",
							dnd: "dnd",
							rouge: "dnd",
							nepasderanger: "dnd", nepasdéranger: "dnd",
							offline: "offline",
							gris: "offline",
							deco: "offline", déco: "offline",
						},
							  statut = statuts[opt];

						if(!statut)
							return send("Statut inconnu : " + opt);
						else
							membres = membres.filter(e => e.presence.status === statut);
						break;

					default:
						return send("Filtre inconnu : " + val);
				}
			}
				break;

			default:
				return send("Option inconnue : " + p);
		}
	}

	membres = membres.map(m => {
		const rôles = m.roles.cache.size - 1; // @everyone
		return `${ping ? m : `**${m.user.tag}**`} : ${isAdmin(m) ? "**Admin.** " : ""}Rôle${rôles > 1 ? "s" : ""} : **${rôles}**. A rejoint le ${m.joinedAt.toLocaleString("fr")}.${m.premiumSinceTimestamp ? " **Booste le serveur.**" : ""}`;
	});

	splitMessage(`${membres.join("\n")}\n*Total : ${membres.length}*`).forEach(send);
});



const numEmojis = ["0️⃣", "1️⃣", "2️⃣", "3️⃣", "4️⃣", "5️⃣", "6️⃣", "7️⃣", "8️⃣", "9️⃣"];
register({names: "épinglés", hidden: ["nombreepingles", "nombreépinglés", "nepingles", "népinglés", "epingles", "épinglés"]},
"Donne le nombre de messages épinglés dans le salon. (le maximum est 50)",
(msg) => {
	msg.channel.messages.fetchPinned().then(async ({size: nPinned}) => {
		if(nPinned && nPinned % 11 === 0) // On ne peut pas réagir deux fois avec la même émoji... patate
			return msg.channel.send(`Il y a **${nPinned}** messages épinglés dans ce salon.`);

		try {
			if(nPinned > 10)
				await msg.react(numEmojis[~~(nPinned / 10)]);
			await msg.react(numEmojis[nPinned % 10]);
			if(nPinned >= 45)
				await msg.react("⚠️");
		}
		catch(e) {
			error(e);
		}
	}, err => {
		error(err);
		msg.react("😕");
	})
}, {inline});


register({names: ["émoji", "émojis"], hidden: ["emote", "emotes", "émote", "émotes", "emoji", "emojis", "émoticône", "émoticônes", "emoticone", "emoticones"]}, "<émojis de serveur>",
"Donne l’URL des émojis données. **Ne fonctionne pas avec les émojis de base de Discord.** Par exemple `>émoji :kek:` fonctionne mais pas `>émoji :sob:`",
({author, channel}, prm) => {
	// (?=<a?:[\wÀ-ÿ]+:)[0-9]+(?=>) ne fonctionne pas parce que le positive lookahead ne fonctionne que s'il est après et le lookbehind ne permet pas une longueur variable :(
	const émojis = prm.join(" ").match(/<a?:[\wÀ-ÿ]+:[0-9]+>/gm);
	const send = channel.send.bind(channel);

	if(émojis)
	{
		const urls = émojis.map(émoji => `https://cdn.discordapp.com/emojis/${émoji.match(/[0-9]+(?=>)/)[0]}.${émoji.startsWith("<a") ? "gif" : "png"}`);
		splitMessage(urls.join("\n")).forEach(send);
	}
	else
		send(`Bah alors, faut mettre une émoji !, ${insulte(author)} !\n(ou alors tu as essayé avec une émoji de base de Discord, mais ça je sais pas faire)`);
}, {inline});


register(["lance", "roll"], "<x>d<y> [(dés)avantage]",
"Lance x dés à y faces. Exemple : `>lance 3d6` lance trois dés à six faces. Le nombre maximum de dés pouvant être lancés simultanément dépend du serveur et est à 10 par défaut. Vous pouvez ajouter `+x` pour augmenter le résultat, par exemple `>ance 1d4+2` donnera un résultat entre 3 et 6 (`-x` fonctionne également).\nSi vous précisez `avantage`, Strun fera deux jets et gardera le meilleur. À l'inverse, préciser `désavantage` fera deux jets pour garder le pire. Vous pouvez raccourcir ça à `av` et `désav`.\n*Vous pouvez aussi lancer un nain.*",
({author, channel, guild}, prm) => {
	const {maxRolls = 30} = guild ? servers.get(guild.id).settings : {};

	prm = prm.join(" ").toLowerCase();

	if(!maxRolls)
		channel.send("Les lancés de dés sont désactivés sur ce serveur.");
	else if(prm.includes("nain") || prm.includes("gimli") || prm.includes("graz") || prm.includes("260865683825360896")) // id de Graz
		channel.send(generators.lanceNain());
	else if(prm.includes("jardin"))
		channel.send(["Hein.", "Un jardin ?..", "Heu.", "... Non ?"].rand());
	else if(prm.includes("frisbee"))
		channel.send(generators.lanceFrisbee());
	else
	{
		const outcome = parseDice(prm, maxRolls);
		if(typeof outcome === "string")
			channel.send(outcome);
		else
		{
			const { params, result, lostResult } = outcome;
			const { n, d, bonus, advantage } = params;
			let { rolls, total } = result;
			if(!bonus)
				total = `**${total}**`;
			let totalStr = n === 1 ? total : `${rolls.join(", ")}. Total : ${total}`;
			const bonusStr = bonus
				? `, ${bonus > 0 ? `+${bonus}` : bonus} qui font **${total + bonus}**.`
				: '.';
			let advStr = "";
			if(lostResult)
			{
				const { rolls, total } = lostResult;
				advStr = `\n*Jet rejeté : ${n !== 1 ? `${rolls.join("+")} = ` : ""}${total}*`;
			}

			channel.send(`🎲${author} a lancé ${n}d${d}${advantage ? ` avec ${advantage}` : ""} : ${totalStr}${bonusStr}${advStr}`);
		}
	}
});



register({names: ["distribue", "répartir"], hidden: ["distribuer", "repartir", "répartit", "repartit"]}, "<éléments>",
`Effectue des associations aléatoires entre les éléments de plusieurs ensembles. Les ensembles sont séparés par des points-virgules ou des retours à la ligne, et les éléments d'un ensemble sont séparés par des virgules.
Exemple : \`>distribue a, b, c ; 1, 2, 3\` peut donner \`a=1,b=3,c=2\`. Ou \`a=3,b=1,c=2\`. C’est aléatoire.
Les ensembles ne sont pas obligés d’avoir le même nombre d’éléments.`,
({channel}, prm) => {
	const sets = prm.join(" ").split(prm.includes(";") ? ";" : "\n");

	if(!sets[1])
		channel.send("Vous devez mettre au moins deux ensembles.");
	else
	{
		function processSet(set) { return set.split(",").map(e => e.trim()).filter(e => e).shuffle(); }
		const keys = processSet(sets.shift());
		const nKeys = keys.length;
		const results = Array.from({length: nKeys}, () => []); // Array#fill would fill it with references to the same array...

		for(let set of sets)
		{
			set = processSet(set);
			const setL = Math.min(nKeys, set.length);

			if(!setL)
				continue;

			for(let i = 0 ; i < nKeys ; i++)
				results[i].push(set[i % setL]);
		}

		const allowedMentions = { parse: [] };
		splitMessage(keys.map((e, i) => `${e.trim()} : ${results[i].join(", ")}`).sort().join("\n"))
			.map(content => ({ content, allowedMentions }))
			.forEach(channel.send.bind(channel));
	}
});


const jeChoisis = ["Je choisis", "Je pense", "Je dirais", "Disons", "On va dire", "Pourquoi pas"];
register({names: ["choisis", "décide"], hidden: ["choix", "decide", "choisis,", "décide,", "choisis:", "décide:", "décidator"]}, "<éléments>",
"Demander à Strun de choisir un élément parmi plusieurs, séparés par des virgules.\nExemple : `>choisis : chocolat, vanille ou pistache ?`\nOu juste : `>choisis chocolat,vanille,pistache`\nMais Morgân s’est embêté à faire en sorte que ça marche avec les deux-points, le “ou” et le point d’interrogation donc faites-le svp.",
({channel}, choix) => {
	if(choix[0] === ":")
		choix.shift();
	if(choix[choix.length - 1] === "?")
		choix.pop();

	for(let i = 0, l = choix.length ; i < l ; i++)
		if(choix[i] === "ou")
			choix[i] = ",";

	choix = choix.join(" ").split(",").map(e => e.trim()).filter(Boolean);

	channel.send({
		content: !choix.length ? "Il faut me dire parmi quoi choisir."
			: choix.length === 1 ? `Bah ${choix[0]} lol`
			: `${jeChoisis.rand()}... ${choix.rand()} !`,
		allowedMentions: {parse: []},
	});
}, {inline});

register("choisis-en", "<nombre> <élément>",
"Comme `>choisis`, mais Strun en sélectionne plusieurs.\nExemple : `>choisis-en 2 parmi salade, tomate et oignon`\nIdem, le “parmi” est facultatif.",
({channel}, choix) => {
	const n = +choix.shift();

	if(!Number.isInteger(n) || n < 0)
		return channel.send("Il faut me dire combien en choisir.");

	if(n === 0)
		return channel.send("Bah rien lol");

	if(choix[0] === "parmi" || choix[0] === ":")
		choix.shift();

	for(let i = choix.length - 1 ; i > 0 ; i--)
		if(choix[i] === "et")
		{ choix[i] = ","; break; }

	choix = choix.join(" ").split(",").map(e => e.trim()).filter(Boolean);

	if(!choix.length)
		return channel.send("Il faut me dire parmi quoi choisir.");
	if(choix.length === n)
		return channel.send("Bah, tout.");
	if(choix.length < n)
		return channel.send(`${n} parmi ${choix.length} ?`);

	if(n === 1)
		return channel.send(`${jeChoisis.rand()}... ${choix.rand()} !`);

	choix.shuffle();
	choix.length = n;
	const last = choix.pop();
	channel.send(`${jeChoisis.rand()}... ${choix.join(", ")}${n > 4 && Math.random() < 0.5 ? [" et", ", et enfin", ", et pour finir"].rand() : " et"} ${last}.`);
}, {inline});


const {datePrésente} = require("./Vive la France !");
register("date",
"Affiche la date du jour.",
({channel}) => {
	const date = datePrésente();
	channel.send(`Nous sommes ${date[0] === 'o' ? "l’" : "le "}${date}.`);
});



register({names: ["lune"], hidden: ["phaselune"]}, "[date]",
"Demander à Strun de vous montrer la phase de la lune pour la date donnée (au format jj/mm/aaaa), ou celle d'aujourd'hui si non précisée.",
(message, [date]) => {
	if(!date)
		date = new Date();
	else if(!date.match(/^([012]?[0-9]|3[01])[/-](0?[1-9]|1[0-2])[/-][0-9]{4}$/))
		return message.reply("Format de date invalide. Merci d'utiliser jj/mm/aaaa");
	else
	{
		const [jour, mois, année] = date.split(date.includes("/") ? "/" : "-");
		date = new Date(année, mois-1, jour);
		if(isNaN(date.valueOf()))
			return message.reply("J'ai pas compris la date. Merci d'utiliser jj/mm/aaaa");
	}

	// Code taken from https://jivebay.com/calculating-the-moon-phase/
	let c, e, jd, b;
	c = e = jd = b = 0;

	let day = date.getDate(), month = date.getMonth()+1, year = date.getFullYear();
	if (month < 3)
	{
		year--;
		month += 12;
	}

	++month;
	c = 365.25 * year;
	e = 30.6 * month;
	jd = c + e + day - 694039.09;	//jd is total days elapsed
	jd /= 29.5305882;				//divide by the moon cycle
	b = ~~jd;						//int(jd) -> b, take integer part of jd
	jd -= b;						//subtract integer part to leave fractional part of original jd
	b = Math.round(jd * 8);			//scale fraction from 0-8 and round

	message.react(["🌑", "🌒", "🌓", "🌔", "🌕", "🌖", "🌗", "🌘"][b % 8]);
})
